const colors = require('tailwindcss/colors');

module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        violet: colors.violet,
        lime: colors.lime,
        orange: colors.orange,
        sky: colors.sky,
        gold: {
          100: '#F4CA3E',
          200: '#D4AF37',
          300: '#E1AD21',
          400: '#A49467',
          700: '#B78727',
        },
        eiev: {
          100: '#13214a',
        },
      },
      fontFamily: {
        cairo: ['Cairo', 'sans-serif'],
      },
    },
  },
  plugins: [require('@tailwindcss/typography'), require('@tailwindcss/forms')],
};
