/**
 * @flow
 */

const envVarArray = Object.entries(process.env)
    .filter(([key]) => key.startsWith('REACT_APP_'))
    .map(([key, value]) => [key.replace(/REACT_APP_/g, ''), String(value)]),
  env: { [string]: string } = Object.fromEntries(envVarArray);

export const { EIEV_API_BASE_URL } = env;
