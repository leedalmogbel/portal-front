/**
 * @flow
 */

import { useState, useEffect, useContext, createContext } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { authLogin } from 'actions/auth/login';
import { authLogout } from 'actions/auth/logout';
import { fetchNotification } from 'actions/notification/fetch';
import { useNotification } from 'hooks/notification';
import useLocalStorage from 'hooks/localStorage';
import type { UserWithPermission } from 'types/user';
import type { State } from 'reducers/types';
import type { AuthState } from 'reducers/auth';

type ContextType = {
  authenticated: boolean,
  login: Function,
  logout: Function,
  user: ?UserWithPermission,
  isAdmin: boolean,
  // permissions: Object,
  canUsersCreate: boolean,
  canUsersUpdate: boolean,
  canUsersDelete: boolean,
  canRacesCreate: boolean,
  canRacesUpdate: boolean,
  canRacesView: boolean,
  canRacesDelete: boolean,
  canRacesApprove: boolean,
  canRacesReject: boolean,
};

const initialValues = {
  authenticated: false,
  login: () => {},
  logout: () => {},
  user: null,
  isAdmin: false,
  canUsersCreate: false,
  canUsersUpdate: false,
  canUsersDelete: false,
  canRacesCreate: false,
  canRacesUpdate: false,
  canRacesView: false,
  canRacesDelete: false,
  canRacesApprove: false,
  canRacesReject: false,
  // permissions: {},
};

const AuthContext = createContext<ContextType>(initialValues);

type AuthProviderProps = {
  children: ?Node,
};

const AuthProvider = ({ children }: AuthProviderProps): Node => {
  const dispatch = useDispatch();
  const { data: auth } = useSelector<State, AuthState>((state) => state.auth);
  const [user, setUser] = useState(initialValues.user);
  const [authenticated, setAuthenticated] = useState(
    initialValues.authenticated
  );
  const [session, setSession] = useLocalStorage('session', null);

  const permissions = user?.role?.permissions?.actions,
    canUsersCreate = permissions?.users?.includes('create') || false,
    canUsersUpdate = permissions?.users?.includes('update') || false,
    canUsersDelete = permissions?.users?.includes('delete') || false,
    canRacesCreate = permissions?.races?.includes('create') || false,
    canRacesUpdate = permissions?.races?.includes('update') || false,
    canRacesView = permissions?.races?.includes('view') || false,
    canRacesDelete = permissions?.races?.includes('delete') || false,
    canRacesApprove = permissions?.races?.includes('approve') || false,
    canRacesReject = permissions?.races?.includes('reject') || false;

  const isAdmin = user?.userType === 'Admin';
  console.log(isAdmin);
  console.log('asdasdadsd', canUsersCreate);

  const login = ({ username, password }) => {
    dispatch(authLogin({ username, password }));
  };

  const logout = () => {
    setAuthenticated(false);
    setSession(null);
    dispatch(authLogout());
  };

  useEffect(() => {
    if (session) {
      setUser(session);
      setAuthenticated(true);
    }
  }, [dispatch, session, auth]);

  useEffect(() => {
    if (auth) {
      setAuthenticated(true);
      setUser(auth);
      setSession(auth);
    }
  }, [auth, setSession]);

  return (
    <AuthContext.Provider
      value={{
        authenticated,
        login,
        logout,
        user,
        isAdmin,
        canUsersCreate,
        canUsersUpdate,
        canUsersDelete,
        canRacesCreate,
        canRacesUpdate,
        canRacesView,
        canRacesDelete,
        canRacesApprove,
        canRacesReject,
      }}>
      {children}
    </AuthContext.Provider>
  );
};

const useAuth = (): ContextType => {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error('useAuth must be used within a AuthProvider');
  }
  return context;
};

export { AuthProvider, useAuth };
