/**
 * @flow
 */

import {
  createContext,
  useContext,
  useEffect,
  useState,
  useCallback,
} from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { fetchRoles } from 'actions/roles/fetch';
import { mapRolesToOptions } from 'utils/generic';
import type { Option } from 'types';
import type { State } from 'reducers/types';
import type { RolesState } from 'reducers/roles';

const initialValues = {
  roleOptions: [],
  dispatchFetchRoles: () => {},
};

type ContextType = {
  roleOptions: Array<Option>,
  dispatchFetchRoles: Function,
};

const RolesContext = createContext<ContextType>(initialValues);

type RolesProviderProps = {
  children: ?Node,
};

const RolesProvider = ({ children }: RolesProviderProps): Node => {
  const [roleOptions, setRoleOptions] = useState([]);
  const {
    data: roles,
    status: { fetched },
  } = useSelector<State, RolesState>((state) => state.roles);
  const dispatch = useDispatch();

  const dispatchFetchRoles = useCallback(() => {
    dispatch(fetchRoles());
  }, [dispatch]);

  useEffect(() => {
    if (fetched) {
      setRoleOptions(mapRolesToOptions(roles));
    }
  }, [fetched, roles]);

  return (
    <RolesContext.Provider value={{ roleOptions, dispatchFetchRoles }}>
      {children}
    </RolesContext.Provider>
  );
};

const useRoles = (): ContextType => {
  const context = useContext(RolesContext);
  if (context === undefined) {
    throw new Error('useRoles must be used within a RolesProvider');
  }
  return context;
};

export { RolesProvider, useRoles };
