/**
 * @flow
 */

import {
  createContext,
  useContext,
  useEffect,
  useState,
  useCallback,
} from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { fetchNotification } from 'actions/notification/fetch';
import type { Option } from 'types';
import type { State } from 'reducers/types';
import type { NotificationState } from 'reducers/notification';
import { useAuth } from 'hooks/auth';

const initialValues = {
  notifications: null,
  dispatchFetchNotification: () => {},
};

type ContextType = {
  notifications: Array<Option>,
  dispatchFetchNotification: Function,
};

const NotificationContext = createContext<ContextType>(initialValues);

type NotificationProviderProps = {
  children: ?Node,
};

const NotificationProvider = ({
  children,
}: NotificationProviderProps): Node => {
  const {
    data: notification,
    status: { fetched },
  } = useSelector<State, NotificationState>((state) => state.notification);
  const [userNotification, setNotif] = useState(initialValues.notifications);
  const dispatch = useDispatch();
  const { user } = useAuth();

  const dispatchFetchNotification = useCallback(() => {
    console.log('dispatchFetchNotification', user.id);
    dispatch(fetchNotification(user.id));
  }, [dispatch, user]);

  useEffect(() => {
    console.log('state', fetched);
    // console.log('fetched', fetched);
    if (fetched) {
      console.log('masodasdasd');
      setNotif(notification);
      console.log('fetched', fetched, notification);
    }
  }, [setNotif, notification, fetched]);

  //   useEffect(() => {
  //     if (fetched) {
  //       console.log('zzzfetched', notification);
  //     }
  //   }, [fetched, notification]);

  return (
    <NotificationContext.Provider
      value={{ dispatchFetchNotification, userNotification }}>
      {children}
    </NotificationContext.Provider>
  );
};

const useNotification = (): ContextType => {
  const context = useContext(NotificationContext);
  if (context === undefined) {
    throw new Error(
      'useNotification must be used within a NotificationProvider'
    );
  }
  return context;
};

export { NotificationProvider, useNotification };
