/**
 * @flow
 */

import { useContext, createContext } from 'react';
import type { Node } from 'react';
import { useDispatch } from 'react-redux';

import { resetPassword } from 'actions/password';
import { forgotPassword } from 'actions/password/forgot';
import { changePassword } from 'actions/password/change';

type ContextType = {
  dispatchForgotPassword: Function,
  dispatchChangePassword: Function,
  dispatchResetPassword: Function,
};

const initialValues = {
  dispatchForgotPassword: () => {},
  dispatchChangePassword: () => {},
  dispatchResetPassword: () => {},
};

const PasswordContext = createContext<ContextType>(initialValues);

type PasswordProviderProps = {
  children: ?Node,
};

const PasswordProvider = ({ children }: PasswordProviderProps): Node => {
  const dispatch = useDispatch();
  const dispatchForgotPassword = (mobile: string): void => {
    dispatch(forgotPassword(mobile));
  };

  const dispatchChangePassword = (data): void => {
    dispatch(changePassword(data));
  };

  const dispatchResetPassword = (): void => {
    dispatch(resetPassword());
  };

  return (
    <PasswordContext.Provider
      value={{
        dispatchForgotPassword,
        dispatchChangePassword,
        dispatchResetPassword,
      }}>
      {children}
    </PasswordContext.Provider>
  );
};

const usePassword = (): ContextType => {
  const context = useContext(PasswordContext);
  if (context === undefined) {
    throw new Error('usePassword must be used within a PasswordProvider');
  }

  return context;
};

export { PasswordProvider, usePassword };
