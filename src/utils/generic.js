/**
 * @flow
 */

import type { Role } from 'types/user';
import type { Option } from 'types';

export const mapRolesToOptions = (roles: Array<Role>): Array<Option> => {
  return roles.map(({ name, id }) => ({
    name,
    value: id.toString(),
  }));
};
