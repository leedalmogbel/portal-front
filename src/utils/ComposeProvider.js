import { isEmpty, reverse } from 'lodash';

export default function ComposeProviders({ Providers, children }) {
  if (isEmpty(Providers)) {
    return children;
  }

  return reverse(Providers).reduce((acc, Provider) => {
    return <Provider>{acc}</Provider>;
  }, children);
}
