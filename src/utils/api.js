/**
 * @flow
 */

import axios from 'axios';
import type { Axios } from 'axios';
import { toast } from 'react-toastify';

import { EIEV_API_BASE_URL } from 'environment';

export const eievApi: Axios = axios.create({
  baseURL: EIEV_API_BASE_URL,
});

const errorInterceptorFunction = (error: any) => {
  if (error.response) {
    console.log('error NOW', error.response);
    switch (error.response.status) {
      case 401:
        toast.error('Unauthorized API call!');
        break;
      case 403:
        toast.error('Forbidden. Resource is forbidden');
        break;
      case 404:
        toast.error(error.response.data.message);
        // toast.error('Resource not Found. Unable to load resource');
        break;
      case 409:
        toast.error(error.response.message);
        break;
      case 500:
        toast.error(
          'Internal Server Error. The server was unable to complete your request.'
        );
        break;
      case 502:
        toast.error(
          'Bad Gateway. The server was unable to complete your request.'
        );
        break;
      case 503:
        toast.error(
          'Service Unavailable. The server was unable to complete your request.'
        );
        break;
      default:
        break;
    }
    return Promise.reject(error);
  }
};

export const axiosInterceptors = () => {
  eievApi.interceptors.response.use(
    (response) => response,
    (error) => errorInterceptorFunction(error)
  );
};
