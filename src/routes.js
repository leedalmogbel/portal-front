/**
 * @flow
 */

import { lazy } from 'react';
import type { AbstractComponent, ComponentType, Node } from 'react';
import { GiHorseHead, GiCowboyBoot } from 'react-icons/gi';
import {
  FaHatCowboy,
  FaUserTie,
  FaClipboardList,
  FaFlagCheckered,
  FaUsers,
} from 'react-icons/fa';
import { MdSpaceDashboard, MdEventNote, MdCottage } from 'react-icons/md';
import { IoLogOut } from 'react-icons/io5';
import { BsCalendar3 } from 'react-icons/bs';

const Dashboard: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/Dashboard')
  ),
  Riders: AbstractComponent<mixed, mixed> = lazy(() => import('views/Riders')),
  RiderDetail: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/RiderDetail')
  ),
  Horses: AbstractComponent<mixed, mixed> = lazy(() => import('views/Horses')),
  HorseDetail: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/HorseDetail')
  ),
  Trainers: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/Trainers')
  ),
  TrainerDetail: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/TrainerDetail')
  ),
  Owners: AbstractComponent<mixed, mixed> = lazy(() => import('views/Owners')),
  OwnerDetail: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/OwnerDetail')
  ),
  Events: AbstractComponent<mixed, mixed> = lazy(() => import('views/Events')),
  EventDetail: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/EventDetail')
  ),
  Races: AbstractComponent<mixed, mixed> = lazy(() => import('views/Races')),
  RaceDetail: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/RaceDetail')
  ),
  Entries: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/Entries')
  ),
  EntryDetail: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/EntryDetail')
  ),
  EntryManager: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/EntryManager')
  ),
  Stables: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/Stables')
  ),
  StableDetail: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/StableDetail')
  ),
  Users: AbstractComponent<mixed, mixed> = lazy(() => import('views/Users')),
  UserDetail: AbstractComponent<mixed, mixed> = lazy(() =>
    import('views/UserDetail')
  ),
  Logout: AbstractComponent<mixed, mixed> = lazy(() => import('views/Logout'));

export type Route = {|
  path: string,
  key: string,
  element: Node,
  sidebar?: {
    name: string,
    Icon: ComponentType<{}>,
  },
  permittedRoles?: Array<string>,
|};

type RoutesType = {|
  [string]: Route,
|};

const Routes: RoutesType = {
  dashboard: {
    path: 'dashboard',
    key: 'DASHBOARD',
    element: <Dashboard />,
    sidebar: {
      name: 'Dashboard',
      Icon: MdSpaceDashboard,
    },
  },
  users: {
    path: 'users',
    key: 'USERS',
    element: <Users />,
    sidebar: {
      name: 'Users',
      Icon: FaUsers,
    },
    permittedRoles: ['Admin'],
  },
  userDetail: {
    path: 'users/:id',
    key: 'USER_DETAIL',
    element: <UserDetail />,
    permittedRoles: ['Admin', 'User'],
  },
  userAdd: {
    path: 'users/add',
    key: 'USER_CREATE',
    element: <UserDetail />,
    permittedRoles: ['Admin'],
  },
  riders: {
    path: 'riders',
    key: 'RIDERS',
    element: <Riders />,
    sidebar: {
      name: 'Riders',
      Icon: GiCowboyBoot,
    },
    permittedRoles: ['Admin', 'User'],
  },
  riderDetail: {
    path: 'riders/:id',
    key: 'RIDER_DETAIL',
    element: <RiderDetail />,
    permittedRoles: ['Admin', 'User'],
  },
  riderAdd: {
    path: 'riders/add',
    key: 'RIDER_CREATE',
    element: <RiderDetail />,
    permittedRoles: ['Admin', 'User'],
  },
  horses: {
    path: 'horses',
    key: 'HORSES',
    element: <Horses />,
    sidebar: {
      name: 'Horses',
      Icon: GiHorseHead,
    },
    permittedRoles: ['Admin', 'User'],
  },
  horseAdd: {
    path: 'horses/add',
    key: 'HORSE_CREATE',
    element: <HorseDetail />,
    permittedRoles: ['Admin', 'User'],
  },
  horseDetail: {
    path: 'horses/:id',
    key: 'HORSE_DETAIL',
    element: <HorseDetail />,
    permittedRoles: ['Admin', 'User'],
  },
  trainers: {
    path: 'trainers',
    key: 'TRAINERS',
    element: <Trainers />,
    sidebar: {
      name: 'Trainers',
      Icon: FaHatCowboy,
    },
    permittedRoles: ['Admin', 'User'],
  },
  trainerDetail: {
    path: 'trainers/:id',
    key: 'TRAINER_DETAIL',
    element: <TrainerDetail />,
    permittedRoles: ['Admin', 'User'],
  },
  trainerAdd: {
    path: 'trainers/add',
    key: 'TRAINER_CREATE',
    element: <TrainerDetail />,
    permittedRoles: ['Admin', 'User'],
  },
  owners: {
    path: 'owners',
    key: 'OWNERS',
    element: <Owners />,
    sidebar: {
      name: 'Owners',
      Icon: FaUserTie,
    },
    permittedRoles: ['Admin', 'User'],
  },
  ownerDetail: {
    path: 'owners/:id',
    key: 'OWNER_DETAIL',
    element: <OwnerDetail />,
    permittedRoles: ['Admin', 'User'],
  },
  ownerAdd: {
    path: 'owners/add',
    key: 'OWNER_CREATE',
    element: <OwnerDetail />,
    permittedRoles: ['Admin', 'User'],
  },
  events: {
    path: 'events',
    key: 'EVENTS',
    element: <Events />,
    sidebar: {
      name: 'Events',
      Icon: MdEventNote,
    },
    permittedRoles: ['Admin'],
  },
  eventDetail: {
    path: 'events/:id',
    key: 'EVENT_DETAIL',
    element: <EventDetail />,
    permittedRoles: ['Admin'],
  },
  eventAdd: {
    path: 'events/add',
    key: 'EVENT_CREATE',
    element: <EventDetail />,
    permittedRoles: ['Admin'],
  },
  races: {
    path: 'races',
    key: 'RACES',
    element: <Races />,
    sidebar: {
      name: 'Races',
      Icon: FaFlagCheckered,
    },
    permittedRoles: ['Admin', 'Eef', 'User'],
  },
  raceDetail: {
    path: 'races/:id',
    key: 'RACE_DETAIL',
    element: <RaceDetail />,
    permittedRoles: ['Admin'],
  },
  raceAdd: {
    path: 'races/add',
    key: 'RACE_CREATE',
    element: <RaceDetail />,
    permittedRoles: ['Admin'],
  },
  calendar: {
    path: 'calendar',
    key: 'CALENDAR',
    element: <Races />,
    sidebar: {
      name: 'Calendar',
      Icon: BsCalendar3,
    },
    permittedRoles: ['Admin', 'Eef', 'User'],
  },
  entries: {
    path: 'entries',
    key: 'ENTRIES',
    element: <Entries />,
    sidebar: {
      name: 'Entries',
      Icon: FaClipboardList,
    },
    permittedRoles: ['Admin', 'User', 'Eef'],
  },
  entryDetail: {
    path: 'entries/:id',
    key: 'ENTRY_DETAIL',
    element: <EntryDetail />,
    permittedRoles: ['Admin', 'User', 'Eef'],
  },
  entryManager: {
    path: 'entries/manage',
    key: 'ENTRY_MANAGER',
    element: <EntryManager />,
    permittedRoles: ['Admin'],
  },
  entryAdd: {
    path: 'entries/add',
    key: 'ENTRY_CREATE',
    element: <EntryDetail />,
    permittedRoles: ['Admin', 'User', 'Eef'],
  },
  stables: {
    path: 'stables',
    key: 'STABLES',
    element: <Stables />,
    sidebar: {
      name: 'Stables',
      Icon: MdCottage,
    },
    permittedRoles: ['Admin'],
  },
  stableDetail: {
    path: 'stables/:id',
    key: 'STABLE_DETAIL',
    element: <StableDetail />,
    permittedRoles: ['Admin'],
  },
  stableAdd: {
    path: 'stables/add',
    key: 'STABLE_CREATE',
    element: <StableDetail />,
    permittedRoles: ['Admin'],
  },
  logout: {
    path: 'logout',
    key: 'LOGOUT',
    // TODO: Logout
    element: <Logout />,
    sidebar: {
      name: 'Logout',
      Icon: IoLogOut,
    },
  },
};

export const routes: Array<Route> = [
  Routes.dashboard,
  Routes.users,
  Routes.userAdd,
  Routes.userDetail,
  Routes.calendar,
  Routes.races,
  Routes.raceAdd,
  Routes.raceDetail,
  Routes.entries,
  Routes.entryAdd,
  Routes.entryDetail,
  Routes.entryManager,
  Routes.horses,
  Routes.horseAdd,
  Routes.horseDetail,
  Routes.riders,
  Routes.riderAdd,
  Routes.riderDetail,
  Routes.trainers,
  Routes.trainerAdd,
  Routes.trainerDetail,
  Routes.owners,
  Routes.ownerAdd,
  Routes.ownerDetail,
  Routes.events,
  Routes.eventAdd,
  Routes.eventDetail,
  Routes.stables,
  Routes.stableAdd,
  Routes.stableDetail,
  Routes.logout,
];
