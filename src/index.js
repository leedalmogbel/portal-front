/**
 * @flow
 */

import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import ComposeProvider from 'utils/ComposeProvider';
import { AuthProvider } from 'hooks/auth';
import { RolesProvider } from 'hooks/roles';
import { UserProvider } from 'views/Users/hook';
import { UserDetailProvider } from 'views/UserDetail/hook';
import { OwnerProvider } from 'views/Owners/hook';
import { OwnerDetailProvider } from 'views/OwnerDetail/hook';
import { TrainerProvider } from 'views/Trainers/hook';
import { TrainerDetailProvider } from 'views/TrainerDetail/hook';
import { RiderProvider } from 'views/Riders/hook';
import { RiderDetailProvider } from 'views/RiderDetail/hook';
import { HorseProvider } from 'views/Horses/hook';
import { HorseDetailProvider } from 'views/HorseDetail/hook';
import { EventProvider } from 'views/Events/hook';
import { EventDetailProvider } from 'views/EventDetail/hook';
import { RaceProvider } from 'views/Races/hook';
import { RaceDetailProvider } from 'views/RaceDetail/hook';
import { EntryProvider } from 'views/Entries/hook';
import { EntryManagerProvider } from 'views/EntryManager/hook';
import { EntryDetailProvider } from 'views/EntryDetail/hook';
import { StableProvider } from 'views/Stables/hook';
import { StableDetailProvider } from 'views/StableDetail/hook';
import { DashboardProvider } from 'views/Dashboard/hook';
import { PasswordProvider } from 'hooks/password';
import { NotificationProvider } from 'hooks/notification';
import store from 'store';

import App from './App';

const providers = [
  AuthProvider,
  UserProvider,
  UserDetailProvider,
  OwnerProvider,
  OwnerDetailProvider,
  TrainerProvider,
  TrainerDetailProvider,
  RiderProvider,
  RiderDetailProvider,
  HorseProvider,
  HorseDetailProvider,
  EventProvider,
  EventDetailProvider,
  RaceProvider,
  RaceDetailProvider,
  EntryProvider,
  EntryDetailProvider,
  EntryManagerProvider,
  StableProvider,
  StableDetailProvider,
  RolesProvider,
  DashboardProvider,
  PasswordProvider,
  NotificationProvider,
];

ReactDOM.render(
  <Provider store={store}>
    <ComposeProvider Providers={providers}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ComposeProvider>
  </Provider>,
  document.getElementById('root')
);
