/**
 * @flow
 */

import { useEffect, useState } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { isEmpty } from 'lodash';

import { useRace } from 'views/Races/hook';
import { useStable } from 'views/Stables/hook';
import type { State } from 'reducers/types';
import type { RaceDetailState } from 'reducers/races/detail';

import Form from './Form';
import { mapRaceToFormData } from './utils';

const Container = tw.div`p-8 bg-white`;

const RaceDetail = (): Node => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { pledge, setPledge } = useState(false);
  const {
    data: raceDetail,
    status: { fetching, fetched, added, edited },
  } = useSelector<State, RaceDetailState>((state) => state.races.detail);
  const { dispatchFetchRace, dispatchResetRace } = useRace();
  const { stableOptions, dispatchFetchStables } = useStable();

  useEffect(() => {
    return () => {
      dispatchResetRace();
    };
  }, [dispatchResetRace]);

  useEffect(() => {
    if (id) {
      console.log('dispatchFetchRace(id)', dispatchFetchRace(id));
      dispatchFetchRace(id);
    }
  }, [id, dispatchFetchRace]);

  useEffect(() => {
    if (added) {
      toast.success('Race succssfully registered!');
      navigate('/races');
    }
  }, [added, navigate, dispatchResetRace]);

  useEffect(() => {
    if (edited) {
      // TODO: Hide toast if nothing was changed
      toast.success('Race succssfully updated!');
      navigate('/races');
    }
  }, [edited, navigate, dispatchResetRace]);

  useEffect(() => {
    dispatchFetchStables();
  }, [dispatchFetchStables]);

  return (
    <Container>
      {fetching && <div>Loading...</div>}
      {fetched && !isEmpty(stableOptions) && (
        <Form defaultValues={mapRaceToFormData(raceDetail)} />
      )}
      {!fetching && !fetched && !isEmpty(stableOptions) && <Form />}
    </Container>
  );
};

export default RaceDetail;
