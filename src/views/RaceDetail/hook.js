/**
 * @flow
 */

import { useState, useContext, createContext, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import type { Scene } from 'types';

type ContextType = {
  race: Race,
  isPledge: boolean,
  sceneIsAdd: boolean,
  sceneIsEdit: boolean,
  sceneIsView: boolean,
  changeSceneToAdd: Function,
  changeSceneToEdit: Function,
  changeSceneToView: Function,
};

const initialValues = {
  race: null,
  isPledge: false,
  scene: 'view',
  sceneIsAdd: false,
  sceneIsEdit: false,
  sceneIsView: true,
  changeSceneToAdd: () => {},
  changeSceneToEdit: () => {},
  changeSceneToView: () => {},
};

const RaceContext = createContext<ContextType>(initialValues);

type RaceDetailProviderProps = {
  children: ?Node,
};

const RaceDetailProvider = ({ children }: RaceDetailProviderProps): Node => {
  const [scene, setScene] = useState<Scene>(initialValues.scene);
  const sceneIsAdd = scene === 'add',
    sceneIsEdit = scene === 'edit',
    sceneIsView = scene === 'view';

  const [isPledge, setIsPledge] = useState<boolean>(initialValues.isPledge);
  const {
    data: race,
    status: { fetched },
  } = useSelector<State, RaceState>((state) => state.races.detail);

  const changeSceneToAdd = () => {
    setScene('add');
  };

  const changeSceneToEdit = () => {
    setScene('edit');
  };

  const changeSceneToView = () => {
    setScene('view');
  };

  useEffect(() => {
    if (fetched) {
      setIsPledge(race.pledge);
    }
  }, [setIsPledge, fetched, race]);

  return (
    <RaceContext.Provider
      value={{
        race,
        isPledge,
        setIsPledge,
        sceneIsAdd,
        sceneIsEdit,
        sceneIsView,
        changeSceneToAdd,
        changeSceneToEdit,
        changeSceneToView,
      }}>
      {children}
    </RaceContext.Provider>
  );
};

const useRaceDetail = (): ContextType => {
  const context = useContext(RaceContext);
  if (context === undefined) {
    throw new Error('useRaceDetail must be used within a RaceContextProvider');
  }
  return context;
};

export { RaceDetailProvider, useRaceDetail };
