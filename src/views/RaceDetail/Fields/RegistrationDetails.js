/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment, useState, useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Textarea from 'components/common/Textarea';
import Input from 'components/common/Input';
import Checkbox from 'components/common/Checkbox';
import Select from 'components/common/Select';
import Error from 'components/common/Error';
import { raceCategoryOptions, raceCodeOptions } from 'utils/constants';

import { useRaceDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`,
  OneThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc((100% - 80px) / 3)])`,
  Required = tw.span`text-red-500 font-black text-xl`;

const RegistrationDetailsFields = (): Node => {
  const { errors } = useFormState();
  const { sceneIsView, isPledge, setIsPledge } = useRaceDetail();
  const readOnly = sceneIsView;
  const [isChecked, setIsChecked] = useState(false);

  useEffect(() => {
    if (isChecked) {
      setIsPledge(isChecked);
    }
  });

  return (
    <Fragment>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="name">
            Race Title<Required> *</Required>
          </Label>
          <Input
            name="name"
            id="name"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.name && 'Race Title is required'}</Error>
        </FormGroup>
        {/* <FormGroup tw="flex-1">
          <Label htmlFor="location">Location</Label>
          <Select
            name="location"
            id="location"
            options={eventLocationOptions}
            readOnly={readOnly}
          />
        </FormGroup> */}
      </TwoThirdColumnContainer>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="contactPerson">
            Contact Person<Required> *</Required>
          </Label>
          <Input
            name="contactPerson"
            id="contactPerson"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.contactPerson && 'Contact Person is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="contactNumber">
            Contact Number<Required> *</Required>
          </Label>
          <Input
            name="contactNumber"
            id="contactNumber"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.contactNumber && 'Contact Number is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="allowedCount">
            Entry Count<Required> *</Required>
          </Label>
          <Input
            name="allowedCount"
            id="allowedCount"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.allowedCount && 'Entry Count is required'}</Error>
        </FormGroup>
      </FlexContainer>
      <FlexContainer>
        <FormGroup tw="w-4/12">
          <Label htmlFor="eventDate">
            Race Date<Required> *</Required>
          </Label>
          <Input
            name="eventDate"
            id="eventDate"
            type="datetime-local"
            format="dd-mm-yyyy HH:mm"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.eventDate && 'Race Date is required'}</Error>
        </FormGroup>
        <FormGroup tw="w-4/12">
          <Label htmlFor="openingDate">
            Opening Date<Required> *</Required>
          </Label>
          <Input
            name="openingDate"
            id="openingDate"
            type="datetime-local"
            format="dd-mm-yyyy HH:mm"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.openingDate && 'Opening Date is required'}</Error>
        </FormGroup>
        <FormGroup tw="w-4/12">
          <Label htmlFor="closingDate">
            Closing Date<Required> *</Required>
          </Label>
          <Input
            name="closingDate"
            id="closingDate"
            type="datetime-local"
            format="dd-mm-yyyy HH:mm"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.closingDate && 'Closing Date is required'}</Error>
        </FormGroup>
      </FlexContainer>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="raceCategory">
            Race Category
            <Required> *</Required>
          </Label>
          <Select
            name="raceCategory"
            id="raceCategory"
            options={raceCategoryOptions}
            rules={{ required: false }}
            readOnly={readOnly}
          />
          <Error>{errors.raceCategory && 'Sheikh Stable is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="raceCode">
            Race Code
            <Required> *</Required>
          </Label>
          <Select
            name="raceCode"
            id="raceCode"
            options={raceCodeOptions}
            rules={{ required: false }}
            readOnly={readOnly}
          />
          <Error>{errors.pvtStable && 'Private Stable is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
      <FlexContainer>
        <FormGroup tw="flex-none inline-block">
          <Label htmlFor="pledge">Pledge</Label>
          <Checkbox
            name="pledge"
            id="pledge"
            type="checkbox"
            onChange={(e) => setIsChecked(e.target.checked)}
            checked={isPledge}
            rules={{ required: false }}
            readOnly={readOnly}
          />
          <Error>{errors.pledge && 'Pledge is required'}</Error>
        </FormGroup>
      </FlexContainer>
      {isPledge && (
        <Fragment>
          <FlexContainer>
            <FormGroup tw="flex-initial w-1/4">
              <Label htmlFor="noticeTitle">Notice Title</Label>
              <Input
                name="noticeTitle"
                id="noticeTitle"
                type="text"
                rules={{ required: true }}
                readOnly={readOnly}
              />
              <Error>{errors.noticeTitle && 'Title is required'}</Error>
            </FormGroup>
          </FlexContainer>
          <FlexContainer>
            <FormGroup tw="flex-initial w-2/4">
              <Label htmlFor="noticeBody">Notice Body</Label>
              <Textarea id="noticeBody" name="noticeBody" readOnly={readOnly} />
            </FormGroup>
          </FlexContainer>
        </Fragment>
      )}
    </Fragment>
  );
};

export default RegistrationDetailsFields;
