/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment, useState } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import AutoComplete from 'components/common/AutoComplete';
import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Select from 'components/common/Select';
import Error from 'components/common/Error';
import Checkbox from 'components/common/Checkbox';
import { useEvent } from 'views/Events/hook';
import { eventLocationOptions, countryOptions } from 'utils/constants';

import { useRaceDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`,
  Required = tw.span`text-red-500 font-black text-xl`;

const EventInformationFields = (): Node => {
  const { errors } = useFormState();
  const { setQuery, eventOptions } = useEvent();
  const { sceneIsEdit, sceneIsView, race } = useRaceDetail();
  const readOnly = sceneIsView;
  const [isChecked, setIsChecked] = useState(false);
  console.log('tzzzae', isChecked);
  return (
    <Fragment>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="eventName">
            Event<Required> *</Required>
          </Label>
          <AutoComplete
            id="eventName"
            name="eventName"
            onInputChange={setQuery}
            options={
              race && (sceneIsEdit || sceneIsView) && race.events
                ? [{ name: race?.events?.name, value: race?.events?.id }]
                : eventOptions
            }
            readOnly={readOnly}
          />
          <Error>{errors.eventName && 'Event is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-none inline-block">
          <Label htmlFor="eventCreate">Create Event</Label>
          <Checkbox
            name="eventCreate"
            id="eventCreate"
            type="checkbox"
            onChange={(e) => setIsChecked(e.target.checked)}
            rules={{ required: false }}
            readOnly={readOnly}
          />
          <Error>{errors.eventCreate && 'Pledge is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
      {isChecked && (
        <Fragment>
          <TwoThirdColumnContainer>
            <FormGroup tw="flex-1">
              <Label htmlFor="eventTitle">
                Event Title
                <Required> *</Required>
              </Label>
              <Input
                name="eventTitle"
                id="eventTitle"
                type="text"
                rules={{ required: false }}
                readOnly={readOnly}
              />
              <Error>{errors.eventTitle && 'Event Title is required'}</Error>
            </FormGroup>
          </TwoThirdColumnContainer>
          <TwoThirdColumnContainer>
            <FormGroup tw="flex-1">
              <Label htmlFor="eventDesc">
                Event Description
                <Required> *</Required>
              </Label>
              <Input
                name="eventDesc"
                id="eventDesc"
                type="text"
                rules={{ required: false }}
                readOnly={readOnly}
              />
              <Error>
                {errors.eventDesc && 'Event Description is required'}
              </Error>
            </FormGroup>
          </TwoThirdColumnContainer>
          <TwoThirdColumnContainer>
            <FormGroup tw="flex-1">
              <Label htmlFor="location">
                Location
                <Required> *</Required>
              </Label>
              <Select
                name="location"
                id="location"
                options={eventLocationOptions}
                readOnly={readOnly}
              />
            </FormGroup>
            <FormGroup tw="flex-1">
              <Label htmlFor="country">
                Country
                <Required> *</Required>
              </Label>
              <Select
                name="country"
                id="country"
                options={countryOptions}
                readOnly={readOnly}
              />
            </FormGroup>
          </TwoThirdColumnContainer>
          <FlexContainer>
            <FormGroup tw="w-4/12">
              <Label htmlFor="startDate">
                Start Date
                <Required> *</Required>
              </Label>
              <Input
                name="startDate"
                id="startDate"
                type="date"
                format="yyyy-mm-dd"
                rules={{ required: true }}
                readOnly={readOnly}
              />
              <Error>{errors.startDate && 'Event Date is required'}</Error>
            </FormGroup>
            <FormGroup tw="w-4/12">
              <Label htmlFor="endDate">
                End Date
                <Required> *</Required>
              </Label>
              <Input
                name="endDate"
                id="endDate"
                type="date"
                format="yyyy-mm-dd"
                rules={{ required: true }}
                readOnly={readOnly}
              />
              <Error>{errors.endDate && 'End Date is required'}</Error>
            </FormGroup>
          </FlexContainer>
        </Fragment>
      )}
    </Fragment>
  );
};

export default EventInformationFields;
