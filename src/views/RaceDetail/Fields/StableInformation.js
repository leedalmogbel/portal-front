/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { first, isEmpty } from 'lodash';
import { Fragment, useState, useEffect, useCallback } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import {
  useFormState,
  useForm,
  useFieldArray,
  Controller,
  useFormContext,
} from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Select from 'components/common/Select';
import Error from 'components/common/Error';
import Checkbox from 'components/common/Checkbox';
import Button from 'components/common/Button';
import { useStable } from 'views/Stables/hook';
import { _countOptions } from 'utils/constants';

import { useRaceDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`,
  Required = tw.span`text-red-500 font-black text-xl`;

const StableInformation = (): Node => {
  const { errors } = useFormState();
  const { stableOptions } = useStable();
  const { getValues, reset } = useFormContext({
    defaultValues: { stableName: '', count: '' },
  });
  // const { register, control, watch, getValues } = useForm({
  //   defaultValues: {
  //     stables: [{ stableName: '', count: '' }],
  //   },
  // });
  console.log('wow');
  const { fields, append, remove } = useFieldArray({
    name: 'stables',
  });
  console.log('lmao', fields);
  console.log('_countOptions', _countOptions);

  const { sceneIsAdd, sceneIsEdit, sceneIsView, race } = useRaceDetail();
  const readOnly = sceneIsView;
  const [isChecked, setIsChecked] = useState(false);

  const addStableDefault = useCallback(() => {
    append({
      stableName: first(stableOptions),
      count: 0,
    });
  }, [append, stableOptions]);

  // const handleAddStable = () => {
  //   setStableList([...stableList, { specificStable: '', count: 0 }]);
  //   console.log('stableList', stableList);
  // };

  // const handleRemoveStable = (index: number) => {
  //   const list = stableList.filter((_, i) => i !== index);

  //   setStableList(list);
  //   console.log('stableList123', stableList);
  // };

  useEffect(() => {
    // console.log('sceneadd', sceneIsAdd);
    // console.log('sceneedit', sceneIsEdit);
    // console.log('sceneView', sceneIsView);

    if (sceneIsAdd && isChecked) {
      addStableDefault();
    }
  }, [sceneIsAdd, addStableDefault, isChecked]);

  useEffect(() => {
    if (!isChecked && sceneIsAdd) {
      reset({
        stableName: first(stableOptions),
        count: 0,
      });
    }
  }, [sceneIsAdd, isChecked, reset, stableOptions]);

  // useEffect(() => {
  //   if ((sceneIsEdit || sceneIsView) && getValues('stables')) {
  //     console.log('asdqweqweq', getValues('stables'));

  //     const stables = getValues('stables') || [];
  //     reset({ stables });
  //   }
  // }, [sceneIsEdit, sceneIsView, reset, getValues]);

  useEffect(() => {
    const stables = getValues('stables') || [];
    reset({ stables });
  }, [reset, getValues]);

  return (
    <Fragment>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="sheikhStable">
            Sheikh Stable
            <Required> *</Required>
          </Label>
          <Select
            name="sheikhStable"
            id="sheikhStable"
            options={_countOptions}
            rules={{ required: false }}
            readOnly={readOnly}
          />
          <Error>{errors.sheikhStable && 'Sheikh Stable is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="pvtStable">
            Private Stable
            <Required> *</Required>
          </Label>
          <Select
            name="pvtStable"
            id="pvtStable"
            options={_countOptions}
            rules={{ required: false }}
            readOnly={readOnly}
          />
          <Error>{errors.pvtStable && 'Private Stable is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
      {sceneIsAdd && (
        <FlexContainer>
          <FormGroup tw="flex-none inline-block">
            <Label htmlFor="specific">Specific</Label>
            <Checkbox
              name="specific"
              id="specific"
              type="checkbox"
              onChange={(e) => setIsChecked(e.target.checked)}
              rules={{ required: false }}
              readOnly={readOnly}
            />
          </FormGroup>
        </FlexContainer>
      )}

      {!isEmpty(fields) &&
        fields?.length > 0 &&
        fields?.map((stableItem, index) => (
          <TwoThirdColumnContainer key={stableItem.id}>
            <FormGroup tw="flex-1">
              <Label htmlFor="specificStable">
                Stable
                <Required> *</Required>
              </Label>
              <Select
                name={`stables.${index}.stableName`}
                id={`stables.${index}.stableName`}
                options={stableOptions}
                readOnly={readOnly}
                key={stableItem.id}
                // {...register(`stables[name[${index}]]`)}
                //{...register(`stables.${index}.stableName`, { required: true })}
              />
            </FormGroup>
            <FormGroup tw="flex-1">
              <Label htmlFor="count">
                Entry Count
                <Required> *</Required>
              </Label>
              {/* <Controller
              name={`stables.${index}.count`}
              render={({ onChange, value }) => ()} /> */}
              <Input
                name={`stables.${index}.count`}
                id={`stables.${index}.count`}
                type="text"
                rules={{ required: true }}
                readOnly={readOnly}
                // {...register(`stables[count[${index}]]`)}
                //{...register(`stables.${index}.count`, { required: true })}
              />
              <Error>{errors.count && 'Private Stable is required'}</Error>
            </FormGroup>
            {fields.length > 1 && !sceneIsView && (
              <FormGroup tw="flex items-center mb-0">
                <Button
                  tw=""
                  type="button"
                  size="xs"
                  onClick={() => {
                    remove(index);
                  }}>
                  X
                </Button>
              </FormGroup>
            )}
          </TwoThirdColumnContainer>
        ))}

      {/* {(sceneIsEdit || sceneIsView) &&
        !isEmpty(getValues('stables')) &&
        getValues('stables')?.length > 0 &&
        getValues('stables')?.map((stableItem, index) => (
          <TwoThirdColumnContainer key={stableItem.id}>
            <FormGroup tw="flex-1">
              <Label htmlFor="specificStable">
                Stable
                <Required> *</Required>
              </Label>
              <Select
                name={`stables.${index}.stableName`}
                id={`stables.${index}.stableName`}
                options={stableOptions}
                readOnly={readOnly}
                key={stableItem.id}
              />
            </FormGroup>
            <FormGroup tw="flex-1">
              <Label htmlFor="count">
                Entry Count
                <Required> *</Required>
              </Label>
              <Input
                name={`stables.${index}.count`}
                id={`stables.${index}.count`}
                type="text"
                rules={{ required: true }}
                readOnly={readOnly}
              />
              <Error>{errors.count && 'Private Stable is required'}</Error>
            </FormGroup>
            {fields.length > 1 && !sceneIsView && (
              <FormGroup tw="flex items-center mb-0">
                <Button
                  tw=""
                  type="button"
                  size="xs"
                  onClick={() => {
                    remove(index);
                  }}>
                  X
                </Button>
              </FormGroup>
            )}
          </TwoThirdColumnContainer>
        ))} */}
      {!sceneIsView && (
        <FlexContainer>
          <FormGroup tw="flex items-center mb-0">
            <Button tw="" type="button" size="xs" onClick={addStableDefault}>
              Add Stable
            </Button>
          </FormGroup>
        </FlexContainer>
      )}
    </Fragment>
  );
};

export default StableInformation;
