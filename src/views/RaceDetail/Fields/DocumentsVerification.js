/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment, useEffect, useState } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { GrDocumentPdf } from 'react-icons/gr';

import Textarea from 'components/common/Textarea';
import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Error from 'components/common/Error';
import type { State } from 'reducers/types';
import type { RaceDetailState } from 'reducers/races/detail';

import { useRaceDetail } from '../hook';

const Container = tw.div`flex gap-10 items-center bg-gray-100 p-4 mb-4`,
  FlexContainer = tw.div`flex gap-10 mb-4`,
  FormGroup = tw.div`w-3/12`,
  Text = tw.span`uppercase`,
  Document = tw.a`font-medium text-red-600`,
  PdfLink = tw.a`text-3xl text-center cursor-pointer`;

const DocumentsVerificationFields = (): Node => {
  const { errors } = useFormState();
  const { data: race } = useSelector<State, RaceDetailState>(
    (state) => state.races.detail
  );

  const { sceneIsView } = useRaceDetail();
  const readOnly = sceneIsView;

  const [selectedFile, setSelectedFile] = useState();
  const [preview, setPreview] = useState();

  // create a preview as a side effect, whenever selected file is changed
  useEffect(() => {
    console.log(selectedFile);
    if (!selectedFile) {
      setPreview(undefined);
      return;
    }

    const objectUrl = URL.createObjectURL(selectedFile);
    setPreview(objectUrl);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl);
  }, [selectedFile]);

  const onSelectFile = (e) => {
    console.log('1111', e);
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined);
      return;
    }

    console.log('e.target', e.target);

    // I've kept this example simple by using the first image instead of multiple
    setSelectedFile(e.target.files[0]);
  };

  return (
    <Fragment>
      <Container>
        <FormGroup>
          <Text>Race Pdf</Text>
        </FormGroup>
        <FormGroup>
          {!sceneIsView ? (
            <Fragment>
              <Label htmlFor="file">Select File</Label>
              <Input
                name="file"
                id="file"
                type="file"
                rules={{ required: false }}
                readOnly={readOnly}
              />
              <Error>{errors.file && 'Race Pdf is required'}</Error>
            </Fragment>
          ) : (
            race?.racePdf && (
              <Fragment>
                <Label htmlFor="file">Race Pdf</Label>
                <PdfLink href={race.racePdf.filePath} target="_blank">
                  <GrDocumentPdf />
                </PdfLink>
              </Fragment>
            )
          )}
        </FormGroup>
      </Container>
      <Container>
        <FormGroup>
          <Text>EVENT PHOTO</Text>
        </FormGroup>
        <FormGroup>
          {!sceneIsView ? (
            <Fragment>
              <Label htmlFor="photo">Select Photo</Label>
              <Input
                name="photo"
                id="photo"
                type="file"
                rules={{ required: false }}
                readOnly={readOnly}
                onChange={onSelectFile}
              />
              {selectedFile && (
                <img
                  tw="w-64 h-64 object-fill"
                  src={preview}
                  alt="thisthisthis"
                />
              )}
              <Error>{errors.photo && 'Photo File is required'}</Error>
            </Fragment>
          ) : (
            race?.raceImg && (
              <Fragment>
                <Label htmlFor="photo">Photo</Label>
                <Document href={race.raceImg.filePath} target="_blank">
                  Photo
                </Document>
              </Fragment>
            )
          )}
        </FormGroup>
      </Container>
      <FlexContainer>
        <Label htmlFor="remarks">Race Description</Label>
        <Textarea id="description" name="description" readOnly={readOnly} />
      </FlexContainer>
    </Fragment>
  );
};

export default DocumentsVerificationFields;
