/**
 * @flow
 */

import { pick, first } from 'lodash';
import { DateTime } from 'luxon';
import { parseISO, format } from 'date-fns';

import type { Race, FormData } from 'types/race';
import {
  eventLocationOptions,
  countryOptions,
  _countOptions,
  raceCodeOptions,
  raceCategoryOptions,
} from 'utils/constants';

export const mapRaceToFormData = (race: Race): FormData => {
  const raceData = pick(race, [
    'name',
    'contactPerson',
    'contactNumber',
    'description',
    'sheikhStable',
    'pvtStable',
  ]);

  const eventDate = parseISO(race.eventDate);
  const openingDate = parseISO(race.openingDate);
  const closingDate = parseISO(race.closingDate);
  console.log('racexxx', race);

  const formData = {
    eventName: race?.events?.name || '',
    eventDate: format(eventDate, 'yyyy-MM-dd HH:mm'),
    openingDate: format(openingDate, 'yyyy-MM-dd HH:mm'),
    closingDate: format(closingDate, 'yyyy-MM-dd HH:mm'),
    allowedCount: race.allowedCount,
    pledge: race.pledge,
    noticeTitle: race?.notice?.title,
    noticeBody: race?.notice?.body,
    racePdf: race?.racePdf,
    raceImg: race?.raceImg,
    sheikhStable: _countOptions.find(
      (option) => option.value === race.sheikhStable || first(_countOptions)
    ),
    pvtStable:
      _countOptions.find((option) => option.value === race.pvtStable) ||
      first(_countOptions),
    location:
      eventLocationOptions.find((option) => option.value === race.location) ||
      first(eventLocationOptions),
    country:
      countryOptions.find((option) => option.value === race.country) ||
      first(countryOptions),
    raceCodeOptions:
      raceCodeOptions.find((option) => option.value === race.raceCode) ||
      first(raceCodeOptions),
    raceCategoryOptions:
      raceCategoryOptions.find(
        (option) => option.value === race.raceCategory
      ) || first(raceCategoryOptions),
    stables: race.metadata,
    ...raceData,
  };

  return formData;
};
