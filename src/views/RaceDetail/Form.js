/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { first, isEmpty } from 'lodash';
import { useNavigate } from 'react-router-dom';

import { useRace } from 'views/Races/hook';
import { useEvent } from 'views/Events/hook';
import { useStable } from 'views/Stables/hook';
import type { FormData } from 'types/race';
import type { Option } from 'types';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import {
  eventLocationOptions,
  countryOptions,
  raceCodeOptions,
  raceCategoryOptions,
  _countOptions,
} from 'utils/constants';
import type { State } from 'reducers/types';
import type { RaceDetailState } from 'reducers/races/detail';
import type { AuthState } from 'reducers/auth';

import RegistrationDetailsFields from './Fields/RegistrationDetails';
import EventInformationFields from './Fields/EventInformation';
import DocumentsVerificationFields from './Fields/DocumentsVerification';
import StableInformationFields from './Fields/StableInformation';
import { useRaceDetail } from './hook';

const Title = tw(BaseTitle)`mb-4 text-red-600`,
  Buttons = tw.div`flex gap-2`;

type FormProps = {
  defaultValues?: FormData,
};

const Form = ({ defaultValues }: FormProps): Node => {
  const { dispatchAddRace, dispatchEditRace } = useRace();
  const { eventOptions } = useEvent();
  const { stableOptions } = useStable();
  const { data: auth } = useSelector<State, AuthState>((state) => state.auth);
  const { data: race } = useSelector<State, RaceDetailState>(
    (state) => state.races.detail
  );
  const navigate = useNavigate();
  const { sceneIsAdd, sceneIsEdit, sceneIsView, changeSceneToView } =
    useRaceDetail();

  const methods = useForm({
    defaultValues,
  });

  const onSubmit = (data) => {
    if (sceneIsAdd) {
      dispatchAddRace(data);
    } else {
      dispatchEditRace(data, race.id);
    }
  };

  const onCancel = () => {
    if (sceneIsAdd) {
      navigate('/races');
    } else if (sceneIsEdit) {
      changeSceneToView();
    }
  };

  useEffect(() => {
    if ((sceneIsView || sceneIsEdit) && !isEmpty(auth)) {
      methods.setValue('userId', auth.id);
    }
  }, [auth, methods, sceneIsView, sceneIsEdit]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(auth)) {
      methods.setValue('userId', auth.id);
    }
  }, [auth, methods, sceneIsAdd]);

  useEffect(() => {
    console.log('race.sheikhStable', race?.sheikhStable);
    if (sceneIsAdd && isEmpty(race?.sheikhStable)) {
      methods.setValue('sheikhStable', first(_countOptions));
    }

    if (sceneIsAdd && isEmpty(race?.pvtStable)) {
      methods.setValue('pvtStable', first(_countOptions));
    }
  });

  useEffect(() => {
    if ((sceneIsView || sceneIsEdit) && !isEmpty(eventOptions)) {
      methods.setValue(
        'eventName',
        eventOptions.find(({ value }) => parseInt(value) === race?.events?.id)
      );
    }
  }, [sceneIsEdit, sceneIsView, race, methods, eventOptions]);

  useEffect(() => {
    if (race && (sceneIsEdit || sceneIsView)) {
      if (race.events) {
        methods.setValue('eventName', {
          name: race?.events?.name,
          value: race?.events?.id,
        });
      }
    }
  }, [methods, race, sceneIsEdit, sceneIsView]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <Title type="h4">Events Details</Title>
        <EventInformationFields />
        <Title type="h4">Race Details</Title>
        <RegistrationDetailsFields />
        <Title type="h4">Stable Details</Title>
        <StableInformationFields />
        <Title type="h4">Race Assets</Title>
        <DocumentsVerificationFields />
        <Buttons>
          {!sceneIsView && (
            <Button type="submit" size="xs">
              {sceneIsAdd ? 'Create' : 'Update'}
            </Button>
          )}
          {(sceneIsAdd || sceneIsEdit) && (
            <Button type="button" size="xs" onClick={onCancel}>
              Cancel
            </Button>
          )}
        </Buttons>
      </form>
    </FormProvider>
  );
};

Form.defaultProps = {
  defaultValues: {
    eventName: '',
    name: '',
    allowedCount: '',
    eventDate: null,
    openingDate: null,
    closingDate: null,
    location: (first(eventLocationOptions): Option),
    country: (first(countryOptions): Option),
    sheikhStable: (first(_countOptions): Option),
    pvtStable: (first(_countOptions): Option),
    raceCode: (first(raceCodeOptions): Option),
    raceCategory: (first(raceCategoryOptions): Option),
    seasonId: 1,
    userId: '',
    description: '',
    stables: [],
  },
};

export default Form;
