/**
 * @flow
 */

import { useState, useContext, createContext, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomCompareCallback } from 'use-custom-compare';
import { isEqual } from 'lodash';

import { resetRace } from 'actions/races';
import { editRaceStatus, editRace } from 'actions/races/edit';
import { fetchRaces, fetchRace } from 'actions/races/fetch';
import { addRace } from 'actions/races/add';
import type { Race } from 'types/race';
import type { State } from 'reducers/types';
import type { RacesState } from 'reducers/races/all';
import type { Option } from 'types';
import { useAuth } from 'hooks/auth';

import { mapRacesToOptions } from './utils';

type ContextType = {
  raceOptions: Array<Option>,
  query: '',
  // setQuery: Function,
  setRaceOptions: Function,
  dispatchAddRace: Function,
  dispatchEditRace: Function,
  dispatchFetchRaces: Function,
  dispatchFetchRace: Function,
  dispatchApproveRace: Function,
  dispatchRejectRace: Function,
  dispatchResetRace: Function,
  getPledgeStatus: Function,
};

const initialValues = {
  raceOptions: [],
  query: '',
  // setQuery: () => {},
  setRaceOptions: () => {},
  dispatchAddRace: () => {},
  dispatchEditRace: () => {},
  dispatchFetchRaces: () => {},
  dispatchFetchRace: () => {},
  dispatchApproveRace: () => {},
  dispatchRejectRace: () => {},
  dispatchResetRace: () => {},
  getPledgeStatus: () => {},
};

const RaceContext = createContext<ContextType>(initialValues);

type RaceProviderProps = {
  children: ?Node,
};

const RaceProvider = ({ children }: RaceProviderProps): Node => {
  const dispatch = useDispatch();
  // const [query, setQuery] = useState(initialValues.query);
  const [raceOptions, setRaceOptions] = useState<Array<Option>>(
    initialValues.raceOptions
  );
  const {
    data: races,
    status: { fetched },
  } = useSelector<State, RacesState>((state) => state.races.all);
  // TODO: Remove selector
  const { user } = useAuth();

  // TODO: Remove auth id
  // const dispatchFetchRaces = useCallback(() => {
  //   dispatch(fetchRaces(auth.id));
  // }, [dispatch, auth]);

  const dispatchFetchRaces = useCustomCompareCallback(
    () => {
      dispatch(fetchRaces({ id: user.id }));
    },
    [dispatch, user],
    (prevDeps, nextDeps) => isEqual(prevDeps, nextDeps)
  );

  const dispatchFetchRace = (id: string) => {
    console.log('DISPATCH HOOK');
    dispatch(fetchRace(id));
  };

  const dispatchAddRace = (race: Race) => {
    dispatch(addRace(race));
  };

  const dispatchEditRace = (race: Race, id: string) => {
    console.log(race);
    dispatch(editRace(race, id));
  };

  const dispatchResetRace = () => {
    dispatch(resetRace());
  };

  const dispatchApproveRace = (id) => {
    dispatch(editRaceStatus(id, 'approved'));
  };

  const dispatchRejectRace = (id) => {
    dispatch(editRaceStatus(id, 'rejected'));
  };

  const getPledgeStatus = (id: string) => {
    if (races.length === 0) return false;
    return races.find((race) => race.id === parseInt(id)).pledge;
  };

  useEffect(() => {
    if (fetched) {
      setRaceOptions(mapRacesToOptions(races));
    }
  }, [fetched, races]);

  // useEffect(() => {
  //   if (query) {
  //     dispatchFetchRaces();
  //   }
  // }, [query, dispatchFetchRaces]);

  return (
    <RaceContext.Provider
      value={{
        raceOptions,
        setRaceOptions,
        // query,
        // setQuery,
        dispatchAddRace,
        dispatchEditRace,
        dispatchFetchRaces,
        dispatchFetchRace,
        dispatchApproveRace,
        dispatchRejectRace,
        dispatchResetRace,
        getPledgeStatus,
      }}>
      {children}
    </RaceContext.Provider>
  );
};

const useRace = (): ContextType => {
  const context = useContext(RaceContext);
  if (context === undefined) {
    throw new Error('useRace must be used within a RaceContextProvider');
  }
  return context;
};

export { RaceProvider, useRace };
