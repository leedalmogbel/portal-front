/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import {
  CheckIcon,
  XIcon,
  PencilAltIcon,
  EyeIcon,
  ClipboardIcon,
} from '@heroicons/react/solid';
import { isEmpty } from 'lodash';
import { parseISO, format } from 'date-fns';
import { GrDocumentPdf } from 'react-icons/gr';

import Status from 'components/common/Status';
import Dropdown from 'components/common/Dropdown';
import type { DropdownItem } from 'components/common/Dropdown';
import Button from 'components/common/Button';
import type { Race } from 'types/race';
import type { Option } from 'types';

const PdfLink = tw.a`text-3xl text-center cursor-pointer`;
const Poster = tw.img`object-cover h-12 w-12`;

type TableData = {
  name: Node,
  location: string,
  contactPerson: string,
  contactNumber: string,
  documents: ?Node,
  eventDate: string,
  openingDate: string,
  closingDate: string,
  events: Array,
  status: Node,
  actions: Node,
};

export const mapRacesToTableData = (
  races: Array<Race>,
  actions: Object,
  userType: string
): Array<TableData> =>
  races.map(
    ({
      id,
      name,
      allowedCount,
      description,
      contactPerson,
      contactNumber,
      events,
      eventDate,
      openingDate,
      closingDate,
      status,
    }) => ({
      id: (
        <Fragment>
          <p tw="font-bold">{id}</p>
        </Fragment>
      ),
      name: (
        <Fragment>
          <p tw="font-bold">{name}</p>
          <p tw="text-sm">Event: {events?.name}</p>
        </Fragment>
      ),
      contact: (
        <Fragment>
          <p tw="font-bold">{contactPerson}</p>
          <p tw="text-sm">{contactNumber}</p>
        </Fragment>
      ),
      // racePdf: !isEmpty(racePdf) && (
      //   <PdfLink href={racePdf.filePath} target="_blank" rel="noreferrer">
      //     <GrDocumentPdf />
      //   </PdfLink>
      // ),
      // raceImg: !isEmpty(raceImg) && (
      //   // <Poster src={racePdf.raceImg} alt="poster" />
      //   <a href={raceImg.filePath} target="_blank" rel="noreferrer">
      //     <Poster src={raceImg.filePath} alt="poster" />
      //   </a>
      // ),
      location: (
        <Fragment>
          <p tw="font-bold">{events?.location}</p>
        </Fragment>
      ),
      openingDate: (
        <Fragment>
          <p tw="">{new Date(openingDate).toLocaleDateString()}</p>
        </Fragment>
      ),
      closingDate: (
        <Fragment>
          <p tw="">{new Date(closingDate).toLocaleDateString()}</p>
        </Fragment>
      ),
      eventDate: (
        <Fragment>
          <p tw="">{new Date(eventDate).toLocaleDateString()}</p>
        </Fragment>
      ),
      status: <Status status={status} />,
      actions:
        !isEmpty(actions) && mapActionsToDropdownItems(actions, id, userType),
    })
  );

const mapActionsToDropdownItems = (actions: Object, id: string, userType) => {
  let actionItems: Array<Array<DropdownItem>> = [
    // [
    //   {
    //     label: 'Approve',
    //     onClick: actions.approve,
    //     Icon: CheckIcon,
    //   },
    //   {
    //     label: 'Reject',
    //     onClick: actions.reject,
    //     Icon: XIcon,
    //   },
    // ],
    [
      {
        label: 'Add Entry',
        onClick: actions.addEntry,
        Icon: ClipboardIcon,
      },
      {
        label: 'View Entry',
        onClick: actions.viewEntry,
        Icon: ClipboardIcon,
      },
      {
        label: 'Manage Entry',
        onClick: actions.manageEntry,
        Icon: ClipboardIcon,
      },
      {
        label: 'View',
        onClick: actions.view,
        Icon: EyeIcon,
      },
      {
        label: 'Edit',
        onClick: actions.edit,
        Icon: PencilAltIcon,
      },
    ],
  ];

  if (userType !== 'Admin') {
    actionItems = [
      // [
      //   {
      //     label: 'Approve',
      //     onClick: actions.approve,
      //     Icon: CheckIcon,
      //   },
      //   {
      //     label: 'Reject',
      //     onClick: actions.reject,
      //     Icon: XIcon,
      //   },
      // ],
      [
        {
          label: 'Add Entry',
          onClick: actions.addEntry,
          Icon: ClipboardIcon,
        },
        {
          label: 'View Entry',
          onClick: actions.viewEntry,
          Icon: ClipboardIcon,
        },
        // {
        //   label: 'View',
        //   onClick: actions.view,
        //   Icon: EyeIcon,
        // },
        // {
        //   label: 'Edit',
        //   onClick: actions.edit,
        //   Icon: PencilAltIcon,
        // },
      ],
    ];
  }

  return <Dropdown items={actionItems} id={id} />;
};

export const mapRacesToOptions = (races: Array<Race>): Array<Option> => {
  return races.map(({ name, id, eventId }) => ({
    name: `[${id}] ${name} `,
    value: id.toString(),
    event: eventId,
  }));
};
