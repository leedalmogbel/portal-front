/**
 * @flow
 */

import type { TableHeader } from 'components/common/Table';

export const tableHeader: Array<TableHeader> = [
  {
    Header: 'id',
    accessor: 'id',
  },
  {
    Header: 'name',
    accessor: 'name',
  },
  {
    Header: 'location',
    accessor: 'location',
  },
  {
    Header: 'contact',
    accessor: 'contact',
  },
  {
    Header: 'race date',
    accessor: 'eventDate',
  },
  {
    Header: 'opening date',
    accessor: 'openingDate',
  },
  {
    Header: 'closing date',
    accessor: 'closingDate',
  },
  {
    Header: 'status',
    accessor: 'status',
  },
  {
    Header: 'actions',
    accessor: 'actions',
  },
];
