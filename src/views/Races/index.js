/**
 * @flow
 */

import { Fragment, useCallback, useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useTable } from 'react-table';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useEntryDetail } from 'views/EntryDetail/hook';
import { useRaceDetail } from 'views/RaceDetail/hook';
import { useEntry } from 'views/Entries/hook';
import { useAuth } from 'hooks/auth';
import Button from 'components/common/Button';
import Title from 'components/common/Title';
import Table from 'components/common/Table';
import type { State } from 'reducers/types';
import type { RaceDetailState } from 'reducers/races/detail';
import type { RacesState } from 'reducers/races/all';

import { mapRacesToTableData } from './utils';
import { useRace } from './hook';
import { tableHeader } from './constants';

const Container = tw.div`flex mb-4 justify-between`;

const Races = (): Node => {
  const {
    dispatchFetchRace,
    dispatchFetchRaces,
    dispatchApproveRace,
    dispatchRejectRace,
    dispatchResetRace,
  } = useRace();
  const { dispatchFetchEntries, setQuery } = useEntry();
  const { canRacesApprove, canRacesReject, canRacesCreate, canRacesUpdate } =
    useAuth();
  const { data: races } = useSelector<State, RacesState>(
    (state) => state.races.all
  );
  const {
    status: { edited },
  } = useSelector<State, RaceDetailState>((state) => state.races.detail);
  const { user } = useAuth();
  const navigate = useNavigate();
  const { changeSceneToAdd, changeSceneToEdit, changeSceneToView } =
    useRaceDetail();
  const { changeSceneToAdd: changeSceneToAddEntry } = useEntryDetail();
  const columns = tableHeader;
  console.log('auth', user);
  const actions = {
    // approve: { ...(canRacesApprove ? dispatchApproveRace : {}) },
    // reject: { ...(canRacesReject ? dispatchRejectRace : {}) },
    approve: canRacesApprove ? dispatchApproveRace : {},
    reject: canRacesReject ? dispatchRejectRace : {},
    view: (id) => {
      changeSceneToView();
      navigate(`/races/${id}`);
    },
    edit: (id) => {
      changeSceneToEdit();
      navigate(`/races/${id}`);
    },
    addEntry: (id) => {
      //setQuery(`raceId=${id}`);
      //dispatchFetchEntries({ raceId: id });
      console.log('entries');
      dispatchFetchRace(id);
      changeSceneToAddEntry();
      navigate(`/entries/add?raceId=${id}`, { replace: true });
    },
    viewEntry: (id) => {
      // setQuery(id);
      // dispatchFetchEntries();
      navigate(`/entries?raceId=${id}`, { replace: true });
    },
    manageEntry: (id) => {
      // setQuery(id);
      // dispatchFetchEntries();
      navigate(`/entries/manage?raceId=${id}`, { replace: true });
    },
  };
  const data = mapRacesToTableData(races, actions, user?.userType);
  const tableInstance = useTable({ columns, data });

  const onClick = useCallback(() => {
    changeSceneToAdd();
    navigate('/races/add');
  }, [changeSceneToAdd, navigate]);

  useEffect(() => {
    dispatchFetchRaces();
  }, [dispatchFetchRaces]);

  useEffect(() => {
    if (edited) {
      dispatchResetRace();
      dispatchFetchRaces();
      toast.success('Race successfully updated!');
    }
  }, [edited, dispatchFetchRaces, dispatchResetRace]);

  return (
    <Fragment>
      <Container>
        <Title type="h1">Races</Title>
        {canRacesCreate && (
          <Button type="button" size="xs" onClick={onClick}>
            Add New Race
          </Button>
        )}
      </Container>
      <Table tableInstance={tableInstance} />
    </Fragment>
  );
};

export default Races;
