/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Select from 'components/common/Select';
import Error from 'components/common/Error';
import { locationOptions, disciplineOptions } from 'utils/constants';

const FormGroup = tw.div`mb-4`,
  TwoColumnContainer = tw.div`lg:(flex gap-10)`,
  Required = tw.span`text-red-500 font-black text-xl`;

const PersonalDetailsFields = (): Node => {
  const { errors } = useFormState();
  console.log('asdasd', errors);
  return (
    <Fragment>
      <TwoColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="firstName">
            First Name
            <Required> *</Required>
          </Label>
          <Input
            name="firstName"
            id="firstName"
            type="text"
            rules={{ required: true }}
          />
          <Error>{errors.firstName && 'First Name is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="lastName">
            Last Name
            <Required> *</Required>
          </Label>
          <Input
            name="lastName"
            id="lastName"
            type="text"
            rules={{ required: true }}
          />
          <Error>{errors.lastName && 'Last Name is required'}</Error>
        </FormGroup>
      </TwoColumnContainer>
      <TwoColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="email">
            Email
            <Required> *</Required>
          </Label>
          <Input
            name="email"
            id="email"
            type="email"
            rules={{ required: true }}
          />
          <Error>{errors.email && 'Email is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="dob">
            Date of Birth
            <Required> *</Required>
          </Label>
          <Input name="dob" id="dob" type="date" rules={{ required: true }} />
          <Error>{errors.dob && 'Date of Birth is required'}</Error>
        </FormGroup>
      </TwoColumnContainer>
      <FormGroup>
        <Label htmlFor="mobile">
          Mobile/Phone No.
          <Required> *</Required>
        </Label>
        <Input
          name="mobile"
          id="mobile"
          type="tel"
          rules={{
            required: true,
            pattern: /^(?:971|\+971|0)?(?:50|51|52|55|56|58|2|3|4|6|7|9)\d{7}$/,
          }}
        />
        <Error>
          {errors.mobile &&
            errors.mobile.type === 'required' &&
            'Mobile/Phone No. is required'}
        </Error>
        <Error>
          {errors.mobile &&
            errors.mobile.type === 'pattern' &&
            'Use 971xxxxxxxxx or 05xxxxxxxx format'}
        </Error>
      </FormGroup>
      <FormGroup>
        <Label htmlFor="location">Location</Label>
        <Select name="location" id="location" options={locationOptions} />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="discipline">Discipline</Label>
        <Select name="discipline" id="discipline" options={disciplineOptions} />
      </FormGroup>
      <FormGroup>
        <Label htmlFor="emiratesId">
          Emirates ID
          <Required> *</Required>
        </Label>
        <Input
          name="emiratesId"
          id="emiratesId"
          type="text"
          rules={{ required: true }}
        />
        <Error>
          {errors.emiratesId && 'Use the 16 digit valid Emirates ID'}
        </Error>
      </FormGroup>
    </Fragment>
  );
};

export default PersonalDetailsFields;
