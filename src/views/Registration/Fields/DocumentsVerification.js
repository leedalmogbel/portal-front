/**
 * @flow
 */

import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Error from 'components/common/Error';

const Container = tw.div`flex gap-10 items-center bg-gray-100 p-4 mb-4`,
  FormGroup = tw.div`w-3/12`,
  Text = tw.span`uppercase`;

const DocumentsVerificationFields = (): Node => {
  const { errors } = useFormState();

  return (
    <Container>
      <FormGroup>
        <Text>National ID</Text>
      </FormGroup>
      <FormGroup>
        <Label htmlFor="documentExpiry">Document Expiry</Label>
        <Input
          name="documentExpiry"
          id="documentExpiry"
          type="date"
          rules={{ required: true }}
        />
        <Error>{errors.documentExpiry && 'Document Expiry is required'}</Error>
      </FormGroup>
      <FormGroup>
        <Label htmlFor="documentExpiryFile">Select File</Label>
        <Input
          name="documentExpiryFile"
          id="documentExpiryFile"
          type="file"
          rules={{ required: true }}
        />
        <Error>
          {errors.documentExpiryFile && 'Document File is required'}
        </Error>
      </FormGroup>
    </Container>
  );
};

export default DocumentsVerificationFields;
