/**
 * @flow
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Error from 'components/common/Error';

const FormGroup = tw.div`mb-4`;

const LoginDetailsFields = (): Node => {
  const { errors } = useFormState();

  return (
    <Fragment>
      <FormGroup>
        <Label htmlFor="username">Username</Label>
        <Input
          name="username"
          id="username"
          type="text"
          rules={{ required: true }}
        />
        <Error>{errors.username && 'Username is required'}</Error>
      </FormGroup>
      <FormGroup>
        <Label htmlFor="password">Password</Label>
        <Input
          name="password"
          id="password"
          type="password"
          rules={{ required: true }}
        />
        <Error>{errors.password && 'Password is required'}</Error>
      </FormGroup>
      <FormGroup>
        <Label htmlFor="confirmPassword">Confirm Password</Label>
        <Input name="confirmPassword" id="confirmPassword" type="password" />
        <Error>{errors.confirmPassword && 'Passwords do not match'}</Error>
      </FormGroup>
    </Fragment>
  );
};

export default LoginDetailsFields;
