/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { useNavigate } from 'react-router-dom';

import { useUser } from 'views/Users/hook';
import Title from 'components/common/Title';
import type { State } from 'reducers/types';
import type { UserDetailState } from 'reducers/users/detail';

import Form from './Form';

const Registration = (): Node => {
  const { dispatchResetUser } = useUser();
  const {
    status: { added },
  } = useSelector<State, UserDetailState>((state) => state.users.detail);
  const navigate = useNavigate();

  useEffect(() => {
    return () => {
      dispatchResetUser();
    };
  }, [dispatchResetUser]);

  useEffect(() => {
    if (added) {
      toast.success('Account succssfully registered!');
      navigate('/login');
    }
  }, [added, navigate]);

  return (
    <div>
      <Title type="h2">Create an account</Title>
      <Form />
    </div>
  );
};

export default Registration;
