/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { first, isEmpty } from 'lodash';

import { useUser } from 'views/Users/hook';
import { useRoles } from 'hooks/roles';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import { locationOptions, disciplineOptions } from 'utils/constants';
import type { Option } from 'types';
import type { FormData } from 'types/user';

import PersonalDetailsFields from './Fields/PersonalDetails';
import LoginDetailsFields from './Fields/LoginDetails';
import DocumentsVerificationFields from './Fields/DocumentsVerification';

const InnerContainer = tw.div`lg:(mt-8 flex gap-10 )`,
  StyledForm = tw.form`lg:(mb-24)`,
  PersonalDetails = tw.div`lg:(w-2/3)`,
  LoginDetails = tw.div`lg:(w-1/3)`,
  Title = tw(BaseTitle)`mb-4 text-red-600`;

type FormProps = {
  defaultValues?: FormData,
};

const Form = ({ defaultValues }: FormProps): Node => {
  const { dispatchAddUser } = useUser();
  const { roleOptions } = useRoles();

  const methods = useForm({ defaultValues });

  const onSubmit = (data) => {
    dispatchAddUser(data);
  };

  useEffect(() => {
    if (!isEmpty(roleOptions)) {
      methods.setValue(
        'role',
        roleOptions.find(({ name }) => name === 'User')
      );
    }
  }, [methods, roleOptions]);

  return (
    <FormProvider {...methods}>
      <StyledForm onSubmit={methods.handleSubmit(onSubmit)}>
        <InnerContainer>
          <PersonalDetails>
            <Title type="h4">Personal Details</Title>
            <PersonalDetailsFields />
          </PersonalDetails>
          <LoginDetails>
            <Title type="h4">Login Details</Title>
            <LoginDetailsFields />
          </LoginDetails>
        </InnerContainer>
        <Title type="h4">Documents Verification</Title>
        <DocumentsVerificationFields />
        <Button type="submit" size="xs">
          Create
        </Button>
      </StyledForm>
    </FormProvider>
  );
};

Form.defaultProps = {
  defaultValues: {
    firstName: '',
    lastName: '',
    dob: null,
    username: '',
    mobile: '',
    emiratesId: '',
    location: (first(locationOptions): Option),
    discipline: (first(disciplineOptions): Option),
    email: '',
    password: '',
    confirmPassword: '',
    documentExpiry: null,
    documentExpiryFile: null,
    role: null,
  },
};

export default Form;
