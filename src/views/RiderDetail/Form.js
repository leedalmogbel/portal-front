/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { first, isEmpty } from 'lodash';
import { useNavigate } from 'react-router-dom';

import { useRider } from 'views/Riders/hook';
import { useAuth } from 'hooks/auth';
import type { FormData } from 'types/rider';
import type { Option } from 'types';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import {
  locationOptions,
  disciplineOptions,
  visaCategoryOptions,
} from 'utils/constants';
import type { State } from 'reducers/types';
import type { RiderDetailState } from 'reducers/riders/detail';
import type { AuthState } from 'reducers/auth';

import RegistrationDetailsFields from './Fields/RegistrationDetails';
import ContactInformationFields from './Fields/ContactInformation';
import DocumentsVerificationFields from './Fields/DocumentsVerification';
import { useRiderDetail } from './hook';

const Title = tw(BaseTitle)`mb-4 text-red-600`,
  Buttons = tw.div`flex gap-2`;

type FormProps = {
  defaultValues?: FormData,
};

const Form = ({ defaultValues }: FormProps): Node => {
  const { dispatchAddRider, dispatchEditRider } = useRider();
  const { user } = useAuth();
  const { data: rider } = useSelector<State, RiderDetailState>(
    (state) => state.riders.detail
  );
  const navigate = useNavigate();
  const { sceneIsAdd, sceneIsEdit, sceneIsView, changeSceneToView } =
    useRiderDetail();

  const methods = useForm({
    defaultValues,
  });

  const onSubmit = (data) => {
    if (sceneIsAdd) {
      dispatchAddRider(data);
    } else {
      dispatchEditRider(data, rider.id);
    }
  };

  const onCancel = () => {
    if (sceneIsAdd) {
      navigate('/riders');
    } else if (sceneIsEdit) {
      changeSceneToView();
    }
  };

  useEffect(() => {
    if ((sceneIsView || sceneIsEdit) && !isEmpty(user)) {
      methods.setValue('userId', user.id);
    }
  }, [user, methods, sceneIsView, sceneIsEdit]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(user)) {
      methods.setValue('userId', user.id);
    }
  }, [user, methods, sceneIsAdd]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <Title type="h4">Registration Details</Title>
        <RegistrationDetailsFields />
        <Title type="h4">Contact Information</Title>
        <ContactInformationFields />
        <Title type="h4">Documents Verification</Title>
        <DocumentsVerificationFields />
        <Buttons>
          {!sceneIsView && (
            <Button type="submit" size="xs">
              {sceneIsAdd ? 'Create' : 'Update'}
            </Button>
          )}
          {(sceneIsAdd || sceneIsEdit) && (
            <Button type="button" size="xs" onClick={onCancel}>
              Cancel
            </Button>
          )}
        </Buttons>
      </form>
    </FormProvider>
  );
};

Form.defaultProps = {
  defaultValues: {
    emiratesId: '',
    discipline: (first(disciplineOptions): Option),
    feiRegistrationNo: '',
    feiRegistrationDate: null,
    visa: (first(visaCategoryOptions): Option),
    firstName: '',
    lastName: '',
    nationality: '',
    uaeAddress: '',
    uaeCity: (first(locationOptions): Option),
    pobox: '',
    uaeCountry: { name: 'United Arab Emirates', value: 'UAE' },
    contactEmail: '',
    contactMobile: '',
    contactTelephone: '',
    homeAddress: '',
    homeCity: '',
    homeCountry: { name: 'United Arab Emirates', value: 'UAE' },
    contactTelHome: '',
    contactMobHome: '',
    documentExpiry: null,
    documentExpiryFile: null,
    userId: '',
  },
};

export default Form;
