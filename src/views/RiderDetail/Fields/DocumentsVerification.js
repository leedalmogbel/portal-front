/**
 * @flow
 */

import { Fragment, useEffect, useState } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';
import { useSelector } from 'react-redux';

import Textarea from 'components/common/Textarea';
import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Error from 'components/common/Error';
import type { State } from 'reducers/types';
import type { RiderDetailState } from 'reducers/riders/detail';

import { useRiderDetail } from '../hook';

const Container = tw.div`flex gap-10 items-center bg-gray-100 p-4 mb-4`,
  FlexContainer = tw.div`flex gap-10 mb-4`,
  FormGroup = tw.div`w-3/12`,
  Text = tw.span`uppercase`,
  Document = tw.a`font-medium text-red-600`;

const DocumentsVerificationFields = (): Node => {
  const { errors } = useFormState();
  const { data: rider } = useSelector<State, RiderDetailState>(
    (state) => state.riders.detail
  );

  const { sceneIsView } = useRiderDetail();
  const readOnly = sceneIsView;

  const [selectedFile, setSelectedFile] = useState();
  const [preview, setPreview] = useState();

  // create a preview as a side effect, whenever selected file is changed
  useEffect(() => {
    console.log(selectedFile);
    if (!selectedFile) {
      setPreview(undefined);
      return;
    }

    const objectUrl = URL.createObjectURL(selectedFile);
    setPreview(objectUrl);

    // free memory when ever this component is unmounted
    return () => URL.revokeObjectURL(objectUrl);
  }, [selectedFile]);

  const onSelectFile = (e) => {
    console.log('1111', e);
    if (!e.target.files || e.target.files.length === 0) {
      setSelectedFile(undefined);
      return;
    }

    console.log('e.target', e.target);

    // I've kept this example simple by using the first image instead of multiple
    setSelectedFile(e.target.files[0]);
  };

  return (
    <Fragment>
      <Container>
        <FormGroup>
          <Text>National ID</Text>
        </FormGroup>
        <FormGroup>
          <Label htmlFor="documentExpiry">Document Expiry</Label>
          <Input
            name="documentExpiry"
            id="documentExpiry"
            type="date"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>
            {errors.documentExpiry && 'Document Expiry is required'}
          </Error>
        </FormGroup>
        <FormGroup>
          {!sceneIsView ? (
            <Fragment>
              <Label htmlFor="documentExpiryFile">Select File</Label>
              <Input
                name="documentExpiryFile"
                id="documentExpiryFile"
                type="file"
                rules={{ required: true }}
                readOnly={readOnly}
              />
              <Error>
                {errors.documentExpiryFile && 'Document File is required'}
              </Error>
            </Fragment>
          ) : (
            rider?.documents && (
              <Fragment>
                <Label htmlFor="documentExpiryFile">Document File</Label>
                <Document href={rider.documents.filePath} target="_blank">
                  document
                </Document>
              </Fragment>
            )
          )}
        </FormGroup>
      </Container>
      <Container>
        <FormGroup>
          <Text>PHOTO</Text>
        </FormGroup>
        <FormGroup>
          {!sceneIsView ? (
            <Fragment>
              <Label htmlFor="photo">Select Photo</Label>
              <Input
                name="photo"
                id="photo"
                type="file"
                rules={{ required: true }}
                readOnly={readOnly}
                onChange={onSelectFile}
              />
              {selectedFile && <img src={preview} alt="thisthisthis" />}
              <Error>{errors.photo && 'Photo File is required'}</Error>
            </Fragment>
          ) : (
            rider?.riderImage && (
              <Fragment>
                <Label htmlFor="photo">Photo</Label>
                <Document href={rider.riderImage.filePath} target="_blank">
                  Photo
                </Document>
              </Fragment>
            )
          )}
        </FormGroup>
      </Container>
      <FlexContainer>
        <Label htmlFor="remarks">Remarks</Label>
        <Textarea id="remarks" name="remarks" readOnly={readOnly} />
      </FlexContainer>
    </Fragment>
  );
};

export default DocumentsVerificationFields;
