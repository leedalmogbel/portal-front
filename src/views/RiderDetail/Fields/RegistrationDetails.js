/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Select from 'components/common/Select';
import Error from 'components/common/Error';
import {
  disciplineOptions,
  locationOptions,
  visaCategoryOptions,
} from 'utils/constants';

import { useRiderDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`,
  OneThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc((100% - 80px) / 3)])`,
  Required = tw.span`text-red-500 font-black text-xl`;

const RegistrationDetailsFields = (): Node => {
  const { errors } = useFormState();
  const { sceneIsView } = useRiderDetail();
  const readOnly = sceneIsView;

  return (
    <Fragment>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="emiratesId">Emirates ID</Label>
          <Input
            name="emiratesId"
            id="emiratesId"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.emiratesId && 'Emirates ID is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="discipline">Discipline</Label>
          <Select
            name="discipline"
            id="discipline"
            options={disciplineOptions}
            readOnly={readOnly}
          />
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="feiRegistrationNo">FEI Registration Number</Label>
          <Input
            name="feiRegistrationNo"
            id="feiRegistrationNo"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>
            {errors.feiRegistrationNo && 'FEI Registration Number is required'}
          </Error>
        </FormGroup>
      </FlexContainer>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="feiRegistrationDate">FEI Registration Date</Label>
          <Input
            name="feiRegistrationDate"
            id="feiRegistrationDate"
            type="date"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>
            {errors.feiRegistrationDate && 'FEI Registration Date is required'}
          </Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="visa">Visa Category</Label>
          <Select
            name="visa"
            id="visa"
            options={visaCategoryOptions}
            readOnly={readOnly}
          />
        </FormGroup>
      </TwoThirdColumnContainer>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="firstName">First Name</Label>
          <Input
            name="firstName"
            id="firstName"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.firstName && 'First Name is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="lastName">Last Name</Label>
          <Input
            name="lastName"
            id="lastName"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.lastName && 'Last Name is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
      <FlexContainer>
        <FormGroup tw="lg:(w-[calc((100% - 80px) / 3)])">
          <Label htmlFor="nationality">Nationality</Label>
          <Input
            name="nationality"
            id="nationality"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.uaeAddress && 'Nationality is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="uaeAddress">Address</Label>
          <Input
            name="uaeAddress"
            id="uaeAddress"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.uaeAddress && 'Address is required'}</Error>
        </FormGroup>
      </FlexContainer>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="uaeCity">City</Label>
          <Select
            name="uaeCity"
            id="uaeCity"
            options={locationOptions}
            readOnly={readOnly}
          />
          <Error>{errors.uaeCity && 'City is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="pobox">P.O. box</Label>
          <Input
            name="pobox"
            id="pobox"
            type="text"
            rules={{ required: false }}
            readOnly={readOnly}
          />
          <Error>{errors.pobox && 'P.O. box is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="uaeCountry">Country</Label>
          <Select
            name="uaeCountry"
            id="uaeCountry"
            options={[{ name: 'United Arab Emirates', value: 'UAE' }]}
            readOnly={readOnly}
          />
          <Error>{errors.uaeCountry && 'Country is required'}</Error>
        </FormGroup>
      </FlexContainer>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="feiId">
            FEI ID
            <Required> *</Required>
          </Label>
          <Input
            name="feiId"
            id="feiId"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.feiId && 'FEI ID is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="feiId">
            EEF ID
            <Required> *</Required>
          </Label>
          <Input
            name="eefId"
            id="eefId"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.eefId && 'EEF ID is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
    </Fragment>
  );
};

export default RegistrationDetailsFields;
