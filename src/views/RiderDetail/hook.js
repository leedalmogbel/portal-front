/**
 * @flow
 */

import { useState, useContext, createContext } from 'react';
import type { Node } from 'react';

import type { Scene } from 'types';

type ContextType = {
  sceneIsAdd: boolean,
  sceneIsEdit: boolean,
  sceneIsView: boolean,
  changeSceneToAdd: Function,
  changeSceneToEdit: Function,
  changeSceneToView: Function,
};

const initialValues = {
  scene: 'view',
  sceneIsAdd: false,
  sceneIsEdit: false,
  sceneIsView: true,
  changeSceneToAdd: () => {},
  changeSceneToEdit: () => {},
  changeSceneToView: () => {},
};

const RiderContext = createContext<ContextType>(initialValues);

type RiderDetailProviderProps = {
  children: ?Node,
};

const RiderDetailProvider = ({ children }: RiderDetailProviderProps): Node => {
  const [scene, setScene] = useState<Scene>(initialValues.scene);
  const sceneIsAdd = scene === 'add',
    sceneIsEdit = scene === 'edit',
    sceneIsView = scene === 'view';

  const changeSceneToAdd = () => {
    setScene('add');
  };

  const changeSceneToEdit = () => {
    setScene('edit');
  };

  const changeSceneToView = () => {
    setScene('view');
  };

  return (
    <RiderContext.Provider
      value={{
        sceneIsAdd,
        sceneIsEdit,
        sceneIsView,
        changeSceneToAdd,
        changeSceneToEdit,
        changeSceneToView,
      }}>
      {children}
    </RiderContext.Provider>
  );
};

const useRiderDetail = (): ContextType => {
  const context = useContext(RiderContext);
  if (context === undefined) {
    throw new Error(
      'useRiderDetail must be used within a RiderContextProvider'
    );
  }
  return context;
};

export { RiderDetailProvider, useRiderDetail };
