/**
 * @flow
 */

import { pick, first } from 'lodash';
import { DateTime } from 'luxon';

import type { Rider, FormData } from 'types/rider';
import { disciplineOptions } from 'utils/constants';

export const mapRiderToFormData = (rider: Rider): FormData => {
  const riderData = pick(rider, [
    'emiratesId',
    'visa',
    'firstName',
    'lastName',
    'nationality',
    'pobox',
  ]);

  const formData = {
    feiRegistrationNo: rider.feiRegistration.no,
    feiRegistrationDate: new DateTime(rider.feiRegistration.date).toFormat(
      'yyyy-LL-dd'
    ),
    uaeAddress: rider.address.uae.address,
    uaeCity: rider.address.uae.city,
    uaeCountry: { name: 'United Arab Emirates', value: 'UAE' },
    contactEmail: rider.email,
    contactMobile: rider.contact.personal.mobile,
    contactTelephone: rider.contact.personal.telephone,
    homeAddress: rider.address.home.address,
    homeCity: rider.address.home.city,
    homeCountry: { name: 'United Arab Emirates', value: 'UAE' },
    contactTelHome: rider.contact.home.telephone,
    contactMobHome: rider.contact.home.mobile,
    documentExpiry: rider.documents
      ? new DateTime(rider.documents.documentExpiry).toFormat('yyyy-LL-dd')
      : null,
    discipline:
      disciplineOptions.find((option) => option.value === rider.discipline) ||
      first(disciplineOptions),
    ...riderData,
  };

  return formData;
};
