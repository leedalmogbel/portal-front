/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useRider } from 'views/Riders/hook';
import type { State } from 'reducers/types';
import type { RiderDetailState } from 'reducers/riders/detail';

import Form from './Form';
import { mapRiderToFormData } from './utils';

const Container = tw.div`p-8 bg-white`;

const RiderDetail = (): Node => {
  const { id } = useParams();
  const navigate = useNavigate();
  const {
    data: riderDetail,
    status: { fetching, fetched, added, edited },
  } = useSelector<State, RiderDetailState>((state) => state.riders.detail);
  const { dispatchFetchRider, dispatchResetRider } = useRider();

  useEffect(() => {
    return () => {
      dispatchResetRider();
    };
  }, [dispatchResetRider]);

  useEffect(() => {
    if (id) {
      dispatchFetchRider(id);
    }
  }, [id, dispatchFetchRider]);

  useEffect(() => {
    if (added) {
      toast.success('Rider succssfully registered!');
      navigate('/riders');
    }
  }, [added, navigate, dispatchResetRider]);

  useEffect(() => {
    if (edited) {
      // TODO: Hide toast if nothing was changed
      toast.success('Rider succssfully updated!');
      navigate('/riders');
    }
  }, [edited, navigate, dispatchResetRider]);

  return (
    <Container>
      {fetching && <div>Loading...</div>}
      {fetched && <Form defaultValues={mapRiderToFormData(riderDetail)} />}
      {!fetching && !fetched && <Form />}
    </Container>
  );
};

export default RiderDetail;
