/**
 * @flow
 */

import { Fragment, useCallback, useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useTable } from 'react-table';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useStableDetail } from 'views/StableDetail/hook';
import Button from 'components/common/Button';
import Title from 'components/common/Title';
import Table from 'components/common/Table';
import type { State } from 'reducers/types';
import type { StableDetailState } from 'reducers/stables/detail';
import type { StablesState } from 'reducers/stables/all';

import { mapStablesToTableData } from './utils';
import { useStable } from './hook';
import { tableHeader } from './constants';

const Container = tw.div`flex mb-4 justify-between`;

const Stables = (): Node => {
  const {
    dispatchFetchStables,
    dispatchApproveStable,
    dispatchRejectStable,
    dispatchResetStable,
  } = useStable();
  const { data: stables } = useSelector<State, StablesState>(
    (state) => state.stables.all
  );
  const {
    status: { edited },
  } = useSelector<State, StableDetailState>((state) => state.stables.detail);
  const navigate = useNavigate();
  const { changeSceneToAdd, changeSceneToEdit, changeSceneToView } =
    useStableDetail();
  const columns = tableHeader;

  const actions = {
    approve: dispatchApproveStable,
    reject: dispatchRejectStable,
    view: (id) => {
      changeSceneToView();
      navigate(`/stables/${id}`);
    },
    edit: (id) => {
      changeSceneToEdit();
      navigate(`/stables/${id}`);
    },
  };
  const data = mapStablesToTableData(stables, actions);
  const tableInstance = useTable({ columns, data });

  const onClick = useCallback(() => {
    changeSceneToAdd();
    navigate('/stables/add');
  }, [changeSceneToAdd, navigate]);

  useEffect(() => {
    dispatchFetchStables();
  }, [dispatchFetchStables]);

  useEffect(() => {
    if (edited) {
      dispatchResetStable();
      dispatchFetchStables();
      toast.success('Stable successfully updated!');
    }
  }, [edited, dispatchFetchStables, dispatchResetStable]);

  return (
    <Fragment>
      <Container>
        <Title type="h1">Stables</Title>
        <Button type="button" size="xs" onClick={onClick}>
          Add New Stable
        </Button>
      </Container>
      <Table tableInstance={tableInstance} />
    </Fragment>
  );
};

export default Stables;
