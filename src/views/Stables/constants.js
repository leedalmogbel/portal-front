/**
 * @flow
 */

import type { TableHeader } from 'components/common/Table';

export const tableHeader: Array<TableHeader> = [
  {
    Header: 'name',
    accessor: 'name',
  },
  {
    Header: 'user',
    accessor: 'user',
  },
  {
    Header: 'status',
    accessor: 'status',
  },
  {
    Header: 'actions',
    accessor: 'actions',
  },
];
