/**
 * @flow
 */

import { useState, useContext, createContext, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomCompareCallback } from 'use-custom-compare';
import { isEqual } from 'lodash';

import { useAuth } from 'hooks/auth';
import { resetStable } from 'actions/stables';
import { editStableStatus, editStable } from 'actions/stables/edit';
import { addStable } from 'actions/stables/add';
import { fetchStables, fetchStable } from 'actions/stables/fetch';
import type { Stable } from 'types/stable';
import type { State } from 'reducers/types';
import type { StablesState } from 'reducers/stables/all';
import type { Option } from 'types';

import { mapStablesToOptions } from './utils';

type ContextType = {
  stableOptions: Array<Option>,
  query: string,
  setQuery: Function,
  dispatchAddStable: Function,
  dispatchEditStable: Function,
  dispatchFetchStables: Function,
  dispatchFetchStable: Function,
  dispatchApproveStable: Function,
  dispatchRejectStable: Function,
  dispatchResetStable: Function,
};

const initialValues = {
  stableOptions: [],
  query: '',
  dispatchAddStable: () => {},
  dispatchEditStable: () => {},
  dispatchFetchStables: () => {},
  dispatchFetchStable: () => {},
  dispatchApproveStable: () => {},
  dispatchRejectStable: () => {},
  dispatchResetStable: () => {},
};

const StableContext = createContext<ContextType>(initialValues);

type StableProviderProps = {
  children: ?Node,
};

const StableProvider = ({ children }: StableProviderProps): Node => {
  const dispatch = useDispatch();
  const [query, setQuery] = useState(initialValues.query);
  const [stableOptions, setStableOptions] = useState<Array<Option>>(
    initialValues.stableOptions
  );
  const {
    data: stables,
    status: { fetched },
  } = useSelector<State, StablesState>((state: State) => state.stables.all);
  // TODO: Remove selector
  const { user } = useAuth();

  // TODO: Remove auth id
  const dispatchFetchStables = useCustomCompareCallback(
    () => {
      dispatch(fetchStables({ id: user.id, query }));
    },
    [dispatch, user, query],
    (prevDeps, nextDeps) => isEqual(prevDeps, nextDeps)
  );

  const dispatchFetchStable = (id: string) => {
    dispatch(fetchStable(id));
  };

  const dispatchAddStable = (stable: Stable) => {
    dispatch(addStable(stable));
  };

  const dispatchEditStable = (stable: Stable, id: string) => {
    dispatch(editStable(stable, id));
  };

  const dispatchResetStable = () => {
    dispatch(resetStable());
  };

  const dispatchApproveStable = (id) => {
    dispatch(editStableStatus(id, 'approved'));
  };

  const dispatchRejectStable = (id) => {
    dispatch(editStableStatus(id, 'rejected'));
  };

  useEffect(() => {
    if (fetched) {
      setStableOptions(mapStablesToOptions(stables));
    }
  }, [stables, fetched]);

  useEffect(() => {
    if (query) {
      dispatchFetchStables();
    }
  }, [query, dispatchFetchStables]);

  return (
    <StableContext.Provider
      value={{
        stableOptions,
        query,
        setQuery,
        dispatchAddStable,
        dispatchEditStable,
        dispatchFetchStables,
        dispatchFetchStable,
        dispatchApproveStable,
        dispatchRejectStable,
        dispatchResetStable,
      }}>
      {children}
    </StableContext.Provider>
  );
};

const useStable = (): ContextType => {
  const context = useContext(StableContext);
  if (context === undefined) {
    throw new Error('useStable must be used within a StableContextProvider');
  }
  return context;
};

export { StableProvider, useStable };
