/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import 'twin.macro';
import {
  CheckIcon,
  XIcon,
  PencilAltIcon,
  EyeIcon,
} from '@heroicons/react/solid';
import { isEmpty } from 'lodash';
import { parseISO, format } from 'date-fns';

import type { Stable } from 'types/stable';
import Status from 'components/common/Status';
import Dropdown from 'components/common/Dropdown';
import type { DropdownItem } from 'components/common/Dropdown';

type TableData = {
  name: Node,
  metadata: Node,
  eievStabledId: string,
  entryCount: string,
  status: Node,
  actions: Node,
};

export const mapStablesToTableData = (
  stables: Array<Stable>,
  actions: Object
): Array<TableData> =>
  stables.map(({ id, name, eievStableId, users, status }) => ({
    name: (
      <Fragment>
        <p tw="font-black">{name}</p>
        <p tw="text-sm">{eievStableId}</p>
      </Fragment>
    ),
    user: (
      <Fragment>
        <p tw="font-bold">
          [{users.id}] {users.firstName} {users.lastName}
        </p>
      </Fragment>
    ),
    status: <Status status={status} />,
    actions: !isEmpty(actions) && mapActionsToDropdownItems(actions, id),
  }));

const mapActionsToDropdownItems = (actions: Object, id: string) => {
  const actionItems: Array<Array<DropdownItem>> = [
    [
      {
        label: 'Approve',
        onClick: actions.approve,
        Icon: CheckIcon,
      },
      {
        label: 'Reject',
        onClick: actions.reject,
        Icon: XIcon,
      },
    ],
    [
      {
        label: 'View',
        onClick: actions.view,
        Icon: EyeIcon,
      },
      {
        label: 'Edit',
        onClick: actions.edit,
        Icon: PencilAltIcon,
      },
    ],
  ];

  return <Dropdown items={actionItems} id={id} />;
};

// TODO: Make it more reusable; Move to utils
export const mapStablesToOptions = (stables: Array<Stable>): Array<Option> => {
  return stables.map(({ id, name, stableType }) => ({
    name: `${id} ${stableType} ${name}`,
    value: id.toString(),
  }));
};
