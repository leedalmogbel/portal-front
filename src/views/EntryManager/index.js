/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment, useCallback, useEffect, useState } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useTable } from 'react-table';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { toast } from 'react-toastify';
import { ReactSortable } from 'react-sortablejs';
import { GrDocumentVerified, GrDocumentMissing } from 'react-icons/gr';

import { useEntryDetail } from 'views/EntryDetail/hook';
import Button from 'components/common/Button';
import Title from 'components/common/Title';
import Table from 'components/common/Table';
import type { State } from 'reducers/types';
import type { EntryDetailState } from 'reducers/entries/detail';
import type { EntriesState } from 'reducers/entries/all';
import { useAuth } from 'hooks/auth';

import { mapEntriesToTableData } from './utils';
import { useEntry } from './hook';
import { tableHeader } from './constants';

const Container = tw.div`flex mb-4 justify-between`;

const EntryManager = (): Node => {
  const { permissions } = useAuth();
  const [searchParams] = useSearchParams();
  const {
    query,
    setQuery,
    dispatchManageEntries,
    dispatchApproveEntry,
    dispatchRejectEntry,
    dispatchResetEntry,
    dispatchSaveEntry,
  } = useEntry();
  const { data: entries, status } = useSelector<State, EntriesState>(
    (state) => state.entries.all
  );
  const {
    status: { edited },
  } = useSelector<State, EntryDetailState>((state) => state.entries.detail);
  const navigate = useNavigate();
  const { changeSceneToAdd, changeSceneToEdit, changeSceneToView } =
    useEntryDetail();
  const columns = tableHeader;
  const [entriesZ, setEntries] = useState([]);

  const actions = {
    // approve: permissions?.users.includes('approve')
    // ? dispatchApproveEntry
    // : () => {},
    // ...(useCanApprove && { approve: dispatchApproveEntry }),
    approve: dispatchApproveEntry,
    reject: dispatchRejectEntry,
    view: (id) => {
      changeSceneToView();
      navigate(`/entries/${id}`);
    },
    edit: (id) => {
      changeSceneToEdit();
      navigate(`/entries/${id}`);
    },
  };
  //   const data = mapEntriesToTableData(entries, actions);
  //   const tableInstance = useTable({ columns, data });

  const onClick = useCallback(() => {
    dispatchSaveEntry(searchParams.get('raceId'), entriesZ);
    dispatchResetEntry();
    dispatchManageEntries();
  }, [
    dispatchManageEntries,
    dispatchResetEntry,
    dispatchSaveEntry,
    searchParams,
    entriesZ,
  ]);

  useEffect(() => {
    // console.log('zzz', entriesZ);
    console.log('status', status);
    if (status.fetched) {
      setEntries(entries);
    }
  }, [entries, status]);

  useEffect(() => {
    setQuery(searchParams.get('raceId'));
    dispatchResetEntry();
    dispatchManageEntries();
  }, [searchParams, setQuery, dispatchManageEntries, dispatchResetEntry]);

  useEffect(() => {
    if (edited) {
      dispatchResetEntry();
      dispatchManageEntries();
      toast.success('Entry successfully updated!');
    }
  }, [edited, dispatchManageEntries, dispatchResetEntry]);

  // useCallback(() => {
  //   console.log('kwelskelws', entriesZ);
  // }, [entriesZ]);

  // TODO: FETCH BASED IN RACEID, IF NOT, FETCH ALL
  // useEffect(() => {
  //   if (searchParams.get('raceId')) {
  //   setQuery('raceId', searchParams.get('raceId'));
  //   dispatchResetEntry();
  //   dispatchManageEntries();
  // }
  //   console.log('zzzzz', query);
  // }, [dispatchManageEntries, query, dispatchResetEntry]);
  // useEffect(() => {
  //   console.log('searchParams.get(raceId)', searchParams.get('raceId'));

  //   // if (searchParams.get('raceId')) {
  //   console.log('lewls');
  //   setQuery(searchParams.get('raceId'));
  //   dispatchResetEntry();
  //   dispatchManageEntries();
  //   // }
  // }, [setQuery, dispatchManageEntries, searchParams, dispatchResetEntry]);
  const PdfLink = tw.a`text-3xl text-center cursor-pointer`;
  return (
    <Fragment>
      <Container>
        <Title type="h1">Entries</Title>
        <div tw="flex gap-5">
          <PdfLink
            href={`http://localhost:7331/api/entries/${searchParams.get(
              'raceId'
            )}/accepted/exportPdf`}
            target="_blank"
            alt="accepted">
            <GrDocumentVerified />
          </PdfLink>
          <PdfLink
            href={`http://localhost:7331/api/entries/${searchParams.get(
              'raceId'
            )}/pending/exportPdf`}
            target="_blank"
            alt="pending">
            <GrDocumentMissing />
          </PdfLink>
          <Button type="button" size="xs" onClick={onClick}>
            Save List
          </Button>
        </div>
      </Container>
      {/* <Table tableInstance={tableInstance} /> */}
      {entriesZ?.length > 0 && status.fetched && (
        <ReactSortable list={entriesZ} setList={setEntries}>
          {entriesZ.map((entry, i) => (
            <Fragment key={entry.id}>
              <div tw="flex">
                <div tw="block p-6 max-w-sm">
                  <h5 tw="text-gray-900 text-xl leading-tight mb-2">{i + 1}</h5>
                </div>
                <div tw="w-1/2 bg-white m-2 cursor-pointer border border-gray-400 rounded-lg hover:shadow-md hover:border-opacity-0 transform hover:-translate-y-1 transition-all duration-200">
                  <div tw="m-1">
                    <h2 tw="text-lg mb-1">
                      {`${entry.id} - ${entry?.users?.stables?.name}`}
                      <span tw="text-sm text-blue-800 font-mono bg-blue-100 inline rounded-full px-2 align-top float-right animate-pulse">
                        {entry.sequence ? entry.sequence : 0}
                      </span>
                    </h2>
                    <p tw="font-light font-mono text-sm text-gray-700 hover:text-gray-900 transition-all duration-200">
                      {entry.horses.name}
                    </p>
                    <p tw="font-light font-mono text-sm text-gray-700 hover:text-gray-900 transition-all duration-200">
                      {entry.riders.firstName} {entry.riders.lastName}
                    </p>
                    {/* <p tw="font-light font-mono text-sm text-gray-700 hover:text-gray-900 transition-all duration-200">`${entry.horses.name}`</p> */}
                  </div>
                </div>
              </div>
            </Fragment>
          ))}
        </ReactSortable>
      )}
    </Fragment>
  );
};

export default EntryManager;
