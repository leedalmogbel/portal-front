/**
 * @flow
 */

import type { TableHeader } from 'components/common/Table';

export const tableHeader: Array<TableHeader> = [
  {
    Header: 'id',
    accessor: 'id',
  },
  {
    Header: 'race',
    accessor: 'race',
  },
  {
    Header: 'horses',
    accessor: 'horses',
  },
  {
    Header: 'athletes',
    accessor: 'athletes',
  },
  {
    Header: 'status',
    accessor: 'status',
  },
  {
    Header: 'actions',
    accessor: 'actions',
  },
];
