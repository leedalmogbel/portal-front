/**
 * @flow
 */

import { useState, useContext, createContext, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomCompareCallback } from 'use-custom-compare';
import { isEqual } from 'lodash';

import { resetEntry } from 'actions/entries';
import {
  editEntryStatus,
  editEntry,
  editManageEntry,
} from 'actions/entries/edit';
import { addEntry } from 'actions/entries/add';
import {
  fetchManageEntries,
  fetchEntries,
  fetchEntry,
} from 'actions/entries/fetch';
import type { Entry } from 'types/entry';
import type { State } from 'reducers/types';
import type { AuthState } from 'reducers/auth';
import { useAuth } from 'hooks/auth';

type ContextType = {
  query: string,
  setQuery: string,
  dispatchAddEntry: Function,
  dispatchEditEntry: Function,
  dispatchManageEntries: Function,
  dispatchFetchEntry: Function,
  dispatchApproveEntry: Function,
  dispatchRejectEntry: Function,
  dispatchResetEntry: Function,
  dispatchSaveEntry: Function,
};

const initialValues = {
  query: '',
  setQuery: '',
  dispatchAddEntry: () => {},
  dispatchEditEntry: () => {},
  dispatchManageEntries: () => {},
  dispatchFetchEntry: () => {},
  dispatchApproveEntry: () => {},
  dispatchRejectEntry: () => {},
  dispatchResetEntry: () => {},
  dispatchSaveEntry: () => {},
};

const EntryContext = createContext<ContextType>(initialValues);

type EntryManagerProviderProps = {
  children: ?Node,
};

const EntryManagerProvider = ({
  children,
}: EntryManagerProviderProps): Node => {
  const dispatch = useDispatch();
  const [query, setQuery] = useState(initialValues.query);
  // TODO: Remove selector
  const { user } = useAuth();

  // TODO: Remove auth id
  const dispatchManageEntries = useCustomCompareCallback(
    () => {
      console.log('zzzxczxc111111', query);
      dispatch(fetchManageEntries({ id: user.id, query }));
    },
    [dispatch, user, query],
    (prevDeps, nextDeps) => isEqual(prevDeps, nextDeps)
  );

  const dispatchFetchEntry = (id: string) => {
    dispatch(fetchEntry(id));
  };

  const dispatchAddEntry = (entry: Entry) => {
    dispatch(addEntry(entry));
  };

  const dispatchEditEntry = (entry: Entry, id: string) => {
    dispatch(editEntry(entry, id));
  };

  const dispatchResetEntry = () => {
    dispatch(resetEntry());
  };

  const dispatchApproveEntry = (id) => {
    dispatch(editEntryStatus(id, 'approved'));
  };

  const dispatchRejectEntry = (id) => {
    dispatch(editEntryStatus(id, 'rejected'));
  };

  const dispatchSaveEntry = (id, entries) => {
    dispatch(editManageEntry(id, entries));
  };

  return (
    <EntryContext.Provider
      value={{
        query,
        setQuery,
        dispatchAddEntry,
        dispatchEditEntry,
        dispatchManageEntries,
        dispatchFetchEntry,
        dispatchApproveEntry,
        dispatchRejectEntry,
        dispatchResetEntry,
        dispatchSaveEntry,
      }}>
      {children}
    </EntryContext.Provider>
  );
};

const useEntry = (): ContextType => {
  const context = useContext(EntryContext);
  if (context === undefined) {
    throw new Error('useEntry must be used within a EntryContextProvider');
  }
  return context;
};

export { EntryManagerProvider, useEntry };
