/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import {
  CheckIcon,
  XIcon,
  PencilAltIcon,
  EyeIcon,
} from '@heroicons/react/solid';
import { isEmpty } from 'lodash';
import { GrDocumentPdf } from 'react-icons/gr';

import type { User } from 'types/user';
import Status from 'components/common/Status';
import Dropdown from 'components/common/Dropdown';
import type { DropdownItem } from 'components/common/Dropdown';

type TableData = {
  name: Node,
  email: string,
  documents: ?Node,
  stable: ?string,
  role: string,
  status: Node,
  actions: Node,
};

const PdfLink = tw.a`text-3xl text-center cursor-pointer`;

export const mapUsersToTableData = (
  users: Array<User>,
  actions: Object
): Array<TableData> =>
  users.map(
    ({
      id,
      firstName,
      lastName,
      username,
      email,
      stableName,
      userType,
      status,
      documents,
    }) => ({
      name: (
        <Fragment>
          <p tw="font-bold">
            {firstName} {lastName}
          </p>
          <span tw="text-sm text-gray-500">@{username}</span>
        </Fragment>
      ),
      email,
      documents: !isEmpty(documents) && (
        <PdfLink href={documents.filePath} target="_blank" rel="noreferrer">
          <GrDocumentPdf />
        </PdfLink>
      ),
      stable: stableName,
      role: userType.toUpperCase(),
      status: <Status status={status} />,
      actions: !isEmpty(actions) && mapActionsToDropdownItems(actions, id),
    })
  );

const mapActionsToDropdownItems = (actions: Object, id: string) => {
  const actionItems: Array<Array<DropdownItem>> = [
    [
      {
        label: 'Approve',
        onClick: actions.approve,
        Icon: CheckIcon,
      },
      {
        label: 'Reject',
        onClick: actions.reject,
        Icon: XIcon,
      },
    ],
    [
      {
        label: 'View',
        onClick: actions.view,
        Icon: EyeIcon,
      },
      {
        label: 'Edit',
        onClick: actions.edit,
        Icon: PencilAltIcon,
      },
    ],
  ];

  return <Dropdown items={actionItems} id={id} />;
};

export const mapUsersToOptions = (users: Array<User>): Array<Option> => {
  console.log('asdasdasdasd', users);
  return users.map(({ firstName, lastName, id }) => ({
    name: `[${id}] ${firstName} ${lastName} `,
    value: id.toString(),
  }));
};
