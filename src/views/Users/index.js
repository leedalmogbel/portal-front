/**
 * @flow
 */

import { Fragment, useEffect, useCallback } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useTable } from 'react-table';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useUserDetail } from 'views/UserDetail/hook';
import Table from 'components/common/Table';
import Title from 'components/common/Title';
import Button from 'components/common/Button';
import type { State } from 'reducers/types';
import type { UsersState } from 'reducers/users/all';
import type { UserDetailState } from 'reducers/users/detail';
import { useAuth } from 'hooks/auth';

import { mapUsersToTableData } from './utils';
import { useUser } from './hook';
import { tableHeader } from './constants';

const Container = tw.div`flex mb-4 justify-between`;

const Users = (): Node => {
  const { canUsersCreate } = useAuth();
  const {
    dispatchFetchUsers,
    dispatchApproveUser,
    dispatchRejectUser,
    dispatchResetUser,
  } = useUser();
  const { data: users } = useSelector<State, UsersState>(
    (state) => state.users.all
  );
  const {
    status: { edited },
  } = useSelector<State, UserDetailState>((state) => state.users.detail);
  const navigate = useNavigate();
  const { changeSceneToAdd, changeSceneToEdit, changeSceneToView } =
    useUserDetail();

  const columns = tableHeader;
  const actions = {
    approve: dispatchApproveUser,
    reject: dispatchRejectUser,
    view: (id) => {
      changeSceneToView();
      navigate(`/users/${id}`);
    },
    edit: (id) => {
      changeSceneToEdit();
      navigate(`/users/${id}`);
    },
  };
  const data = mapUsersToTableData(users, actions);
  const tableInstance = useTable({ columns, data });

  const onClick = useCallback(() => {
    changeSceneToAdd();
    navigate('/users/add');
  }, [changeSceneToAdd, navigate]);

  useEffect(() => {
    dispatchFetchUsers();
  }, [dispatchFetchUsers]);

  useEffect(() => {
    if (edited) {
      dispatchResetUser();
      dispatchFetchUsers();
      toast.success('Account successfully updated!');
    }
  }, [edited, dispatchFetchUsers, dispatchResetUser]);

  return (
    <Fragment>
      <Container>
        <Title type="h1">Users</Title>
        <Button type="button" size="xs" onClick={onClick}>
          Add New User
        </Button>
      </Container>
      <Table tableInstance={tableInstance} />
    </Fragment>
  );
};

export default Users;
