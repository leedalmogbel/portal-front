/**
 * @flow
 */

import type { TableHeader } from 'components/common/Table';

export const tableHeader: Array<TableHeader> = [
  {
    Header: 'name',
    accessor: 'name',
  },
  {
    Header: 'email',
    accessor: 'email',
  },
  {
    Header: 'documents',
    accessor: 'documents',
  },
  {
    Header: 'stable',
    accessor: 'stable',
  },
  {
    Header: 'role',
    accessor: 'role',
  },
  {
    Header: 'status',
    accessor: 'status',
  },
  {
    Header: 'actions',
    accessor: 'actions',
  },
];
