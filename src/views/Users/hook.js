/**
 * @flow
 */

import { useContext, createContext, useState, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomCompareCallback } from 'use-custom-compare';
import { isEqual } from 'lodash';

import { useAuth } from 'hooks/auth';
import { resetUser } from 'actions/users';
import { editUserStatus, editUser } from 'actions/users/edit';
import { fetchUsers, fetchUserDetail } from 'actions/users/fetch';
import { addUser } from 'actions/users/add';
import type { User } from 'types/user';

import { mapUsersToOptions } from './utils';

type ContextType = {
  useroptions: Array<Option>,
  dispatchAddUser: Function,
  dispatchEditUser: Function,
  dispatchFetchUsers: Function,
  dispatchFetchUser: Function,
  dispatchApproveUser: Function,
  dispatchRejectUser: Function,
  dispatchResetUser: Function,
};

const initialValues = {
  userOptions: [],
  dispatchAddUser: () => {},
  dispatchEditUser: () => {},
  dispatchFetchUsers: () => {},
  dispatchFetchUser: () => {},
  dispatchApproveUser: () => {},
  dispatchRejectUser: () => {},
  dispatchResetUser: () => {},
};

const UserContext = createContext<ContextType>(initialValues);

type UserProviderProps = {
  children: ?Node,
};

const UserProvider = ({ children }: UserProviderProps): Node => {
  const dispatch = useDispatch();
  const [userOptions, setUserOptions] = useState<Array<Option>>(
    initialValues.userOptions
  );
  const {
    data: users,
    status: { fetched },
  } = useSelector<State, UsersState>((state) => state.users.all);
  const { user } = useAuth();

  // const dispatchFetchUsers = () => {
  //   dispatch(fetchUsers());
  // };

  const dispatchFetchUsers = useCustomCompareCallback(
    () => {
      dispatch(fetchUsers({ id: user.id }));
    },
    [dispatch, user],
    (prevDeps, nextDeps) => isEqual(prevDeps, nextDeps)
  );

  const dispatchFetchUser = (id: string) => {
    dispatch(fetchUserDetail(id));
  };

  const dispatchAddUser = (user: User) => {
    dispatch(addUser(user));
  };

  const dispatchEditUser = (user: User, id: string) => {
    dispatch(editUser(user, id));
  };

  const dispatchResetUser = () => {
    dispatch(resetUser());
  };

  const dispatchApproveUser = (id) => {
    dispatch(editUserStatus(id, 'approved'));
  };

  const dispatchRejectUser = (id) => {
    dispatch(editUserStatus(id, 'rejected'));
  };

  useEffect(() => {
    if (fetched) {
      setUserOptions(mapUsersToOptions(users));
    }
  }, [fetched, users]);

  return (
    <UserContext.Provider
      value={{
        userOptions,
        setUserOptions,
        dispatchAddUser,
        dispatchEditUser,
        dispatchFetchUsers,
        dispatchFetchUser,
        dispatchApproveUser,
        dispatchRejectUser,
        dispatchResetUser,
      }}>
      {children}
    </UserContext.Provider>
  );
};

const useUser = (): ContextType => {
  const context = useContext(UserContext);
  if (context === undefined) {
    throw new Error('useUser must be used within a UserContextProvider');
  }
  return context;
};

export { UserProvider, useUser };
