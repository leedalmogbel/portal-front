/**
 * @flow
 */

import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

import { useAuth } from 'hooks/auth';
import BaseButton from 'components/common/Button';

import Fields from './Fields';

const StyledForm = tw.form``,
  Button = tw(BaseButton)`mt-4`;

const Form = (): Node => {
  const { login } = useAuth();
  const methods = useForm({
    defaultValues: {
      username: '',
      password: '',
    },
  });

  const onSubmit = (data) => {
    login(data);
  };

  return (
    <FormProvider {...methods}>
      <StyledForm onSubmit={methods.handleSubmit(onSubmit)}>
        <Fields />
        <Button type="submit" size="xs">
          Login my account
        </Button>
      </StyledForm>
    </FormProvider>
  );
};

export default Form;
