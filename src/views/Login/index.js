/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import { useNavigate, Link as BaseLink } from 'react-router-dom';
import tw from 'twin.macro';
import { AiOutlineUserAdd } from 'react-icons/ai';

import { useAuth } from 'hooks/auth';
import { useNotification } from 'hooks/notification';
import BaseTitle from 'components/common/Title';
import BaseDivider from 'components/common/Divider';
import BaseButton from 'components/common/Button';
import BaseSocialMedia from 'components/SocialMedia';

import Form from './Form';

const Container = tw.div`lg:(flex gap-10)`,
  FormContainer = tw.div`mb-16 lg:(mb-0 pr-10 flex-1 border-r border-r-gold-400)`,
  SocialMedia = tw(BaseSocialMedia)`lg:(flex-1)`,
  Title = tw(BaseTitle)`mb-8`,
  Divider = tw(BaseDivider)`mt-16 mb-4 uppercase text-xs`,
  Button = tw(BaseButton)`m-auto`,
  LinkWrapper = tw.div`text-center`,
  Link = tw(BaseLink)`inline-block mt-4 text-xs uppercase`;

const Login = (): Node => {
  const { authenticated } = useAuth();
  const navigate = useNavigate();
  const { dispatchFetchNotification } = useNotification();

  useEffect(() => {
    if (authenticated) {
      navigate('/dashboard');
      dispatchFetchNotification();
    }
  }, [authenticated, navigate, dispatchFetchNotification]);

  return (
    <Container>
      <FormContainer>
        <Title type="h2">Login into your account</Title>
        <Form />
        <Divider text="Don't have an account yet" />
        <Button onClick={() => navigate('/registration')}>
          <AiOutlineUserAdd tw="mr-1" /> Create an account
        </Button>
        <LinkWrapper>
          <Link to="/forgot-password">Forgot Password</Link>
        </LinkWrapper>
      </FormContainer>
      <SocialMedia />
    </Container>
  );
};

export default Login;
