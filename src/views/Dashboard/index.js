/**
 * @flow
 */

import { Fragment, useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { GiHorseHead, GiCowboyBoot } from 'react-icons/gi';
import { FaHatCowboy, FaUserTie } from 'react-icons/fa';

import QuickLinks from 'components/common/QuickLinks';
import BaseStatWidget from 'components/common/widgets/Stat';
import type { StatProps } from 'components/common/widgets/Stat';
import Title from 'components/common/Title';
import { quickLinks } from 'components/constants';
import { useAuth } from 'hooks/auth';
import type { DashboardState } from 'reducers/dashboard';
import type { State } from 'reducers/types';

import { useDashboard } from './hook';

const Highlight = tw.span`text-gold-400`;
const StatsContainer = tw.div`flex flex-col flex-wrap justify-between mt-4 lg:(flex-row my-8)`,
  StatWidget = tw(BaseStatWidget)`mb-4 lg:(flex-basis[23%] mb-0) bg-white`;
const QuickLinksContainer = tw.div`flex flex-wrap justify-between`;

const Dashboard = (): Node => {
  const { user } = useAuth();
  const { dispatchFetchDashboard } = useDashboard();
  const { data: dashboard } = useSelector<State, DashboardState>(
    (state) => state.dashboard
  );

  useEffect(() => {
    dispatchFetchDashboard();
  }, [dispatchFetchDashboard]);

  const stats: Array<StatProps> = [
    {
      label: 'Riders',
      count: dashboard?.riders,
      Icon: GiCowboyBoot,
    },
    {
      label: 'Trainers',
      count: dashboard?.trainers,
      Icon: FaHatCowboy,
    },
    {
      label: 'Horses',
      count: dashboard?.horses,
      Icon: GiHorseHead,
    },
    {
      label: 'Owners',
      count: dashboard?.owners,
      Icon: FaUserTie,
    },
  ];

  return (
    <Fragment>
      <Title type="h1">
        Hello,{' '}
        <Highlight>
          {user?.firstName} {user?.lastName}
        </Highlight>
      </Title>
      <StatsContainer>
        {stats.map((statProps, index) => (
          <StatWidget key={index} {...statProps} />
        ))}
      </StatsContainer>
      <QuickLinksContainer>
        {quickLinks.map(({ title, Icon, items }) => (
          <QuickLinks key={title} title={title} Icon={Icon} items={items} />
        ))}
      </QuickLinksContainer>
    </Fragment>
  );
};

export default Dashboard;
