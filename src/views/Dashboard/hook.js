/**
 * @flow
 */

import { useContext, createContext } from 'react';
import type { Node } from 'react';
import { useDispatch } from 'react-redux';

import { resetDashboard } from 'actions/dashboard';
import { fetchDashboard } from 'actions/dashboard/fetch';
import { fetchNotification } from 'actions/notification/fetch';
import { useAuth } from 'hooks/auth';

type ContextType = {
  dispatchFetchDashboard: Function,
  dispatchResetDashboard: Function,
};

const initialValues = {
  dispatchFetchDashboard: () => {},
  dispatchResetDashboard: () => {},
};

const DashboardContext = createContext<ContextType>(initialValues);

type DashboardProviderProps = {
  children: ?Node,
};

const DashboardProvider = ({ children }: DashboardProviderProps): Node => {
  const dispatch = useDispatch();
  // TODO: Remove selector
  const { user } = useAuth();

  // TODO: Remove auth id
  const dispatchFetchDashboard = () => {
    console.log('oiiijqwjeqjwe');
    dispatch(fetchDashboard(user.id));
    dispatch(fetchNotification(user.id));
  };

  const dispatchResetDashboard = () => {
    dispatch(resetDashboard());
  };

  return (
    <DashboardContext.Provider
      value={{
        dispatchFetchDashboard,
        dispatchResetDashboard,
      }}>
      {children}
    </DashboardContext.Provider>
  );
};

const useDashboard = (): ContextType => {
  const context = useContext(DashboardContext);
  if (context === undefined) {
    throw new Error(
      'useDashboard must be used within a DashboardContextProvider'
    );
  }
  return context;
};

export { DashboardProvider, useDashboard };
