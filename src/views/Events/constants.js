/**
 * @flow
 */

import type { TableHeader } from 'components/common/Table';

export const tableHeader: Array<TableHeader> = [
  {
    Header: 'name',
    accessor: 'name',
  },
  {
    Header: 'location',
    accessor: 'location',
  },
  {
    Header: 'start date',
    accessor: 'startDate',
  },
  {
    Header: 'end date',
    accessor: 'endDate',
  },
  {
    Header: 'status',
    accessor: 'status',
  },
  {
    Header: 'actions',
    accessor: 'actions',
  },
];
