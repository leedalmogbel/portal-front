/**
 * @flow
 */

import { Fragment, useCallback, useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useTable } from 'react-table';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useEventDetail } from 'views/EventDetail/hook';
import Button from 'components/common/Button';
import Title from 'components/common/Title';
import Table from 'components/common/Table';
import type { State } from 'reducers/types';
import type { EventDetailState } from 'reducers/events/detail';
import type { EventsState } from 'reducers/events/all';

import { mapEventsToTableData } from './utils';
import { useEvent } from './hook';
import { tableHeader } from './constants';

const Container = tw.div`flex mb-4 justify-between`;

const Events = (): Node => {
  const {
    dispatchFetchEvents,
    dispatchApproveEvent,
    dispatchRejectEvent,
    dispatchResetEvent,
  } = useEvent();
  const { data: events } = useSelector<State, EventsState>(
    (state) => state.events.all
  );
  const {
    status: { edited },
  } = useSelector<State, EventDetailState>((state) => state.events.detail);
  const navigate = useNavigate();
  const { changeSceneToAdd, changeSceneToEdit, changeSceneToView } =
    useEventDetail();
  const columns = tableHeader;

  const actions = {
    approve: dispatchApproveEvent,
    reject: dispatchRejectEvent,
    view: (id) => {
      changeSceneToView();
      navigate(`/events/${id}`);
    },
    edit: (id) => {
      changeSceneToEdit();
      navigate(`/events/${id}`);
    },
  };
  const data = mapEventsToTableData(events, actions);
  const tableInstance = useTable({ columns, data });

  const onClick = useCallback(() => {
    changeSceneToAdd();
    navigate('/events/add');
  }, [changeSceneToAdd, navigate]);

  useEffect(() => {
    dispatchFetchEvents();
  }, [dispatchFetchEvents]);

  useEffect(() => {
    if (edited) {
      dispatchResetEvent();
      dispatchFetchEvents();
      toast.success('Event successfully updated!');
    }
  }, [edited, dispatchFetchEvents, dispatchResetEvent]);

  return (
    <Fragment>
      <Container>
        <Title type="h1">Events</Title>
        <Button type="button" size="xs" onClick={onClick}>
          Add New Event
        </Button>
      </Container>
      <Table tableInstance={tableInstance} />
    </Fragment>
  );
};

export default Events;
