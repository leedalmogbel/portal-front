/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import 'twin.macro';
import {
  CheckIcon,
  XIcon,
  PencilAltIcon,
  EyeIcon,
} from '@heroicons/react/solid';
import { isEmpty } from 'lodash';
import { parseISO, format } from 'date-fns';

import type { Event } from 'types/event';
import Status from 'components/common/Status';
import Dropdown from 'components/common/Dropdown';
import type { DropdownItem } from 'components/common/Dropdown';

type TableData = {
  name: Node,
  location: string,
  contactPerson: string,
  documents: ?Node,
  eventDate: string,
  openingDate: string,
  closingDate: string,
  status: Node,
  actions: Node,
};

export const mapEventsToTableData = (
  events: Array<Event>,
  actions: Object
): Array<TableData> =>
  events.map(
    ({
      id,
      name,
      country,
      countryCode,
      location,
      description,
      startDate,
      endDate,
      status,
    }) => ({
      name: (
        <Fragment>
          <p tw="font-black">{name}</p>
          <p tw="text-sm">{description}</p>
        </Fragment>
      ),
      location: (
        <Fragment>
          <p tw="font-bold">{location}</p>
          <p tw="text-sm text-gray-500">{country}</p>
          <p tw="text-sm text-gray-500">{countryCode}</p>
        </Fragment>
      ),
      startDate: (
        <Fragment>
          <p tw="">{new Date(startDate).toLocaleDateString()}</p>
        </Fragment>
      ),
      endDate: (
        <Fragment>
          <p tw="">{new Date(endDate).toLocaleDateString()}</p>
        </Fragment>
      ),
      status: <Status status={status} />,
      actions: !isEmpty(actions) && mapActionsToDropdownItems(actions, id),
    })
  );

const mapActionsToDropdownItems = (actions: Object, id: string) => {
  const actionItems: Array<Array<DropdownItem>> = [
    [
      {
        label: 'Approve',
        onClick: actions.approve,
        Icon: CheckIcon,
      },
      {
        label: 'Reject',
        onClick: actions.reject,
        Icon: XIcon,
      },
    ],
    [
      {
        label: 'View',
        onClick: actions.view,
        Icon: EyeIcon,
      },
      {
        label: 'Edit',
        onClick: actions.edit,
        Icon: PencilAltIcon,
      },
    ],
  ];

  return <Dropdown items={actionItems} id={id} />;
};

// TODO: Make it more reusable; Move to utils
export const mapEventsToOptions = (events: Array<Event>): Array<Option> => {
  return events.map(({ id, name }) => ({
    name: `${id} ${name}`,
    value: id.toString(),
  }));
};
