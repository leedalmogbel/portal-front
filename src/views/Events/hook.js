/**
 * @flow
 */

import { useState, useContext, createContext, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomCompareCallback } from 'use-custom-compare';
import { isEqual } from 'lodash';

import { resetEvent } from 'actions/events';
import { editEventStatus, editEvent } from 'actions/events/edit';
import { addEvent } from 'actions/events/add';
import { fetchEvents, fetchEvent } from 'actions/events/fetch';
import type { Event } from 'types/event';
import type { State } from 'reducers/types';
import type { EventsState } from 'reducers/events/all';
import type { Option } from 'types';
import { useAuth } from 'hooks/auth';

import { mapEventsToOptions } from './utils';

type ContextType = {
  eventOptions: Array<Option>,
  query: string,
  setQuery: Function,
  dispatchAddEvent: Function,
  dispatchEditEvent: Function,
  dispatchFetchEvents: Function,
  dispatchFetchEvent: Function,
  dispatchApproveEvent: Function,
  dispatchRejectEvent: Function,
  dispatchResetEvent: Function,
};

const initialValues = {
  eventOptions: [],
  query: '',
  dispatchAddEvent: () => {},
  dispatchEditEvent: () => {},
  dispatchFetchEvents: () => {},
  dispatchFetchEvent: () => {},
  dispatchApproveEvent: () => {},
  dispatchRejectEvent: () => {},
  dispatchResetEvent: () => {},
};

const EventContext = createContext<ContextType>(initialValues);

type EventProviderProps = {
  children: ?Node,
};

const EventProvider = ({ children }: EventProviderProps): Node => {
  const dispatch = useDispatch();
  const [query, setQuery] = useState(initialValues.query);
  const [eventOptions, setEventOptions] = useState<Array<Option>>(
    initialValues.eventOptions
  );
  const {
    data: events,
    status: { fetched },
  } = useSelector<State, EventsState>((state: State) => state.events.all);
  // TODO: Remove selector
  const { user } = useAuth();

  // TODO: Remove auth id
  const dispatchFetchEvents = useCustomCompareCallback(
    () => {
      dispatch(fetchEvents({ id: user.id, query }));
    },
    [dispatch, user, query],
    (prevDeps, nextDeps) => isEqual(prevDeps, nextDeps)
  );

  const dispatchFetchEvent = (id: string) => {
    dispatch(fetchEvent(id));
  };

  const dispatchAddEvent = (event: Event) => {
    dispatch(addEvent(event));
  };

  const dispatchEditEvent = (event: Event, id: string) => {
    dispatch(editEvent(event, id));
  };

  const dispatchResetEvent = () => {
    dispatch(resetEvent());
  };

  const dispatchApproveEvent = (id) => {
    dispatch(editEventStatus(id, 'approved'));
  };

  const dispatchRejectEvent = (id) => {
    dispatch(editEventStatus(id, 'rejected'));
  };

  useEffect(() => {
    if (fetched) {
      setEventOptions(mapEventsToOptions(events));
    }
  }, [events, fetched]);

  useEffect(() => {
    if (query) {
      dispatchFetchEvents();
    }
  }, [query, dispatchFetchEvents]);

  return (
    <EventContext.Provider
      value={{
        eventOptions,
        query,
        setQuery,
        dispatchAddEvent,
        dispatchEditEvent,
        dispatchFetchEvents,
        dispatchFetchEvent,
        dispatchApproveEvent,
        dispatchRejectEvent,
        dispatchResetEvent,
      }}>
      {children}
    </EventContext.Provider>
  );
};

const useEvent = (): ContextType => {
  const context = useContext(EventContext);
  if (context === undefined) {
    throw new Error('useEvent must be used within a EventContextProvider');
  }
  return context;
};

export { EventProvider, useEvent };
