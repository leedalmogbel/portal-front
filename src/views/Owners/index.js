/**
 * @flow
 */

import { Fragment, useCallback, useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useTable } from 'react-table';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import Button from 'components/common/Button';
import Title from 'components/common/Title';
import Table from 'components/common/Table';
import { useOwnerDetail } from 'views/OwnerDetail/hook';
import type { State } from 'reducers/types';
import type { OwnerDetailState } from 'reducers/owners/detail';
import type { OwnersState } from 'reducers/owners/all';

import { mapOwnersToTableData } from './utils';
import { useOwner } from './hook';
import { tableHeader } from './constants';

const Container = tw.div`flex mb-4 justify-between`;

const Owners = (): Node => {
  const {
    dispatchFetchOwners,
    dispatchApproveOwner,
    dispatchRejectOwner,
    dispatchResetOwner,
  } = useOwner();
  const { data: owners } = useSelector<State, OwnersState>(
    (state) => state.owners.all
  );
  const {
    status: { edited },
  } = useSelector<State, OwnerDetailState>((state) => state.owners.detail);
  const navigate = useNavigate();
  const { changeSceneToAdd, changeSceneToEdit, changeSceneToView } =
    useOwnerDetail();
  const columns = tableHeader;

  const actions = {
    approve: dispatchApproveOwner,
    reject: dispatchRejectOwner,
    view: (id) => {
      changeSceneToView();
      navigate(`/owners/${id}`);
    },
    edit: (id) => {
      changeSceneToEdit();
      navigate(`/owners/${id}`);
    },
  };
  const data = mapOwnersToTableData(owners, actions);
  const tableInstance = useTable({ columns, data });

  const onClick = useCallback(() => {
    changeSceneToAdd();
    navigate('/owners/add');
  }, [changeSceneToAdd, navigate]);

  useEffect(() => {
    dispatchFetchOwners();
  }, [dispatchFetchOwners]);

  useEffect(() => {
    if (edited) {
      dispatchResetOwner();
      dispatchFetchOwners();
      toast.success('Owner successfully updated!');
    }
  }, [edited, dispatchFetchOwners, dispatchResetOwner]);

  return (
    <Fragment>
      <Container>
        <Title type="h1">Owners</Title>
        <Button type="button" size="xs" onClick={onClick}>
          Add New Owner
        </Button>
      </Container>
      <Table tableInstance={tableInstance} />
    </Fragment>
  );
};

export default Owners;
