/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import {
  CheckIcon,
  XIcon,
  PencilAltIcon,
  EyeIcon,
} from '@heroicons/react/solid';
import { isEmpty } from 'lodash';
import { GrDocumentPdf } from 'react-icons/gr';

import Status from 'components/common/Status';
import Dropdown from 'components/common/Dropdown';
import type { DropdownItem } from 'components/common/Dropdown';
import type { Owner } from 'types/owner';
import type { Option } from 'types';

type TableData = {
  name: Node,
  email: string,
  documents: ?Node,
  country: string,
  visa: string,
  status: Node,
  actions: Node,
};

const PdfLink = tw.a`text-3xl text-center cursor-pointer`;

export const mapOwnersToTableData = (
  owners: Array<Owner>,
  actions: Object
): Array<TableData> =>
  owners.map(
    ({
      id,
      firstName,
      lastName,
      email,
      address: {
        home: { country },
      },
      visa,
      status,
      documents,
    }) => ({
      name: (
        <Fragment>
          <p tw="font-bold">
            {firstName} {lastName}
          </p>
        </Fragment>
      ),
      email,
      documents: !isEmpty(documents) && (
        <PdfLink href={documents.filePath} target="_blank" rel="noreferrer">
          <GrDocumentPdf />
        </PdfLink>
      ),
      country,
      visa,
      status: <Status status={status} />,
      actions: !isEmpty(actions) && mapActionsToDropdownItems(actions, id),
    })
  );

const mapActionsToDropdownItems = (actions: Object, id: string) => {
  const actionItems: Array<Array<DropdownItem>> = [
    [
      {
        label: 'Approve',
        onClick: actions.approve,
        Icon: CheckIcon,
      },
      {
        label: 'Reject',
        onClick: actions.reject,
        Icon: XIcon,
      },
    ],
    [
      {
        label: 'View',
        onClick: actions.view,
        Icon: EyeIcon,
      },
      {
        label: 'Edit',
        onClick: actions.edit,
        Icon: PencilAltIcon,
      },
    ],
  ];

  return <Dropdown items={actionItems} id={id} />;
};

// TODO: Make it more reusable; Move to utils
export const mapOwnersToOptions = (owners: Array<Owner>): Array<Option> => {
  return owners.map(({ firstName, lastName, id }) => ({
    name: `${id} ${firstName} ${lastName}`,
    value: id.toString(),
  }));
};
