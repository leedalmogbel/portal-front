/**
 * @flow
 */

import { useState, useContext, createContext, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomCompareCallback } from 'use-custom-compare';
import { isEqual } from 'lodash';

import { resetOwner } from 'actions/owners';
import { editOwnerStatus, editOwner } from 'actions/owners/edit';
import { fetchOwners, fetchOwner } from 'actions/owners/fetch';
import { addOwner } from 'actions/owners/add';
import type { Owner } from 'types/owner';
import type { State } from 'reducers/types';
import type { OwnersState } from 'reducers/owners/all';
import type { Option } from 'types';
import { useAuth } from 'hooks/auth';
import { fetchNotification } from 'actions/notification/fetch';

import { mapOwnersToOptions } from './utils';

type ContextType = {
  ownerOptions: Array<Option>,
  query: string,
  setQuery: Function,
  dispatchAddOwner: Function,
  dispatchEditOwner: Function,
  dispatchFetchOwners: Function,
  dispatchFetchOwner: Function,
  dispatchApproveOwner: Function,
  dispatchRejectOwner: Function,
  dispatchResetOwner: Function,
};

const initialValues = {
  ownerOptions: [],
  query: '',
  setQuery: () => {},
  dispatchAddOwner: () => {},
  dispatchEditOwner: () => {},
  dispatchFetchOwners: () => {},
  dispatchFetchOwner: () => {},
  dispatchApproveOwner: () => {},
  dispatchRejectOwner: () => {},
  dispatchResetOwner: () => {},
};

const OwnerContext = createContext<ContextType>(initialValues);

type OwnerProviderProps = {
  children: ?Node,
};

const OwnerProvider = ({ children }: OwnerProviderProps): Node => {
  const dispatch = useDispatch();
  const [query, setQuery] = useState(initialValues.query);
  const [ownerOptions, setOwnerOptions] = useState<Array<Option>>(
    initialValues.ownerOptions
  );
  const {
    data: owners,
    status: { fetched },
  } = useSelector<State, OwnersState>((state: State) => state.owners.all);
  // TODO: Remove selector
  const { user } = useAuth();

  // TODO: Remove auth id
  const dispatchFetchOwners = useCustomCompareCallback(
    () => {
      dispatch(fetchOwners({ id: user?.id, query }));
    },
    [dispatch, user, query],
    (prevDeps, nextDeps) => isEqual(prevDeps, nextDeps)
  );

  const dispatchFetchOwner = (id: string) => {
    dispatch(fetchOwner(id));
  };

  const dispatchAddOwner = (owner: Owner) => {
    dispatch(addOwner(owner));
  };

  const dispatchEditOwner = (owner: Owner, id: string) => {
    dispatch(editOwner(owner, id));
  };

  const dispatchResetOwner = () => {
    dispatch(resetOwner());
  };

  const dispatchApproveOwner = (id) => {
    dispatch(editOwnerStatus(id, 'approved'));
    dispatch(fetchNotification(user.id));
  };

  const dispatchRejectOwner = (id) => {
    dispatch(editOwnerStatus(id, 'rejected'));
  };

  useEffect(() => {
    if (fetched) {
      setOwnerOptions(mapOwnersToOptions(owners));
    }
  }, [owners, fetched]);

  useEffect(() => {
    if (query) {
      dispatchFetchOwners();
    }
  }, [query, dispatchFetchOwners]);

  return (
    <OwnerContext.Provider
      value={{
        ownerOptions,
        query,
        setQuery,
        dispatchAddOwner,
        dispatchEditOwner,
        dispatchFetchOwners,
        dispatchFetchOwner,
        dispatchApproveOwner,
        dispatchRejectOwner,
        dispatchResetOwner,
      }}>
      {children}
    </OwnerContext.Provider>
  );
};

const useOwner = (): ContextType => {
  const context = useContext(OwnerContext);
  if (context === undefined) {
    throw new Error('useOwner must be used within a OwnerContextProvider');
  }
  return context;
};

export { OwnerProvider, useOwner };
