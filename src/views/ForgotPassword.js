/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';

import Spinner from 'components/common/Spinner';
import Label from 'components/common/Label';
import Input from 'components/common/Input';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import { usePassword } from 'hooks/password';
import type { State } from 'reducers/types';
import type { ForgotPasswordState } from 'reducers/password/forgot';

const Container = tw.div`lg:(w-1/2)`,
  InnerContainer = tw.div`flex items-center`,
  Title = tw(BaseTitle)`mb-8`,
  FormGroup = tw.div`mb-4`;

const ForgotPassword = (): Node => {
  const methods = useForm();
  const { dispatchForgotPassword } = usePassword();
  const navigate = useNavigate();
  const {
    status: { reset, resetting },
  } = useSelector<State, ForgotPasswordState>((state) => state.password.forgot);

  const onSubmit = (data) => {
    dispatchForgotPassword(data);
  };

  useEffect(() => {
    if (reset) {
      navigate('/change-password');
    }
  }, [reset, navigate]);

  return (
    <Container>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(onSubmit)}>
          <Title type="h2">Forgot Password</Title>
          <FormGroup>
            <Label htmlFor="mobile">Input mobile number</Label>
            <Input type="text" id="mobile" name="mobile" />
          </FormGroup>
          <InnerContainer>
            <Button type="submit" size="xs" disabled={resetting}>
              Submit
            </Button>
            {resetting && <Spinner />}
          </InnerContainer>
        </form>
      </FormProvider>
    </Container>
  );
};

export default ForgotPassword;
