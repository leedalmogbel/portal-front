/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { isEmpty } from 'lodash';
import { toast } from 'react-toastify';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import { usePassword } from 'hooks/password';
import type { State } from 'reducers/types';
import type { ForgotPasswordState } from 'reducers/password/forgot';
import type { ChangePasswordState } from 'reducers/password/change';

const Container = tw.div`lg:(w-1/2)`,
  Title = tw(BaseTitle)`mb-8`,
  FormGroup = tw.div`mb-4`;

const ChangePassword = (): Node => {
  const methods = useForm();
  const { dispatchChangePassword, dispatchResetPassword } = usePassword();
  const navigate = useNavigate();
  const { data } = useSelector<State, ForgotPasswordState>(
    (state) => state.password.forgot
  );
  const {
    status: { changed, changing },
  } = useSelector<State, ChangePasswordState>((state) => state.password.change);

  const onSubmit = (data) => {
    dispatchChangePassword(data);
  };

  useEffect(() => {
    if (isEmpty(data)) {
      navigate('/');
    } else {
      const { userId } = data;
      methods.setValue('id', userId);
    }
  }, [data, navigate, methods]);

  useEffect(() => {
    if (changed) {
      dispatchResetPassword();
      navigate('/');
      toast.success('Password changed successfully');
    }
  }, [changed, navigate, dispatchResetPassword]);

  return (
    <Container>
      <FormProvider {...methods}>
        <form onSubmit={methods.handleSubmit(onSubmit)}>
          <Title type="h2">Change Password</Title>
          <FormGroup>
            <Label htmlFor="password">New Password</Label>
            <Input type="text" id="password" name="password" />
          </FormGroup>
          <FormGroup>
            <Label htmlFor="otp">OTP</Label>
            <Input type="text" id="otp" name="otp" />
          </FormGroup>
          <Button type="submit" size="xs" disabled={changing}>
            Submit
          </Button>
        </form>
      </FormProvider>
    </Container>
  );
};

export default ChangePassword;
