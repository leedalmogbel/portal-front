/**
 * @flow
 */

import type { Node } from 'react';
import { Navigate, useLocation } from 'react-router-dom';

import Layout from 'containers/Layout';
import { useAuth } from 'hooks/auth';

const Authentication = (): Node => {
  const { authenticated } = useAuth();
  const location = useLocation();

  if (!authenticated) {
    return <Navigate to="/login" state={{ from: location }} />;
  }

  return <Layout />;
};

export default Authentication;
