/**
 * @flow
 */

import { pick, first } from 'lodash';
import { DateTime } from 'luxon';
import { parseISO, format } from 'date-fns';

import type { Stable, FormData } from 'types/stable';
import { stableTypeOptions } from 'utils/constants';

export const mapStableToFormData = (stable: Stable): FormData => {
  const stableData = pick(stable, ['name', 'eievStableId', 'entryCount']);
  console.log('stable33', stable);
  const formData = {
    stableType:
      stableTypeOptions.find((option) => option.value === stable.stableType) ||
      first(stableTypeOptions),
    ...stableData,
  };

  return formData;
};
