/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { first, isEmpty } from 'lodash';
import { useNavigate } from 'react-router-dom';

import { stableTypeOptions } from 'utils/constants';
import { useStable } from 'views/Stables/hook';
import { useUser } from 'views/Users/hook';
import type { FormData } from 'types/stable';
import type { Option } from 'types';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import type { State } from 'reducers/types';
import type { StableDetailState } from 'reducers/stables/detail';
import type { AuthState } from 'reducers/auth';

import RegistrationDetailsFields from './Fields/RegistrationDetails';
import { useStableDetail } from './hook';

const Title = tw(BaseTitle)`mb-4 text-red-600`,
  Buttons = tw.div`flex gap-2`;

type FormProps = {
  defaultValues?: FormData,
};

const Form = ({ defaultValues }: FormProps): Node => {
  const { dispatchAddStable, dispatchEditStable } = useStable();
  const { data: auth } = useSelector<State, AuthState>((state) => state.auth);
  const { data: stable } = useSelector<State, StableDetailState>(
    (state) => state.stables.detail
  );
  const { userOptions } = useUser();

  const navigate = useNavigate();
  const { sceneIsAdd, sceneIsEdit, sceneIsView, changeSceneToView } =
    useStableDetail();

  const methods = useForm({
    defaultValues,
  });

  const onSubmit = (data) => {
    if (sceneIsAdd) {
      dispatchAddStable(data);
    } else {
      dispatchEditStable(data, stable.id);
    }
  };

  const onCancel = () => {
    if (sceneIsAdd) {
      navigate('/stables');
    } else if (sceneIsEdit) {
      changeSceneToView();
    }
  };

  useEffect(() => {
    if ((sceneIsView || sceneIsEdit) && !isEmpty(auth)) {
      methods.setValue('userId', auth.id);
    }
  }, [auth, methods, sceneIsView, sceneIsEdit]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(auth)) {
      methods.setValue('userId', auth.id);
    }
  }, [auth, methods, sceneIsAdd]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(userOptions)) {
      methods.setValue('user', first(userOptions));
    }
  }, [methods, userOptions, sceneIsAdd]);

  useEffect(() => {
    if (
      (sceneIsView || sceneIsEdit) &&
      !isEmpty(stable) &&
      !isEmpty(userOptions)
    ) {
      methods.setValue(
        'user',
        userOptions.find(({ value }) => parseInt(value) === stable.userId)
      );
    }
  }, [sceneIsEdit, sceneIsView, stable, methods, userOptions]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <Title type="h4">Stable Details</Title>
        <RegistrationDetailsFields />
        <Buttons>
          {!sceneIsView && (
            <Button type="submit" size="xs">
              {sceneIsAdd ? 'Create' : 'Update'}
            </Button>
          )}
          {(sceneIsAdd || sceneIsEdit) && (
            <Button type="button" size="xs" onClick={onCancel}>
              Cancel
            </Button>
          )}
        </Buttons>
      </form>
    </FormProvider>
  );
};

Form.defaultProps = {
  defaultValues: {
    name: '',
    userId: '',
    user: null,
    entryCount: '',
    stableType: (first(stableTypeOptions): Option),
  },
};

export default Form;
