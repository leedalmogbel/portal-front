/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Select from 'components/common/Select';
import Error from 'components/common/Error';
import { stableTypeOptions } from 'utils/constants';
import { useUser } from 'views/Users/hook';

import { useStableDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`,
  OneThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc((100% - 80px) / 3)])`,
  Required = tw.span`text-red-500 font-black text-xl`;

const RegistrationDetailsFields = (): Node => {
  const { errors } = useFormState();
  const { sceneIsView } = useStableDetail();
  const { userOptions } = useUser();
  const readOnly = sceneIsView;

  return (
    <Fragment>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="name">
            Stable Name
            <Required> *</Required>
          </Label>
          <Input
            name="name"
            id="name"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.name && 'Stable Name is required'}</Error>
        </FormGroup>
        {/* <FormGroup tw="w-4/12">
          <Label htmlFor="entryCount">
            Entry per Race
            <Required> *</Required>
          </Label>
          <Input
            name="entryCount"
            id="entryCount"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.entryCount && 'Entry per Race is required'}</Error>
        </FormGroup> */}
        <FormGroup tw="flex-1">
          <Label htmlFor="stableType">
            Stable Type<Required> *</Required>
          </Label>
          <Select
            name="stableType"
            id="stableType"
            options={stableTypeOptions}
            readOnly={readOnly}
          />
        </FormGroup>
      </TwoThirdColumnContainer>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="user">User</Label>
          <Select
            name="user"
            id="user"
            options={userOptions}
            readOnly={readOnly}
          />
        </FormGroup>
      </TwoThirdColumnContainer>
      <TwoThirdColumnContainer>
        {/* <FormGroup tw="flex-1">
          <Label htmlFor="entryCount">
            Entry per Race
            <Required> *</Required>
          </Label>
          <Input
            name="entryCount"
            id="entryCount"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.entryCount && 'Entry per Race is required'}</Error>
        </FormGroup> */}
      </TwoThirdColumnContainer>
      {/* <FlexContainer>
        <FormGroup tw="w-4/12">
          <Label htmlFor="startDate">
            Start Date
            <Required> *</Required>
          </Label>
          <Input
            name="startDate"
            id="startDate"
            type="date"
            format="yyyy-mm-dd"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.startDate && 'Stable Date is required'}</Error>
        </FormGroup>
        <FormGroup tw="w-4/12">
          <Label htmlFor="endDate">
            End Date
            <Required> *</Required>
          </Label>
          <Input
            name="endDate"
            id="endDate"
            type="date"
            format="yyyy-mm-dd"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.endDate && 'End Date is required'}</Error>
        </FormGroup>
      </FlexContainer> */}
    </Fragment>
  );
};

export default RegistrationDetailsFields;
