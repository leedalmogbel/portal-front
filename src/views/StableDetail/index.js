/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { isEmpty } from 'lodash';

import { useStable } from 'views/Stables/hook';
import { useUser } from 'views/Users/hook';
import type { State } from 'reducers/types';
import type { StableDetailState } from 'reducers/stables/detail';

import Form from './Form';
import { mapStableToFormData } from './utils';

const Container = tw.div`p-8 bg-white`;

const StableDetail = (): Node => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { userOptions, dispatchFetchUsers } = useUser();
  const {
    data: stableDetail,
    status: { fetching, fetched, added, edited },
  } = useSelector<State, StableDetailState>((state) => state.stables.detail);
  const { dispatchFetchStable, dispatchResetStable } = useStable();

  useEffect(() => {
    return () => {
      dispatchResetStable();
    };
  }, [dispatchResetStable]);

  useEffect(() => {
    if (id) {
      dispatchFetchStable(id);
    }
  }, [id, dispatchFetchStable]);

  useEffect(() => {
    if (added) {
      toast.success('Stable succssfully registered!');
      navigate('/stables');
    }
  }, [added, navigate, dispatchResetStable]);

  useEffect(() => {
    if (edited) {
      // TODO: Hide toast if nothing was changed
      toast.success('Stable succssfully updated!');
      navigate('/stables');
    }
  }, [edited, navigate, dispatchResetStable]);

  useEffect(() => {
    dispatchFetchUsers();
  }, [dispatchFetchUsers]);

  return (
    <Container>
      {fetching && <div>Loading...</div>}
      {fetched && !isEmpty(userOptions) && (
        <Form defaultValues={mapStableToFormData(stableDetail)} />
      )}
      {!fetching && !fetched && !isEmpty(userOptions) && <Form />}
    </Container>
  );
};

export default StableDetail;
