/**
 * @flow
 */

import { useState, useContext, createContext } from 'react';
import type { Node } from 'react';

import type { Scene } from 'types';

type ContextType = {
  sceneIsAdd: boolean,
  sceneIsEdit: boolean,
  sceneIsView: boolean,
  changeSceneToAdd: Function,
  changeSceneToEdit: Function,
  changeSceneToView: Function,
};

const initialValues = {
  scene: 'view',
  sceneIsAdd: false,
  sceneIsEdit: false,
  sceneIsView: true,
  changeSceneToAdd: () => {},
  changeSceneToEdit: () => {},
  changeSceneToView: () => {},
};

const StableContext = createContext<ContextType>(initialValues);

type StableDetailProviderProps = {
  children: ?Node,
};

const StableDetailProvider = ({
  children,
}: StableDetailProviderProps): Node => {
  const [scene, setScene] = useState<Scene>(initialValues.scene);
  const sceneIsAdd = scene === 'add',
    sceneIsEdit = scene === 'edit',
    sceneIsView = scene === 'view';

  const changeSceneToAdd = () => {
    setScene('add');
  };

  const changeSceneToEdit = () => {
    setScene('edit');
  };

  const changeSceneToView = () => {
    setScene('view');
  };

  return (
    <StableContext.Provider
      value={{
        sceneIsAdd,
        sceneIsEdit,
        sceneIsView,
        changeSceneToAdd,
        changeSceneToEdit,
        changeSceneToView,
      }}>
      {children}
    </StableContext.Provider>
  );
};

const useStableDetail = (): ContextType => {
  const context = useContext(StableContext);
  if (context === undefined) {
    throw new Error(
      'useStableDetail must be used within a StableContextProvider'
    );
  }
  return context;
};

export { StableDetailProvider, useStableDetail };
