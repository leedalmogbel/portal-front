/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useHorse } from 'views/Horses/hook';
import type { State } from 'reducers/types';
import type { HorseDetailState } from 'reducers/horses/detail';

import Form from './Form';
import { mapHorseToFormData } from './utils';

const Container = tw.div`p-8 bg-white`;

const HorseDetail = (): Node => {
  const { id } = useParams();
  const navigate = useNavigate();
  const {
    data: horseDetail,
    status: { fetching, fetched, added, edited },
  } = useSelector<State, HorseDetailState>((state) => state.horses.detail);
  const { dispatchFetchHorse, dispatchResetHorse } = useHorse();

  useEffect(() => {
    return () => {
      dispatchResetHorse();
    };
  }, [dispatchResetHorse]);

  useEffect(() => {
    if (id) {
      dispatchFetchHorse(id);
    }
  }, [id, dispatchFetchHorse]);

  useEffect(() => {
    if (added) {
      toast.success('Horse succssfully registered!');
      navigate('/horses');
    }
  }, [added, navigate, dispatchResetHorse]);

  useEffect(() => {
    if (edited) {
      // TODO: Hide toast if nothing was changed
      toast.success('Horse succssfully updated!');
      navigate('/horses');
    }
  }, [edited, navigate, dispatchResetHorse]);

  return (
    <Container>
      {fetching && <div>Loading...</div>}
      {fetched && <Form defaultValues={mapHorseToFormData(horseDetail)} />}
      {!fetching && !fetched && <Form />}
    </Container>
  );
};

export default HorseDetail;
