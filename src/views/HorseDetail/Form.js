/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { first, isEmpty } from 'lodash';
import { useNavigate } from 'react-router-dom';

import { useOwner } from 'views/Owners/hook';
import { useHorse } from 'views/Horses/hook';
import { useAuth } from 'hooks/auth';
import type { FormData } from 'types/horse';
import type { Option } from 'types';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import { breedOptions, horseColorOptions } from 'utils/constants';
import type { State } from 'reducers/types';
import type { HorseDetailState } from 'reducers/horses/detail';
import type { AuthState } from 'reducers/auth';

import HorseInformationFields from './Fields/HorseInformation';
import FeiRegistrationFields from './Fields/FeiRegistration';
import DocumentsVerificationFields from './Fields/DocumentsVerification';
import OwnerInformationFields from './Fields/OwnerInformation';
import TrainerInformationFields from './Fields/TrainerInformation';
import { useHorseDetail } from './hook';

const Title = tw(BaseTitle)`mb-4 text-red-600`,
  Buttons = tw.div`flex gap-2`;

type FormProps = {
  defaultValues?: FormData,
};

const Form = ({ defaultValues }: FormProps): Node => {
  const { dispatchAddHorse, dispatchEditHorse } = useHorse();
  const { ownerOptions } = useOwner();
  const { user } = useAuth();
  const { data: horse } = useSelector<State, HorseDetailState>(
    (state) => state.horses.detail
  );
  const navigate = useNavigate();
  const { sceneIsAdd, sceneIsEdit, sceneIsView, changeSceneToView } =
    useHorseDetail();

  const methods = useForm({
    defaultValues,
  });

  const onSubmit = (data) => {
    if (sceneIsAdd) {
      dispatchAddHorse(data);
    } else {
      dispatchEditHorse(data, horse.id);
    }
  };

  const onCancel = () => {
    if (sceneIsAdd) {
      navigate('/horses');
    } else if (sceneIsEdit) {
      changeSceneToView();
    }
  };

  useEffect(() => {
    if ((sceneIsView || sceneIsEdit) && !isEmpty(user)) {
      methods.setValue('userId', user.id);
    }
  }, [user, methods, sceneIsView, sceneIsEdit]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(user)) {
      methods.setValue('userId', user.id);
    }
  }, [user, methods, sceneIsAdd]);

  useEffect(() => {
    if ((sceneIsView || sceneIsEdit) && !isEmpty(horse)) {
      console.log('yeah', ownerOptions);

      methods.setValue(
        'owner',
        ownerOptions.find(({ value }) => parseInt(value) === horse.ownerId)
      );
    }
  }, [sceneIsEdit, sceneIsView, horse, methods, ownerOptions]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(ownerOptions)) {
      methods.setValue('owner', first(ownerOptions));
    }
  }, [methods, ownerOptions, sceneIsAdd]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <Title type="h4">Horse Information</Title>
        <HorseInformationFields />
        <Title type="h4">FEI Registration</Title>
        <FeiRegistrationFields />
        <Title type="h4">Owner Information</Title>
        <OwnerInformationFields />
        <Title type="h4">Trainer Information</Title>
        <TrainerInformationFields />
        <Title type="h4">Documents Verification</Title>
        <DocumentsVerificationFields />
        <Buttons>
          {!sceneIsView && (
            <Button type="submit" size="xs">
              {sceneIsAdd ? 'Create' : 'Update'}
            </Button>
          )}
          {(sceneIsAdd || sceneIsEdit) && (
            <Button type="button" size="xs" onClick={onCancel}>
              Cancel
            </Button>
          )}
        </Buttons>
      </form>
    </FormProvider>
  );
};

Form.defaultProps = {
  defaultValues: {
    name: '',
    originalName: '',
    countryBirth: { name: 'United Arab Emirates', value: 'UAE' },
    breed: (first(breedOptions): Option),
    breeder: '',
    dob: '',
    gender: '',
    color: (first(horseColorOptions): Option),
    microchipNum: '',
    uelnNo: '',
    countryResidence: { name: 'United Arab Emirates', value: 'UAE' },
    sire: '',
    dam: '',
    sireOfDam: '',
    feiRegistrationNo: '',
    feiPassportExpiryDate: null,
    feiPassportNo: '',
    owner: null,
    documentExpiry: null,
    documentExpiryFile: null,
    remarks: '',
  },
};

export default Form;
