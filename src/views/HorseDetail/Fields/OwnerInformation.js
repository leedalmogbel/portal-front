/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import AutoComplete from 'components/common/AutoComplete';
import Label from 'components/common/Label';
import Error from 'components/common/Error';
import { useOwner } from 'views/Owners/hook';

const FormGroup = tw.div`mb-4`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`;

const OwnerInformationFields = (): Node => {
  const { errors } = useFormState();
  const { setQuery, ownerOptions } = useOwner();

  return (
    <Fragment>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="owner">Owner</Label>
          <AutoComplete
            id="owner"
            name="owner"
            onInputChange={setQuery}
            options={ownerOptions}
          />
          <Error>{errors.owner && 'Owner is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
    </Fragment>
  );
};

export default OwnerInformationFields;
