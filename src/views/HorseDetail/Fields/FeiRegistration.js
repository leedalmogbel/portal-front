/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Error from 'components/common/Error';

import { useHorseDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`,
  Required = tw.span`text-red-500 font-black text-xl`;

const FeiRegistrationFields = (): Node => {
  const { errors } = useFormState();
  const { sceneIsView } = useHorseDetail();
  const readOnly = sceneIsView;

  return (
    <Fragment>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="feiPassportNo">
            FEI Passport No.
            <Required> *</Required>
          </Label>
          <Input
            name="feiPassportNo"
            id="feiPassportNo"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>
            {errors.feiPassportNo && 'FEI Passport No. is required'}
          </Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="feiPassportExpiryDate">
            FEI Passport Expiry Date
            <Required> *</Required>
          </Label>
          <Input
            name="feiPassportExpiryDate"
            id="feiPassportExpiryDate"
            type="date"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>
            {errors.feiPassportExpiryDate &&
              'FEI Passport Expiry Date is required'}
          </Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="feiRegistrationNo">
            FEI Registration No.
            <Required> *</Required>
          </Label>
          <Input
            name="feiRegistrationNo"
            id="feiRegistrationNo"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>
            {errors.feiRegistrationNo && 'FEI Registration No. is required'}
          </Error>
        </FormGroup>
      </FlexContainer>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="feiId">
            FEI ID
            <Required> *</Required>
          </Label>
          <Input
            name="feiId"
            id="feiId"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.feiId && 'FEI ID is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="feiId">
            EEF ID
            <Required> *</Required>
          </Label>
          <Input
            name="eefId"
            id="eefId"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.eefId && 'EEF ID is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
    </Fragment>
  );
};

export default FeiRegistrationFields;
