/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Select from 'components/common/Select';
import Error from 'components/common/Error';
import {
  breedOptions,
  countryOptions,
  visaCategoryOptions,
  horseColorOptions,
} from 'utils/constants';

import { useHorseDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`,
  OneThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc((100% - 80px) / 3)])`;

const HorseInformationFields = (): Node => {
  const { errors } = useFormState();
  const { sceneIsView } = useHorseDetail();
  const readOnly = sceneIsView;

  return (
    <Fragment>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="name">Name</Label>
          <Input
            name="name"
            id="name"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.name && 'Name is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="discipline">Original Name</Label>
          <Input
            name="originalName"
            id="originalName"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.originalName && 'Original Name is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="countryBirth">Country of Birth</Label>
          <Select
            name="countryBirth"
            id="countryBirth"
            options={countryOptions}
            readOnly={readOnly}
          />
        </FormGroup>
      </FlexContainer>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="breed">Breed</Label>
          <Select
            name="breed"
            id="breed"
            options={breedOptions}
            readOnly={readOnly}
          />
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="breeder">Breeder</Label>
          <Input
            name="breeder"
            id="breeder"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.breeder && 'Breeder is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="dob">Date of Birth</Label>
          <Input
            name="dob"
            id="dob"
            type="date"
            format="yyyy-mm-dd"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.dob && 'Date of Birth is required'}</Error>
        </FormGroup>
      </FlexContainer>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="gender">Gender</Label>
          <Input
            name="gender"
            id="gender"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.gender && 'Gender is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="color">Colour</Label>
          <Select
            name="color"
            id="color"
            options={horseColorOptions}
            readOnly={readOnly}
          />
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="microchipNum">Microchip No.</Label>
          <Input
            name="microchipNum"
            id="microchipNum"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.microchipNum && 'Microchip No. is required'}</Error>
        </FormGroup>
      </FlexContainer>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="uelnNo">UELN No.</Label>
          <Input
            name="uelnNo"
            id="uelnNo"
            type="text"
            rules={{ required: false }}
            readOnly={readOnly}
          />
          <Error>{errors.uelnNo && 'UELN No. is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="countryResidence">Country of Residence</Label>
          <Select
            name="countryResidence"
            id="countryResidence"
            options={[{ name: 'United Arab Emirates', value: 'UAE' }]}
            readOnly={readOnly}
          />
        </FormGroup>
      </TwoThirdColumnContainer>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="sire">Sire</Label>
          <Input
            name="sire"
            id="sire"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.sire && 'Sire is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="dam">Dam</Label>
          <Input
            name="dam"
            id="dam"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.dam && 'Dam is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="sireOfDam">Sire of Dam</Label>
          <Input
            name="sireOfDam"
            id="sireOfDam"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.sireOfDam && 'Sire of Dam is required'}</Error>
        </FormGroup>
      </FlexContainer>
    </Fragment>
  );
};

export default HorseInformationFields;
