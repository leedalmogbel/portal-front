/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import AutoComplete from 'components/common/AutoComplete';
import Label from 'components/common/Label';
import Error from 'components/common/Error';
import { useTrainer } from 'views/Trainers/hook';

const FormGroup = tw.div`mb-4`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`;

const TrainerInformationFields = (): Node => {
  const { errors } = useFormState();
  const { setQuery, trainerOptions } = useTrainer();

  return (
    <Fragment>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="trainer">Trainer</Label>
          <AutoComplete
            id="trainer"
            name="trainer"
            onInputChange={setQuery}
            options={trainerOptions}
          />
          <Error>{errors.trainer && 'Trainer is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
    </Fragment>
  );
};

export default TrainerInformationFields;
