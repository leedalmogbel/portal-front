/**
 * @flow
 */

import { pick, first } from 'lodash';
import { DateTime } from 'luxon';
import { parseISO, format } from 'date-fns';
import { utcToZonedTime } from 'date-fns-tz';

import type { Horse, FormData } from 'types/horse';
import {
  horseColorOptions,
  breedOptions,
  countryOptions,
} from 'utils/constants';

export const mapHorseToFormData = (horse: Horse): FormData => {
  const horseData = pick(horse, [
    'name',
    'originalName',
    'breeder',
    'gender',
    'microchipNum',
    'uelnNo',
    'sire',
    'dam',
    'sireOfDam',
    'remarks',
  ]);

  const horseDob = parseISO(horse.dob);
  const feiRegistrationDate = parseISO(horse.feiRegistration.date);
  const documentExpiryDate = parseISO(horse.documents.documentExpiry);
  // console.log('horse', horse);
  // console.log('dd', format(horseDob, 'yyyy-MM-dd'));
  // console.log('feiRegistrationDate', format(feiRegistrationDate, 'yyyy-MM-dd'));
  // console.log(
  //   'documentExpiryDate',
  //   format(horse.documents.documentExpiryDate, 'yyyy-MM-dd')
  // );
  const formData = {
    feiRegistrationNo: horse.feiRegistration.no,
    feiPassportExpiryDate: format(feiRegistrationDate, 'yyyy-MM-dd'),
    dob: format(horseDob, 'yyyy-MM-dd'),
    feiPassportNo: horse.feiRegistration.passportNo,
    countryBirth:
      countryOptions.find((option) => option.value === horse.countryBirth) ||
      first(countryOptions),
    documentExpiry: horse.documents
      ? format(documentExpiryDate, 'yyyy-MM-dd')
      : null,
    countryResidence: { name: 'United Arab Emirates', value: 'UAE' },
    breed:
      breedOptions.find((option) => option.value === horse.breed) ||
      first(breedOptions),
    color:
      horseColorOptions.find((option) => option.value === horse.color) ||
      first(horseColorOptions),
    ...horseData,
  };

  return formData;
};
