/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import { useNavigate } from 'react-router-dom';

import { useAuth } from 'hooks/auth';

const Logout = (): Node => {
  const navigate = useNavigate();
  const { logout } = useAuth();

  useEffect(() => {
    logout();
  }, [navigate, logout]);

  return <div>Logout</div>;
};
export default Logout;
