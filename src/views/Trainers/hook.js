/**
 * @flow
 */

import { useState, useContext, createContext, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomCompareCallback } from 'use-custom-compare';
import { isEqual } from 'lodash';

import { resetTrainer } from 'actions/trainers';
import { editTrainerStatus, editTrainer } from 'actions/trainers/edit';
import { fetchTrainers, fetchTrainer } from 'actions/trainers/fetch';
import { addTrainer } from 'actions/trainers/add';
import type { Trainer } from 'types/trainer';
import type { State } from 'reducers/types';
import { useAuth } from 'hooks/auth';
import type { TrainersState } from 'reducers/trainers/all';
import type { Option } from 'types';

import { mapTrainersToOptions } from './utils';

type ContextType = {
  trainerOptions: Array<Option>,
  query: string,
  setQuery: Function,
  dispatchAddTrainer: Function,
  dispatchEditTrainer: Function,
  dispatchFetchTrainers: Function,
  dispatchFetchTrainer: Function,
  dispatchApproveTrainer: Function,
  dispatchRejectTrainer: Function,
  dispatchResetTrainer: Function,
};

const initialValues = {
  trainerOptions: [],
  query: '',
  setQuery: () => {},
  dispatchAddTrainer: () => {},
  dispatchEditTrainer: () => {},
  dispatchFetchTrainers: () => {},
  dispatchFetchTrainer: () => {},
  dispatchApproveTrainer: () => {},
  dispatchRejectTrainer: () => {},
  dispatchResetTrainer: () => {},
};

const TrainerContext = createContext<ContextType>(initialValues);

type TrainerProviderProps = {
  children: ?Node,
};

const TrainerProvider = ({ children }: TrainerProviderProps): Node => {
  const dispatch = useDispatch();
  const [query, setQuery] = useState(initialValues.query);
  const [trainerOptions, setTrainerOptions] = useState<Array<Option>>(
    initialValues.trainerOptions
  );
  const {
    data: trainers,
    status: { fetched },
  } = useSelector<State, TrainersState>((state: State) => state.trainers.all);
  // TODO: Remove selector
  const { user } = useAuth();

  // TODO: Remove auth id
  const dispatchFetchTrainers = useCustomCompareCallback(
    () => {
      console.log(query);
      dispatch(fetchTrainers({ id: user.id, query }));
    },
    [dispatch, user, query],
    (prevDeps, nextDeps) => isEqual(prevDeps, nextDeps)
  );

  const dispatchFetchTrainer = (id: string) => {
    dispatch(fetchTrainer(id));
  };

  const dispatchAddTrainer = (trainer: Trainer) => {
    dispatch(addTrainer(trainer));
  };

  const dispatchEditTrainer = (trainer: Trainer, id: string) => {
    dispatch(editTrainer(trainer, id));
  };

  const dispatchResetTrainer = () => {
    dispatch(resetTrainer());
  };

  const dispatchApproveTrainer = (id) => {
    dispatch(editTrainerStatus(id, 'approved'));
  };

  const dispatchRejectTrainer = (id) => {
    dispatch(editTrainerStatus(id, 'rejected'));
  };

  useEffect(() => {
    if (fetched) {
      setTrainerOptions(mapTrainersToOptions(trainers));
    }
  }, [trainers, fetched]);

  useEffect(() => {
    if (query) {
      dispatchFetchTrainers();
    }
  }, [query, dispatchFetchTrainers]);

  return (
    <TrainerContext.Provider
      value={{
        trainerOptions,
        query,
        setQuery,
        dispatchAddTrainer,
        dispatchEditTrainer,
        dispatchFetchTrainers,
        dispatchFetchTrainer,
        dispatchApproveTrainer,
        dispatchRejectTrainer,
        dispatchResetTrainer,
      }}>
      {children}
    </TrainerContext.Provider>
  );
};

const useTrainer = (): ContextType => {
  const context = useContext(TrainerContext);
  if (context === undefined) {
    throw new Error('useTrainer must be used within a TrainerContextProvider');
  }
  return context;
};

export { TrainerProvider, useTrainer };
