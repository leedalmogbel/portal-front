/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import {
  CheckIcon,
  XIcon,
  PencilAltIcon,
  EyeIcon,
} from '@heroicons/react/solid';
import { isEmpty } from 'lodash';
import { GrDocumentPdf } from 'react-icons/gr';

import Status from 'components/common/Status';
import Dropdown from 'components/common/Dropdown';
import type { DropdownItem } from 'components/common/Dropdown';
import type { Trainer } from 'types/trainer';
import type { Option } from 'types';

type TableData = {
  name: Node,
  documents: ?Node,
  visa: string,
  status: Node,
  actions: Node,
};

const PdfLink = tw.a`text-3xl text-center cursor-pointer`;

export const mapTrainersToTableData = (
  trainers: Array<Trainer>,
  actions: Object
): Array<TableData> =>
  trainers.map(({ id, firstName, lastName, visa, status, documents }) => ({
    name: (
      <Fragment>
        <p tw="font-bold">
          {firstName} {lastName}
        </p>
      </Fragment>
    ),
    documents: !isEmpty(documents) && (
      <PdfLink href={documents.filePath} target="_blank" rel="noreferrer">
        <GrDocumentPdf />
      </PdfLink>
    ),
    visa,
    status: <Status status={status} />,
    actions: !isEmpty(actions) && mapActionsToDropdownItems(actions, id),
  }));

const mapActionsToDropdownItems = (actions: Object, id: string) => {
  const actionItems: Array<Array<DropdownItem>> = [
    [
      {
        label: 'Approve',
        onClick: actions.approve,
        Icon: CheckIcon,
      },
      {
        label: 'Reject',
        onClick: actions.reject,
        Icon: XIcon,
      },
    ],
    [
      {
        label: 'View',
        onClick: actions.view,
        Icon: EyeIcon,
      },
      {
        label: 'Edit',
        onClick: actions.edit,
        Icon: PencilAltIcon,
      },
    ],
  ];

  return <Dropdown items={actionItems} id={id} />;
};

export const mapTrainersToOptions = (
  trainers: Array<Trainer>
): Array<Option> => {
  return trainers.map(({ firstName, lastName, id }) => ({
    name: `${id} ${firstName} ${lastName}`,
    value: id.toString(),
  }));
};
