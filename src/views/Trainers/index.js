/**
 * @flow
 */

import { Fragment, useCallback, useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useTable } from 'react-table';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import Button from 'components/common/Button';
import Title from 'components/common/Title';
import Table from 'components/common/Table';
import { useTrainerDetail } from 'views/TrainerDetail/hook';
import type { State } from 'reducers/types';
import type { TrainerDetailState } from 'reducers/trainers/detail';
import type { TrainersState } from 'reducers/trainers/all';

import { mapTrainersToTableData } from './utils';
import { useTrainer } from './hook';
import { tableHeader } from './constants';

const Container = tw.div`flex mb-4 justify-between`;

const Trainers = (): Node => {
  const {
    dispatchFetchTrainers,
    dispatchApproveTrainer,
    dispatchRejectTrainer,
    dispatchResetTrainer,
  } = useTrainer();
  const { data: trainers } = useSelector<State, TrainersState>(
    (state) => state.trainers.all
  );
  const {
    status: { edited },
  } = useSelector<State, TrainerDetailState>((state) => state.trainers.detail);
  const navigate = useNavigate();
  const { changeSceneToAdd, changeSceneToEdit, changeSceneToView } =
    useTrainerDetail();
  const columns = tableHeader;

  const actions = {
    approve: dispatchApproveTrainer,
    reject: dispatchRejectTrainer,
    view: (id) => {
      changeSceneToView();
      navigate(`/trainers/${id}`);
    },
    edit: (id) => {
      changeSceneToEdit();
      navigate(`/trainers/${id}`);
    },
  };
  const data = mapTrainersToTableData(trainers, actions);
  const tableInstance = useTable({ columns, data });

  const onClick = useCallback(() => {
    changeSceneToAdd();
    navigate('/trainers/add');
  }, [changeSceneToAdd, navigate]);

  useEffect(() => {
    dispatchFetchTrainers();
  }, [dispatchFetchTrainers]);

  useEffect(() => {
    if (edited) {
      dispatchResetTrainer();
      dispatchFetchTrainers();
      toast.success('Trainer successfully updated!');
    }
  }, [edited, dispatchFetchTrainers, dispatchResetTrainer]);

  return (
    <Fragment>
      <Container>
        <Title type="h1">Trainers</Title>
        <Button type="button" size="xs" onClick={onClick}>
          Add New Trainer
        </Button>
      </Container>
      <Table tableInstance={tableInstance} />
    </Fragment>
  );
};

export default Trainers;
