/**
 * @flow
 */

import { pick, first } from 'lodash';
import { DateTime } from 'luxon';

import type { Owner, FormData } from 'types/owner';
import { disciplineOptions } from 'utils/constants';

export const mapOwnerToFormData = (owner: Owner): FormData => {
  const ownerData = pick(owner, [
    'emiratesId',
    'visa',
    'firstName',
    'lastName',
    'nationality',
    'pobox',
  ]);

  const formData = {
    feiRegistrationNo: owner.feiRegistration.no,
    feiRegistrationDate: new DateTime(owner.feiRegistration.date).toFormat(
      'yyyy-LL-dd'
    ),
    uaeAddress: owner.address.uae.address,
    uaeCity: owner.address.uae.city,
    uaeCountry: { name: 'United Arab Emirates', value: 'UAE' },
    contactEmail: owner.email,
    contactMobile: owner.contact.personal.mobile,
    contactTelephone: owner.contact.personal.telephone,
    homeAddress: owner.address.home.address,
    homeCity: owner.address.home.city,
    homeCountry: { name: 'United Arab Emirates', value: 'UAE' },
    contactTelHome: owner.contact.home.telephone,
    contactMobHome: owner.contact.home.mobile,
    documentExpiry: owner.documents
      ? new DateTime(owner.documents.documentExpiry).toFormat('yyyy-LL-dd')
      : null,
    discipline:
      disciplineOptions.find((option) => option.value === owner.discipline) ||
      first(disciplineOptions),
    ...ownerData,
  };

  return formData;
};
