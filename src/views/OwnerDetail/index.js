/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useOwner } from 'views/Owners/hook';
import type { State } from 'reducers/types';
import type { OwnerDetailState } from 'reducers/owners/detail';

import Form from './Form';
import { mapOwnerToFormData } from './utils';

const Container = tw.div`p-8 bg-white`;

const OwnerDetail = (): Node => {
  const { id } = useParams();
  const navigate = useNavigate();
  const {
    data: ownerDetail,
    status: { fetching, fetched, added, edited },
  } = useSelector<State, OwnerDetailState>((state) => state.owners.detail);
  const { dispatchFetchOwner, dispatchResetOwner } = useOwner();

  useEffect(() => {
    return () => {
      dispatchResetOwner();
    };
  }, [dispatchResetOwner]);

  useEffect(() => {
    if (id) {
      dispatchFetchOwner(id);
    }
  }, [id, dispatchFetchOwner]);

  useEffect(() => {
    if (added) {
      toast.success('Owner succssfully registered!');
      navigate('/owners');
    }
  }, [added, navigate, dispatchResetOwner]);

  useEffect(() => {
    if (edited) {
      // TODO: Hide toast if nothing was changed
      toast.success('Owner succssfully updated!');
      navigate('/owners');
    }
  }, [edited, navigate, dispatchResetOwner]);

  return (
    <Container>
      {fetching && <div>Loading...</div>}
      {fetched && <Form defaultValues={mapOwnerToFormData(ownerDetail)} />}
      {!fetching && !fetched && <Form />}
    </Container>
  );
};

export default OwnerDetail;
