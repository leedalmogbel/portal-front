/**
 * @flow
 */

import { useState, useContext, createContext } from 'react';
import type { Node } from 'react';

import type { Scene } from 'types';

type ContextType = {
  sceneIsAdd: boolean,
  sceneIsEdit: boolean,
  sceneIsView: boolean,
  changeSceneToAdd: Function,
  changeSceneToEdit: Function,
  changeSceneToView: Function,
};

const initialValues = {
  scene: 'view',
  sceneIsAdd: false,
  sceneIsEdit: false,
  sceneIsView: true,
  changeSceneToAdd: () => {},
  changeSceneToEdit: () => {},
  changeSceneToView: () => {},
};

const OwnerContext = createContext<ContextType>(initialValues);

type OwnerDetailProviderProps = {
  children: ?Node,
};

const OwnerDetailProvider = ({ children }: OwnerDetailProviderProps): Node => {
  const [scene, setScene] = useState<Scene>(initialValues.scene);
  const sceneIsAdd = scene === 'add',
    sceneIsEdit = scene === 'edit',
    sceneIsView = scene === 'view';

  const changeSceneToAdd = () => {
    setScene('add');
  };

  const changeSceneToEdit = () => {
    setScene('edit');
  };

  const changeSceneToView = () => {
    setScene('view');
  };

  return (
    <OwnerContext.Provider
      value={{
        sceneIsAdd,
        sceneIsEdit,
        sceneIsView,
        changeSceneToAdd,
        changeSceneToEdit,
        changeSceneToView,
      }}>
      {children}
    </OwnerContext.Provider>
  );
};

const useOwnerDetail = (): ContextType => {
  const context = useContext(OwnerContext);
  if (context === undefined) {
    throw new Error(
      'useOwnerDetail must be used within a OwnerContextProvider'
    );
  }
  return context;
};

export { OwnerDetailProvider, useOwnerDetail };
