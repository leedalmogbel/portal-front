/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useTrainer } from 'views/Trainers/hook';
import type { State } from 'reducers/types';
import type { TrainerDetailState } from 'reducers/trainers/detail';

import Form from './Form';
import { mapTrainerToFormData } from './utils';

const Container = tw.div`p-8 bg-white`;

const TrainerDetail = (): Node => {
  const { id } = useParams();
  const navigate = useNavigate();
  const {
    data: trainerDetail,
    status: { fetching, fetched, added, edited },
  } = useSelector<State, TrainerDetailState>((state) => state.trainers.detail);
  const { dispatchFetchTrainer, dispatchResetTrainer } = useTrainer();

  useEffect(() => {
    return () => {
      dispatchResetTrainer();
    };
  }, [dispatchResetTrainer]);

  useEffect(() => {
    if (id) {
      dispatchFetchTrainer(id);
    }
  }, [id, dispatchFetchTrainer]);

  useEffect(() => {
    if (added) {
      toast.success('Trainer succssfully registered!');
      navigate('/trainers');
    }
  }, [added, navigate, dispatchResetTrainer]);

  useEffect(() => {
    if (edited) {
      // TODO: Hide toast if nothing was changed
      toast.success('Trainer succssfully updated!');
      navigate('/trainers');
    }
  }, [edited, navigate, dispatchResetTrainer]);

  return (
    <Container>
      {fetching && <div>Loading...</div>}
      {fetched && <Form defaultValues={mapTrainerToFormData(trainerDetail)} />}
      {!fetching && !fetched && <Form />}
    </Container>
  );
};

export default TrainerDetail;
