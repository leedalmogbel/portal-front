/**
 * @flow
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';
import { useSelector } from 'react-redux';

import Textarea from 'components/common/Textarea';
import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Error from 'components/common/Error';
import type { State } from 'reducers/types';
import type { TrainerDetailState } from 'reducers/trainers/detail';

import { useTrainerDetail } from '../hook';

const Container = tw.div`flex gap-10 items-center bg-gray-100 p-4 mb-4`,
  FlexContainer = tw.div`flex gap-10 mb-4`,
  FormGroup = tw.div`w-3/12`,
  Text = tw.span`uppercase`,
  Document = tw.a`font-medium text-red-600`;

const DocumentsVerificationFields = (): Node => {
  const { errors } = useFormState();
  const { data: trainer } = useSelector<State, TrainerDetailState>(
    (state) => state.trainers.detail
  );
  const { sceneIsView } = useTrainerDetail();
  const readOnly = sceneIsView;

  return (
    <Fragment>
      <Container>
        <FormGroup>
          <Text>National ID</Text>
        </FormGroup>
        <FormGroup>
          <Label htmlFor="documentExpiry">Document Expiry</Label>
          <Input
            name="documentExpiry"
            id="documentExpiry"
            type="date"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>
            {errors.documentExpiry && 'Document Expiry is required'}
          </Error>
        </FormGroup>
        <FormGroup>
          {!sceneIsView ? (
            <Fragment>
              <Label htmlFor="documentExpiryFile">Select File</Label>
              <Input
                name="documentExpiryFile"
                id="documentExpiryFile"
                type="file"
                rules={{ required: true }}
                readOnly={readOnly}
              />
              <Error>
                {errors.documentExpiryFile && 'Document File is required'}
              </Error>
            </Fragment>
          ) : (
            trainer?.documents && (
              <Fragment>
                <Label htmlFor="documentExpiryFile">Document File</Label>
                <Document href={trainer.documents.filePath} target="_blank">
                  document
                </Document>
              </Fragment>
            )
          )}
        </FormGroup>
      </Container>
      <FlexContainer>
        <Label htmlFor="remarks">Remarks</Label>
        <Textarea id="remarks" name="remarks" readOnly={readOnly} />
      </FlexContainer>
    </Fragment>
  );
};

export default DocumentsVerificationFields;
