/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { first, isEmpty } from 'lodash';
import { useNavigate } from 'react-router-dom';

import { useTrainer } from 'views/Trainers/hook';
import { useAuth } from 'hooks/auth';
import type { FormData } from 'types/trainer';
import type { Option } from 'types';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import {
  locationOptions,
  disciplineOptions,
  visaCategoryOptions,
} from 'utils/constants';
import type { State } from 'reducers/types';
import type { TrainerDetailState } from 'reducers/trainers/detail';
import type { AuthState } from 'reducers/auth';

import RegistrationDetailsFields from './Fields/RegistrationDetails';
import ContactInformationFields from './Fields/ContactInformation';
import DocumentsVerificationFields from './Fields/DocumentsVerification';
import { useTrainerDetail } from './hook';

const Title = tw(BaseTitle)`mb-4 text-red-600`,
  Buttons = tw.div`flex gap-2`;

type FormProps = {
  defaultValues?: FormData,
};

const Form = ({ defaultValues }: FormProps): Node => {
  const { dispatchAddTrainer, dispatchEditTrainer } = useTrainer();
  const { user } = useAuth();
  const { data: trainer } = useSelector<State, TrainerDetailState>(
    (state) => state.trainers.detail
  );
  const navigate = useNavigate();
  const { sceneIsAdd, sceneIsEdit, sceneIsView, changeSceneToView } =
    useTrainerDetail();

  const methods = useForm({
    defaultValues,
  });

  const onSubmit = (data) => {
    if (sceneIsAdd) {
      dispatchAddTrainer(data);
    } else {
      dispatchEditTrainer(data, trainer.id);
    }
  };

  const onCancel = () => {
    if (sceneIsAdd) {
      navigate('/trainers');
    } else if (sceneIsEdit) {
      changeSceneToView();
    }
  };

  useEffect(() => {
    if ((sceneIsView || sceneIsEdit) && !isEmpty(user)) {
      methods.setValue('userId', user.id);
    }
  }, [user, methods, sceneIsView, sceneIsEdit]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(user)) {
      methods.setValue('userId', user.id);
    }
  }, [user, methods, sceneIsAdd]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <Title type="h4">Registration Details</Title>
        <RegistrationDetailsFields />
        <Title type="h4">Contact Information</Title>
        <ContactInformationFields />
        <Title type="h4">Documents Verification</Title>
        <DocumentsVerificationFields />
        <Buttons>
          {!sceneIsView && (
            <Button type="submit" size="xs">
              {sceneIsAdd ? 'Create' : 'Update'}
            </Button>
          )}
          {(sceneIsAdd || sceneIsEdit) && (
            <Button type="button" size="xs" onClick={onCancel}>
              Cancel
            </Button>
          )}
        </Buttons>
      </form>
    </FormProvider>
  );
};

Form.defaultProps = {
  defaultValues: {
    emiratesId: '',
    discipline: (first(disciplineOptions): Option),
    feiRegistrationNo: '',
    feiRegistrationDate: null,
    visa: (first(visaCategoryOptions): Option),
    firstName: '',
    lastName: '',
    nationality: '',
    uaeAddress: '',
    uaeCity: (first(locationOptions): Option),
    pobox: '',
    uaeCountry: { name: 'United Arab Emirates', value: 'UAE' },
    contactEmail: '',
    contactMobile: '',
    contactTelephone: '',
    homeAddress: '',
    homeCity: '',
    homeCountry: { name: 'United Arab Emirates', value: 'UAE' },
    contactTelHome: '',
    contactMobHome: '',
    documentExpiry: null,
    documentExpiryFile: null,
    userId: '',
  },
};

export default Form;
