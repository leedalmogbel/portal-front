/**
 * @flow
 */

import { pick, first } from 'lodash';
import { DateTime } from 'luxon';

import type { Trainer, FormData } from 'types/trainer';
import { disciplineOptions } from 'utils/constants';

export const mapTrainerToFormData = (trainer: Trainer): FormData => {
  const trainerData = pick(trainer, [
    'emiratesId',
    'visa',
    'firstName',
    'lastName',
    'nationality',
    'pobox',
  ]);

  const formData = {
    feiRegistrationNo: trainer.feiRegistration.no,
    feiRegistrationDate: new DateTime(trainer.feiRegistration.date).toFormat(
      'yyyy-LL-dd'
    ),
    uaeAddress: trainer.address.uae.address,
    uaeCity: trainer.address.uae.city,
    uaeCountry: { name: 'United Arab Emirates', value: 'UAE' },
    contactEmail: trainer.contactEmail,
    contactMobile: trainer.contact.personal.mobile,
    contactTelephone: trainer.contact.personal.telephone,
    homeAddress: trainer.address.home.address,
    homeCity: trainer.address.home.city,
    homeCountry: { name: 'United Arab Emirates', value: 'UAE' },
    contactTelHome: trainer.contact.home.telephone,
    contactMobHome: trainer.contact.home.mobile,
    documentExpiry: trainer.documents
      ? new DateTime(trainer.documents.documentExpiry).toFormat('yyyy-LL-dd')
      : null,
    discipline:
      disciplineOptions.find((option) => option.value === trainer.discipline) ||
      first(disciplineOptions),
    ...trainerData,
  };

  return formData;
};
