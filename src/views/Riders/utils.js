/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import {
  CheckIcon,
  XIcon,
  PencilAltIcon,
  EyeIcon,
} from '@heroicons/react/solid';
import { isEmpty } from 'lodash';
import { GrDocumentPdf } from 'react-icons/gr';

import type { Rider } from 'types/rider';
import type { Option } from 'types';
import Status from 'components/common/Status';
import Dropdown from 'components/common/Dropdown';
import type { DropdownItem } from 'components/common/Dropdown';

type TableData = {
  name: Node,
  email: string,
  documents: ?Node,
  country: string,
  visa: string,
  status: Node,
  actions: Node,
};

const PdfLink = tw.a`text-3xl text-center cursor-pointer`;

export const mapRidersToTableData = (
  riders: Array<Rider>,
  actions: Object
): Array<TableData> =>
  riders.map(
    ({
      id,
      firstName,
      lastName,
      email,
      address: {
        home: { country },
      },
      visa,
      status,
      documents,
    }) => ({
      name: (
        <Fragment>
          <p tw="font-bold">
            {firstName} {lastName}
          </p>
        </Fragment>
      ),
      email,
      documents: !isEmpty(documents) && (
        // <a href={documents.filePath} target="_blank" rel="noreferrer">
        //   Document
        // </a>
        <PdfLink href={documents.filePath} target="_blank" rel="noreferrer">
          <GrDocumentPdf />
        </PdfLink>
      ),
      country,
      visa,
      status: <Status status={status} />,
      actions: !isEmpty(actions) && mapActionsToDropdownItems(actions, id),
    })
  );

const mapActionsToDropdownItems = (actions: Object, id: string) => {
  const actionItems: Array<Array<DropdownItem>> = [
    [
      {
        label: 'Approve',
        onClick: actions.approve,
        Icon: CheckIcon,
      },
      {
        label: 'Reject',
        onClick: actions.reject,
        Icon: XIcon,
      },
    ],
    [
      {
        label: 'View',
        onClick: actions.view,
        Icon: EyeIcon,
      },
      {
        label: 'Edit',
        onClick: actions.edit,
        Icon: PencilAltIcon,
      },
    ],
  ];

  return <Dropdown items={actionItems} id={id} />;
};

export const mapRidersToOptions = (riders: Array<Rider>): Array<Option> => {
  return riders.map(({ firstName, lastName, id }) => ({
    name: `[${id}] ${firstName} ${lastName} `,
    value: id.toString(),
  }));
};
