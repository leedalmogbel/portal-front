/**
 * @flow
 */

import { useState, useContext, createContext, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomCompareCallback } from 'use-custom-compare';
import { isEqual } from 'lodash';

import { resetRider } from 'actions/riders';
import { editRiderStatus, editRider } from 'actions/riders/edit';
import { fetchRiders, fetchRider } from 'actions/riders/fetch';
import { addRider } from 'actions/riders/add';
import { useAuth } from 'hooks/auth';
import type { Rider } from 'types/rider';
import type { State } from 'reducers/types';
import type { RidersState } from 'reducers/riders/all';
import type { Option } from 'types';

import { mapRidersToOptions } from './utils';

type ContextType = {
  riderOptions: Array<Option>,
  dispatchAddRider: Function,
  dispatchEditRider: Function,
  dispatchFetchRiders: Function,
  dispatchFetchRider: Function,
  dispatchApproveRider: Function,
  dispatchRejectRider: Function,
  dispatchResetRider: Function,
};

const initialValues = {
  riderOptions: [],
  dispatchAddRider: () => {},
  dispatchEditRider: () => {},
  dispatchFetchRiders: () => {},
  dispatchFetchRider: () => {},
  dispatchApproveRider: () => {},
  dispatchRejectRider: () => {},
  dispatchResetRider: () => {},
};

const RiderContext = createContext<ContextType>(initialValues);

type RiderProviderProps = {
  children: ?Node,
};

const RiderProvider = ({ children }: RiderProviderProps): Node => {
  const dispatch = useDispatch();
  const [riderOptions, setRiderOptions] = useState<Array<Option>>(
    initialValues.riderOptions
  );
  const {
    data: riders,
    status: { fetched },
  } = useSelector<State, RidersState>((state) => state.riders.all);
  // TODO: Remove selector
  const { user } = useAuth();

  const dispatchFetchRiders = useCustomCompareCallback(
    (_id) => {
      const id = _id || user.id;
      dispatch(fetchRiders({ id }));
    },
    [dispatch, user],
    (prevDeps, nextDeps) => isEqual(prevDeps, nextDeps)
  );

  const dispatchFetchRider = (id: string) => {
    dispatch(fetchRider(id));
  };

  const dispatchAddRider = (rider: Rider) => {
    dispatch(addRider(rider));
  };

  const dispatchEditRider = (rider: Rider, id: string) => {
    dispatch(editRider(rider, id));
  };

  const dispatchResetRider = () => {
    dispatch(resetRider());
  };

  const dispatchApproveRider = (id) => {
    dispatch(editRiderStatus(id, 'approved'));
  };

  const dispatchRejectRider = (id) => {
    dispatch(editRiderStatus(id, 'rejected'));
  };

  useEffect(() => {
    if (fetched) {
      setRiderOptions(mapRidersToOptions(riders));
    }
  }, [fetched, riders]);

  return (
    <RiderContext.Provider
      value={{
        riderOptions,
        setRiderOptions,
        dispatchAddRider,
        dispatchEditRider,
        dispatchFetchRiders,
        dispatchFetchRider,
        dispatchApproveRider,
        dispatchRejectRider,
        dispatchResetRider,
      }}>
      {children}
    </RiderContext.Provider>
  );
};

const useRider = (): ContextType => {
  const context = useContext(RiderContext);
  if (context === undefined) {
    throw new Error('useRider must be used within a RiderContextProvider');
  }
  return context;
};

export { RiderProvider, useRider };
