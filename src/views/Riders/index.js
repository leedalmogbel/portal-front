/**
 * @flow
 */

import { Fragment, useCallback, useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useTable } from 'react-table';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useRiderDetail } from 'views/RiderDetail/hook';
import Button from 'components/common/Button';
import Title from 'components/common/Title';
import Table from 'components/common/Table';
import type { State } from 'reducers/types';
import type { RiderDetailState } from 'reducers/riders/detail';
import type { RidersState } from 'reducers/riders/all';

import { mapRidersToTableData } from './utils';
import { useRider } from './hook';
import { tableHeader } from './constants';

const Container = tw.div`flex mb-4 justify-between`;

const Riders = (): Node => {
  const {
    dispatchFetchRiders,
    dispatchApproveRider,
    dispatchRejectRider,
    dispatchResetRider,
  } = useRider();
  const { data: riders } = useSelector<State, RidersState>(
    (state) => state.riders.all
  );
  const {
    status: { edited },
  } = useSelector<State, RiderDetailState>((state) => state.riders.detail);
  const navigate = useNavigate();
  const { changeSceneToAdd, changeSceneToEdit, changeSceneToView } =
    useRiderDetail();
  const columns = tableHeader;

  const actions = {
    approve: dispatchApproveRider,
    reject: dispatchRejectRider,
    view: (id) => {
      changeSceneToView();
      navigate(`/riders/${id}`);
    },
    edit: (id) => {
      changeSceneToEdit();
      navigate(`/riders/${id}`);
    },
  };
  const data = mapRidersToTableData(riders, actions);
  const tableInstance = useTable({ columns, data });

  const onClick = useCallback(() => {
    changeSceneToAdd();
    navigate('/riders/add');
  }, [changeSceneToAdd, navigate]);

  useEffect(() => {
    dispatchFetchRiders();
  }, [dispatchFetchRiders]);

  useEffect(() => {
    if (edited) {
      dispatchResetRider();
      dispatchFetchRiders();
      toast.success('Rider successfully updated!');
    }
  }, [edited, dispatchFetchRiders, dispatchResetRider]);

  return (
    <Fragment>
      <Container>
        <Title type="h1">Riders</Title>
        <Button type="button" size="xs" onClick={onClick}>
          Add New Rider
        </Button>
      </Container>
      <Table tableInstance={tableInstance} />
    </Fragment>
  );
};

export default Riders;
