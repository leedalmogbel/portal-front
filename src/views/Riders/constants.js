/**
 * @flow
 */

import type { TableHeader } from 'components/common/Table';

export const tableHeader: Array<TableHeader> = [
  {
    Header: 'name',
    accessor: 'name',
  },
  {
    Header: 'email',
    accessor: 'email',
  },
  {
    Header: 'documents',
    accessor: 'documents',
  },
  {
    Header: 'country',
    accessor: 'country',
  },
  {
    Header: 'visa',
    accessor: 'visa',
  },
  {
    Header: 'status',
    accessor: 'status',
  },
  {
    Header: 'actions',
    accessor: 'actions',
  },
];
