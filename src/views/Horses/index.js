/**
 * @flow
 */

import { Fragment, useCallback, useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useTable } from 'react-table';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useHorseDetail } from 'views/HorseDetail/hook';
import Button from 'components/common/Button';
import Title from 'components/common/Title';
import Table from 'components/common/Table';
import type { State } from 'reducers/types';
import type { HorseDetailState } from 'reducers/horses/detail';
import type { HorsesState } from 'reducers/horses/all';

import { mapHorsesToTableData } from './utils';
import { useHorse } from './hook';
import { tableHeader } from './constants';

const Container = tw.div`flex mb-4 justify-between`;

const Horses = (): Node => {
  const {
    dispatchFetchHorses,
    dispatchApproveHorse,
    dispatchRejectHorse,
    dispatchResetHorse,
  } = useHorse();
  const { data: horses } = useSelector<State, HorsesState>(
    (state) => state.horses.all
  );
  const {
    status: { edited },
  } = useSelector<State, HorseDetailState>((state) => state.horses.detail);
  const navigate = useNavigate();
  const { changeSceneToAdd, changeSceneToEdit, changeSceneToView } =
    useHorseDetail();
  const columns = tableHeader;

  const actions = {
    approve: dispatchApproveHorse,
    reject: dispatchRejectHorse,
    view: (id) => {
      changeSceneToView();
      navigate(`/horses/${id}`);
    },
    edit: (id) => {
      changeSceneToEdit();
      navigate(`/horses/${id}`);
    },
  };
  const data = mapHorsesToTableData(horses, actions);
  const tableInstance = useTable({ columns, data });

  const onClick = useCallback(() => {
    changeSceneToAdd();
    navigate('/horses/add');
  }, [changeSceneToAdd, navigate]);

  useEffect(() => {
    dispatchResetHorse();
    dispatchFetchHorses();
  }, [dispatchFetchHorses]);

  useEffect(() => {
    if (edited) {
      dispatchResetHorse();
      dispatchFetchHorses();
      toast.success('Horse successfully updated!');
    }
  }, [edited, dispatchFetchHorses, dispatchResetHorse]);

  return (
    <Fragment>
      <Container>
        <Title type="h1">Horses</Title>
        <Button type="button" size="xs" onClick={onClick}>
          Add New Horse
        </Button>
      </Container>
      <Table tableInstance={tableInstance} />
    </Fragment>
  );
};

export default Horses;
