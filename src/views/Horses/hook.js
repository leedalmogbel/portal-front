/**
 * @flow
 */

import { useState, useContext, createContext, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomCompareCallback } from 'use-custom-compare';
import { isEqual } from 'lodash';

import { resetHorse } from 'actions/horses';
import { editHorseStatus, editHorse } from 'actions/horses/edit';
import { fetchHorses, fetchHorse } from 'actions/horses/fetch';
import { addHorse } from 'actions/horses/add';
import type { Horse } from 'types/horse';
import type { State } from 'reducers/types';
import type { HorsesState } from 'reducers/horses/all';
import type { Option } from 'types';
import { useAuth } from 'hooks/auth';

import { mapHorsesToOptions } from './utils';

type ContextType = {
  horseOptions: Array<Option>,
  dispatchAddHorse: Function,
  dispatchEditHorse: Function,
  dispatchFetchHorses: Function,
  dispatchFetchHorse: Function,
  dispatchApproveHorse: Function,
  dispatchRejectHorse: Function,
  dispatchResetHorse: Function,
};

const initialValues = {
  horseOptions: [],
  dispatchAddHorse: () => {},
  dispatchEditHorse: () => {},
  dispatchFetchHorses: () => {},
  dispatchFetchHorse: () => {},
  dispatchApproveHorse: () => {},
  dispatchRejectHorse: () => {},
  dispatchResetHorse: () => {},
};

const HorseContext = createContext<ContextType>(initialValues);

type HorseProviderProps = {
  children: ?Node,
};

const HorseProvider = ({ children }: HorseProviderProps): Node => {
  const dispatch = useDispatch();
  const [horseOptions, setHorseOptions] = useState<Array<Option>>(
    initialValues.horseOptions
  );
  const {
    data: horses,
    status: { fetched },
  } = useSelector<State, HorsesState>((state) => state.horses.all);

  // TODO: Remove selector
  const { user } = useAuth();

  // TODO: Remove auth id
  // const dispatchFetchHorses = () => {
  //   dispatch(fetchHorses(auth.id));
  // };

  const dispatchFetchHorses = useCustomCompareCallback(
    (_id) => {
      const id = _id || user.id;
      dispatch(fetchHorses({ id }));
    },
    [dispatch, user],
    (prevDeps, nextDeps) => isEqual(prevDeps, nextDeps)
  );

  const dispatchFetchHorse = (id: string) => {
    dispatch(fetchHorse(id));
  };

  const dispatchAddHorse = (horse: Horse) => {
    dispatch(addHorse(horse));
  };

  const dispatchEditHorse = (horse: Horse, id: string) => {
    dispatch(editHorse(horse, id));
  };

  const dispatchResetHorse = () => {
    dispatch(resetHorse());
  };

  const dispatchApproveHorse = (id) => {
    dispatch(editHorseStatus(id, 'approved'));
  };

  const dispatchRejectHorse = (id) => {
    dispatch(editHorseStatus(id, 'rejected'));
  };

  useEffect(() => {
    if (fetched) {
      setHorseOptions(mapHorsesToOptions(horses));
    }
  }, [fetched, horses]);

  return (
    <HorseContext.Provider
      value={{
        horseOptions,
        setHorseOptions,
        dispatchAddHorse,
        dispatchEditHorse,
        dispatchFetchHorses,
        dispatchFetchHorse,
        dispatchApproveHorse,
        dispatchRejectHorse,
        dispatchResetHorse,
      }}>
      {children}
    </HorseContext.Provider>
  );
};

const useHorse = (): ContextType => {
  const context = useContext(HorseContext);
  if (context === undefined) {
    throw new Error('useHorse must be used within a HorseContextProvider');
  }
  return context;
};

export { HorseProvider, useHorse };
