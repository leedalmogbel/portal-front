/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import {
  CheckIcon,
  XIcon,
  PencilAltIcon,
  EyeIcon,
} from '@heroicons/react/solid';
import { isEmpty } from 'lodash';
import { GrDocumentPdf } from 'react-icons/gr';

import type { Horse } from 'types/horse';
import Status from 'components/common/Status';
import Dropdown from 'components/common/Dropdown';
import type { DropdownItem } from 'components/common/Dropdown';

type TableData = {
  name: Node,
  sire: string,
  dam: string,
  feiPassportNo: string,
  breed: string,
  sireOfDam: string,
  documents: ?Node,
  status: Node,
  actions: Node,
};

const PdfLink = tw.a`text-3xl text-center cursor-pointer`;

export const mapHorsesToTableData = (
  horses: Array<Horse>,
  actions: Object
): Array<TableData> =>
  horses.map(
    ({
      id,
      name,
      originalName,
      microchipNum,
      sire,
      dam,
      sireOfDam,
      status,
      documents,
      breed,
      countryBirth,
    }) => ({
      name: (
        <Fragment>
          <p tw="font-bold">{name}</p>
          <span tw="text-sm text-gray-500">{originalName}</span>
        </Fragment>
      ),
      microchipNum,
      sire,
      dam,
      sireOfDam,
      documents: !isEmpty(documents) && (
        <PdfLink href={documents.filePath} target="_blank" rel="noreferrer">
          <GrDocumentPdf />
        </PdfLink>
      ),
      breed,
      countryBirth,
      status: <Status status={status} />,
      actions: !isEmpty(actions) && mapActionsToDropdownItems(actions, id),
    })
  );

const mapActionsToDropdownItems = (actions: Object, id: string) => {
  const actionItems: Array<Array<DropdownItem>> = [
    [
      {
        label: 'Approve',
        onClick: actions.approve,
        Icon: CheckIcon,
      },
      {
        label: 'Reject',
        onClick: actions.reject,
        Icon: XIcon,
      },
    ],
    [
      {
        label: 'View',
        onClick: actions.view,
        Icon: EyeIcon,
      },
      {
        label: 'Edit',
        onClick: actions.edit,
        Icon: PencilAltIcon,
      },
    ],
  ];

  return <Dropdown items={actionItems} id={id} />;
};

export const mapHorsesToOptions = (horses: Array<Horse>): Array<Option> => {
  return horses.map(({ name, id }) => ({
    name: `[${id}] ${name} `,
    value: id.toString(),
  }));
};
