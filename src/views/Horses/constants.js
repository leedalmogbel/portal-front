/**
 * @flow
 */

import type { TableHeader } from 'components/common/Table';

export const tableHeader: Array<TableHeader> = [
  {
    Header: 'name',
    accessor: 'name',
  },
  {
    Header: 'breed',
    accessor: 'breed',
  },
  {
    Header: 'documents',
    accessor: 'documents',
  },
  {
    Header: 'country',
    accessor: 'countryBirth',
  },
  {
    Header: 'Microchip No',
    accessor: 'microchipNum',
  },
  {
    Header: 'status',
    accessor: 'status',
  },
  {
    Header: 'actions',
    accessor: 'actions',
  },
];
