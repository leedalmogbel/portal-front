/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Select from 'components/common/Select';
import Error from 'components/common/Error';
import {
  eventLocationOptions,
  countryOptions,
  visaCategoryOptions,
} from 'utils/constants';

import { useEventDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`,
  OneThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc((100% - 80px) / 3)])`,
  Required = tw.span`text-red-500 font-black text-xl`;

const RegistrationDetailsFields = (): Node => {
  const { errors } = useFormState();
  const { sceneIsView } = useEventDetail();
  const readOnly = sceneIsView;

  return (
    <Fragment>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="name">
            Event Title
            <Required> *</Required>
          </Label>
          <Input
            name="name"
            id="name"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.name && 'Event Title is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="name">
            Event Description
            <Required> *</Required>
          </Label>
          <Input
            name="description"
            id="description"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.description && 'Event Description is required'}</Error>
        </FormGroup>
      </TwoThirdColumnContainer>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="location">
            Location
            <Required> *</Required>
          </Label>
          <Select
            name="location"
            id="location"
            options={eventLocationOptions}
            readOnly={readOnly}
          />
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="country">
            Country
            <Required> *</Required>
          </Label>
          <Select
            name="country"
            id="country"
            options={countryOptions}
            readOnly={readOnly}
          />
        </FormGroup>
      </TwoThirdColumnContainer>
      <FlexContainer>
        <FormGroup tw="w-4/12">
          <Label htmlFor="startDate">
            Start Date
            <Required> *</Required>
          </Label>
          <Input
            name="startDate"
            id="startDate"
            type="date"
            format="yyyy-mm-dd"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.startDate && 'Event Date is required'}</Error>
        </FormGroup>
        <FormGroup tw="w-4/12">
          <Label htmlFor="endDate">
            End Date
            <Required> *</Required>
          </Label>
          <Input
            name="endDate"
            id="endDate"
            type="date"
            format="yyyy-mm-dd"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.endDate && 'End Date is required'}</Error>
        </FormGroup>
      </FlexContainer>
      {/* <OneThirdColumnContainer>
        <FormGroup tw="">
          <Label htmlFor="pledge">Pledge</Label>
          <Input
            name="pledge"
            id="pledge"
            type="checkbox"
            rules={{ required: false }}
            readOnly={readOnly}
          />
          <Error>{errors.pledge && 'Pledge is required'}</Error>
        </FormGroup>
      </OneThirdColumnContainer> */}
    </Fragment>
  );
};

export default RegistrationDetailsFields;
