/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Select from 'components/common/Select';
import Error from 'components/common/Error';

import { useEventDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`;

const ContactInformationFields = (): Node => {
  const { errors } = useFormState();
  const { sceneIsView } = useEventDetail();
  const readOnly = sceneIsView;

  return (
    <Fragment>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="contactEmail">Email</Label>
          <Input
            name="contactEmail"
            id="contactEmail"
            type="email"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.contactEmail && 'Email is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="contactMobile">Mobile</Label>
          <Input
            name="contactMobile"
            id="contactMobile"
            type="tel"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.contactMobile && 'Mobile is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="contactTelephone">Phone No.</Label>
          <Input
            name="contactTelephone"
            id="contactTelephone"
            type="tel"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.contactTelephone && 'Phone No. is required'}</Error>
        </FormGroup>
      </FlexContainer>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="homeAddress">Home Address</Label>
          <Input
            name="homeAddress"
            id="homeAddress"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.homeAddress && 'Home Address is required'}</Error>
        </FormGroup>
        <FormGroup tw="lg:(w-[calc((100% - 80px) / 3)])">
          <Label htmlFor="homeCity">Home City</Label>
          <Input
            name="homeCity"
            id="homeCity"
            type="text"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>{errors.homeCity && 'Home City is required'}</Error>
        </FormGroup>
      </FlexContainer>
      <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="homeCountry">Home Country</Label>
          <Select
            name="homeCountry"
            id="homeCountry"
            options={[{ name: 'United Arab Emirates', value: 'UAE' }]}
            readOnly={readOnly}
          />
          <Error>{errors.homeCountry && 'Home Country is required'}</Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="contactTelHome">Home Telephone No.</Label>
          <Input
            name="contactTelHome"
            id="contactTelHome"
            type="tel"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>
            {errors.contactTelHome && 'Home Telephone No. is required'}
          </Error>
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="contactMobHome">Home Mobile No.</Label>
          <Input
            name="contactMobHome"
            id="contactMobHome"
            type="tel"
            rules={{ required: true }}
            readOnly={readOnly}
          />
          <Error>
            {errors.contactMobHome && 'Home Mobile No. is required'}
          </Error>
        </FormGroup>
      </FlexContainer>
    </Fragment>
  );
};

export default ContactInformationFields;
