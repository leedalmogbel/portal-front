/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { first, isEmpty } from 'lodash';
import { useNavigate } from 'react-router-dom';

import { useEvent } from 'views/Events/hook';
import type { FormData } from 'types/event';
import type { Option } from 'types';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import {
  eventLocationOptions,
  countryOptions,
  visaCategoryOptions,
} from 'utils/constants';
import type { State } from 'reducers/types';
import type { EventDetailState } from 'reducers/events/detail';
import type { AuthState } from 'reducers/auth';

import RegistrationDetailsFields from './Fields/RegistrationDetails';
import DocumentsVerificationFields from './Fields/DocumentsVerification';
import { useEventDetail } from './hook';

const Title = tw(BaseTitle)`mb-4 text-red-600`,
  Buttons = tw.div`flex gap-2`;

type FormProps = {
  defaultValues?: FormData,
};

const Form = ({ defaultValues }: FormProps): Node => {
  const { dispatchAddEvent, dispatchEditEvent } = useEvent();
  const { data: auth } = useSelector<State, AuthState>((state) => state.auth);
  const { data: event } = useSelector<State, EventDetailState>(
    (state) => state.events.detail
  );
  const navigate = useNavigate();
  const { sceneIsAdd, sceneIsEdit, sceneIsView, changeSceneToView } =
    useEventDetail();

  const methods = useForm({
    defaultValues,
  });

  const onSubmit = (data) => {
    console.log('tatatatata', data);
    if (sceneIsAdd) {
      dispatchAddEvent(data);
    } else {
      dispatchEditEvent(data, event.id);
    }
  };

  const onCancel = () => {
    if (sceneIsAdd) {
      navigate('/events');
    } else if (sceneIsEdit) {
      changeSceneToView();
    }
  };

  useEffect(() => {
    if ((sceneIsView || sceneIsEdit) && !isEmpty(auth)) {
      methods.setValue('userId', auth.id);
    }
  }, [auth, methods, sceneIsView, sceneIsEdit]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(auth)) {
      methods.setValue('userId', auth.id);
    }
  }, [auth, methods, sceneIsAdd]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <Title type="h4">Event Details</Title>
        <RegistrationDetailsFields />
        <Buttons>
          {!sceneIsView && (
            <Button type="submit" size="xs">
              {sceneIsAdd ? 'Create' : 'Update'}
            </Button>
          )}
          {(sceneIsAdd || sceneIsEdit) && (
            <Button type="button" size="xs" onClick={onCancel}>
              Cancel
            </Button>
          )}
        </Buttons>
      </form>
    </FormProvider>
  );
};

Form.defaultProps = {
  defaultValues: {
    name: '',
    description: '',
    location: (first(eventLocationOptions): Option),
    country: (first(countryOptions): Option),
    startDate: null,
    endDate: null,
    seasonId: 1,
    userId: '',
  },
};

export default Form;
