/**
 * @flow
 */

import { useState, useContext, createContext } from 'react';
import type { Node } from 'react';

import type { Scene } from 'types';

type ContextType = {
  sceneIsAdd: boolean,
  sceneIsEdit: boolean,
  sceneIsView: boolean,
  changeSceneToAdd: Function,
  changeSceneToEdit: Function,
  changeSceneToView: Function,
};

const initialValues = {
  scene: 'view',
  sceneIsAdd: false,
  sceneIsEdit: false,
  sceneIsView: true,
  changeSceneToAdd: () => {},
  changeSceneToEdit: () => {},
  changeSceneToView: () => {},
};

const EventContext = createContext<ContextType>(initialValues);

type EventDetailProviderProps = {
  children: ?Node,
};

const EventDetailProvider = ({ children }: EventDetailProviderProps): Node => {
  const [scene, setScene] = useState<Scene>(initialValues.scene);
  const sceneIsAdd = scene === 'add',
    sceneIsEdit = scene === 'edit',
    sceneIsView = scene === 'view';

  const changeSceneToAdd = () => {
    setScene('add');
  };

  const changeSceneToEdit = () => {
    setScene('edit');
  };

  const changeSceneToView = () => {
    setScene('view');
  };

  return (
    <EventContext.Provider
      value={{
        sceneIsAdd,
        sceneIsEdit,
        sceneIsView,
        changeSceneToAdd,
        changeSceneToEdit,
        changeSceneToView,
      }}>
      {children}
    </EventContext.Provider>
  );
};

const useEventDetail = (): ContextType => {
  const context = useContext(EventContext);
  if (context === undefined) {
    throw new Error(
      'useEventDetail must be used within a EventContextProvider'
    );
  }
  return context;
};

export { EventDetailProvider, useEventDetail };
