/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useEvent } from 'views/Events/hook';
import type { State } from 'reducers/types';
import type { EventDetailState } from 'reducers/events/detail';

import Form from './Form';
import { mapEventToFormData } from './utils';

const Container = tw.div`p-8 bg-white`;

const EventDetail = (): Node => {
  const { id } = useParams();
  const navigate = useNavigate();
  const {
    data: eventDetail,
    status: { fetching, fetched, added, edited },
  } = useSelector<State, EventDetailState>((state) => state.events.detail);
  const { dispatchFetchEvent, dispatchResetEvent } = useEvent();

  useEffect(() => {
    return () => {
      dispatchResetEvent();
    };
  }, [dispatchResetEvent]);

  useEffect(() => {
    if (id) {
      dispatchFetchEvent(id);
    }
  }, [id, dispatchFetchEvent]);

  useEffect(() => {
    if (added) {
      toast.success('Event succssfully registered!');
      navigate('/events');
    }
  }, [added, navigate, dispatchResetEvent]);

  useEffect(() => {
    if (edited) {
      // TODO: Hide toast if nothing was changed
      toast.success('Event succssfully updated!');
      navigate('/events');
    }
  }, [edited, navigate, dispatchResetEvent]);

  return (
    <Container>
      {fetching && <div>Loading...</div>}
      {fetched && <Form defaultValues={mapEventToFormData(eventDetail)} />}
      {!fetching && !fetched && <Form />}
    </Container>
  );
};

export default EventDetail;
