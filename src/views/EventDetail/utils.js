/**
 * @flow
 */

import { pick, first } from 'lodash';
import { DateTime } from 'luxon';
import { parseISO, format } from 'date-fns';

import type { Event, FormData } from 'types/event';
import { eventLocationOptions, countryOptions } from 'utils/constants';

export const mapEventToFormData = (event: Event): FormData => {
  const eventData = pick(event, ['name', 'description']);

  const startDate = parseISO(event.startDate);
  const endDate = parseISO(event.endDate);
  console.log('taeevent', event);
  const formData = {
    startDate: format(startDate, 'yyyy-MM-dd'),
    endDate: format(endDate, 'yyyy-MM-dd'),
    location:
      eventLocationOptions.find((option) => option.value === event.location) ||
      first(eventLocationOptions),
    country:
      countryOptions.find((option) => option.value === event.country) ||
      first(countryOptions),
    ...eventData,
  };

  return formData;
};
