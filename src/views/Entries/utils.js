/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import {
  CheckIcon,
  XIcon,
  PencilAltIcon,
  EyeIcon,
} from '@heroicons/react/solid';
import { isEmpty } from 'lodash';
import { parseISO, format } from 'date-fns';
import { GrDocumentPdf } from 'react-icons/gr';

import type { Entry } from 'types/entry';
import Button from 'components/common/Button';
import Status from 'components/common/Status';
import Dropdown from 'components/common/Dropdown';
import type { DropdownItem } from 'components/common/Dropdown';

const PdfLink = tw.a`text-3xl text-center cursor-pointer`;
const Poster = tw.img`object-cover h-12 w-12`;

type TableData = {
  // name: Node,
  races: ?Node,
  horses: ?Node,
  riders: ?Node,
  status: Node,
  actions: Node,
};

export const mapEntriesToTableData = (
  entries: Array<Entry>,
  actions: Object
): Array<TableData> =>
  entries.map(({ id, races, horses, riders, status }) => ({
    id: (
      <Fragment>
        <p tw="font-bold">{id}</p>
      </Fragment>
    ),
    // location,
    race: (
      <Fragment>
        <p tw="font-bold">{races.name}</p>
        <p tw="text-sm font-semibold">EVENT: {races?.events?.name}</p>
      </Fragment>
    ),
    horses: (
      <Fragment>
        <p tw="font-bold">{horses.name}</p>
        <p tw="text-sm">{horses.breed}</p>
      </Fragment>
    ),
    athletes: (
      <Fragment>
        <p tw="font-bold">{riders.firstName} </p>
        <p tw="font-bold">{riders.lastName}</p>
      </Fragment>
    ),
    status: <Status status={status} />,
    actions: !isEmpty(actions) && mapActionsToDropdownItems(actions, id),
  }));

const mapActionsToDropdownItems = (actions: Object, id: string) => {
  const actionItems: Array<Array<DropdownItem>> = [
    [
      {
        label: 'Approve',
        onClick: actions.approve,
        Icon: CheckIcon,
      },
      {
        label: 'Reject',
        onClick: actions.reject,
        Icon: XIcon,
      },
    ],
    [
      {
        label: 'View',
        onClick: actions.view,
        Icon: EyeIcon,
      },
      {
        label: 'Edit',
        onClick: actions.edit,
        Icon: PencilAltIcon,
      },
    ],
  ];

  return <Dropdown items={actionItems} id={id} />;
};
