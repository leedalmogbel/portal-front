/**
 * @flow
 */

import { Fragment, useCallback, useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useTable } from 'react-table';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useEntryDetail } from 'views/EntryDetail/hook';
import Button from 'components/common/Button';
import Title from 'components/common/Title';
import Table from 'components/common/Table';
import type { State } from 'reducers/types';
import type { EntryDetailState } from 'reducers/entries/detail';
import type { EntriesState } from 'reducers/entries/all';
import { useAuth } from 'hooks/auth';

import { mapEntriesToTableData } from './utils';
import { useEntry } from './hook';
import { tableHeader } from './constants';

const Container = tw.div`flex mb-4 justify-between`;

const Entries = (): Node => {
  const { permissions } = useAuth();
  const [searchParams] = useSearchParams();
  const {
    query,
    setQuery,
    dispatchFetchEntries,
    dispatchApproveEntry,
    dispatchRejectEntry,
    dispatchResetEntry,
  } = useEntry();
  const { data: entries } = useSelector<State, EntriesState>(
    (state) => state.entries.all
  );
  const {
    status: { edited },
  } = useSelector<State, EntryDetailState>((state) => state.entries.detail);
  const navigate = useNavigate();
  const { changeSceneToAdd, changeSceneToEdit, changeSceneToView } =
    useEntryDetail();
  const columns = tableHeader;

  const actions = {
    // approve: permissions?.users.includes('approve')
    // ? dispatchApproveEntry
    // : () => {},
    // ...(useCanApprove && { approve: dispatchApproveEntry }),
    approve: dispatchApproveEntry,
    reject: dispatchRejectEntry,
    view: (id) => {
      changeSceneToView();
      navigate(`/entries/${id}`);
    },
    edit: (id) => {
      changeSceneToEdit();
      navigate(`/entries/${id}`);
    },
  };
  const data = mapEntriesToTableData(entries, actions);
  const tableInstance = useTable({ columns, data });

  const onClick = useCallback(() => {
    changeSceneToAdd();
    navigate('/entries/add');
  }, [changeSceneToAdd, navigate]);

  useEffect(() => {
    // dispatchResetEntry();
    // dispatchFetchEntries();
  }, [dispatchFetchEntries, dispatchResetEntry]);

  useEffect(() => {
    if (edited) {
      dispatchResetEntry();
      dispatchFetchEntries();
      toast.success('Entry successfully updated!');
    }
  }, [edited, dispatchFetchEntries, dispatchResetEntry]);

  // TODO: FETCH BASED IN RACEID, IF NOT, FETCH ALL
  // useEffect(() => {
  //   if (searchParams.get('raceId')) {
  //   setQuery('raceId', searchParams.get('raceId'));
  //   dispatchResetEntry();
  //   dispatchFetchEntries();
  // }
  //   console.log('zzzzz', query);
  // }, [dispatchFetchEntries, query, dispatchResetEntry]);
  useEffect(() => {
    console.log('searchParams.get(raceId)', searchParams.get('raceId'));
    if (searchParams.get('manage')) {
      console.log('searchParams.get(qqq)', searchParams.get('manage'));
      setQuery(searchParams.get('manage'));
    }
    // if (searchParams.get('raceId')) {
    setQuery(searchParams.get('raceId'));
    dispatchResetEntry();
    dispatchFetchEntries();
    // }
  }, [dispatchFetchEntries, searchParams, setQuery, dispatchResetEntry]);
  return (
    <Fragment>
      <Container>
        <Title type="h1">Entries</Title>
        <Button type="button" size="xs" onClick={onClick}>
          Add New Entry
        </Button>
      </Container>
      <Table tableInstance={tableInstance} />
    </Fragment>
  );
};

export default Entries;
