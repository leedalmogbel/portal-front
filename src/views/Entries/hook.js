/**
 * @flow
 */

import { useState, useContext, createContext, useEffect } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useCustomCompareCallback } from 'use-custom-compare';
import { isEqual } from 'lodash';

import { resetEntry } from 'actions/entries';
import { editEntryStatus, editEntry } from 'actions/entries/edit';
import { addEntry } from 'actions/entries/add';
import { fetchEntries, fetchEntry } from 'actions/entries/fetch';
import { useAuth } from 'hooks/auth';
import type { Entry } from 'types/entry';

type ContextType = {
  query: string,
  setQuery: string,
  dispatchAddEntry: Function,
  dispatchEditEntry: Function,
  dispatchFetchEntries: Function,
  dispatchFetchEntry: Function,
  dispatchApproveEntry: Function,
  dispatchRejectEntry: Function,
  dispatchResetEntry: Function,
};

const initialValues = {
  query: '',
  setQuery: '',
  dispatchAddEntry: () => {},
  dispatchEditEntry: () => {},
  dispatchFetchEntries: () => {},
  dispatchFetchEntry: () => {},
  dispatchApproveEntry: () => {},
  dispatchRejectEntry: () => {},
  dispatchResetEntry: () => {},
};

const EntryContext = createContext<ContextType>(initialValues);

type EntryProviderProps = {
  children: ?Node,
};

const EntryProvider = ({ children }: EntryProviderProps): Node => {
  const dispatch = useDispatch();
  const [query, setQuery] = useState(initialValues.query);
  // TODO: Remove selector
  const { user } = useAuth();

  // TODO: Remove auth id
  const dispatchFetchEntries = useCustomCompareCallback(
    () => {
      console.log('zzzxczxc', query);
      dispatch(fetchEntries({ id: user.id, query }));
    },
    [dispatch, user, query],
    (prevDeps, nextDeps) => isEqual(prevDeps, nextDeps)
  );

  const dispatchFetchEntry = (id: string) => {
    console.log('here');
    dispatch(fetchEntry(id));
  };

  const dispatchAddEntry = (entry: Entry) => {
    dispatch(addEntry(entry));
  };

  const dispatchEditEntry = (entry: Entry, id: string) => {
    dispatch(editEntry(entry, id));
  };

  const dispatchResetEntry = () => {
    dispatch(resetEntry());
  };

  const dispatchApproveEntry = (id) => {
    dispatch(editEntryStatus(id, 'approved', query));
  };

  const dispatchRejectEntry = (id) => {
    dispatch(editEntryStatus(id, 'rejected'));
  };

  return (
    <EntryContext.Provider
      value={{
        query,
        setQuery,
        dispatchAddEntry,
        dispatchEditEntry,
        dispatchFetchEntries,
        dispatchFetchEntry,
        dispatchApproveEntry,
        dispatchRejectEntry,
        dispatchResetEntry,
      }}>
      {children}
    </EntryContext.Provider>
  );
};

const useEntry = (): ContextType => {
  const context = useContext(EntryContext);
  if (context === undefined) {
    throw new Error('useEntry must be used within a EntryContextProvider');
  }
  return context;
};

export { EntryProvider, useEntry };
