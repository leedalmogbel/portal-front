/**
 * @flow
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';
import { useSelector } from 'react-redux';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Error from 'components/common/Error';
import type { State } from 'reducers/types';
import type { UserDetailState } from 'reducers/users/detail';

import { useUserDetail } from '../hook';

const Container = tw.div`flex gap-10 bg-gray-50 p-4 mb-4`,
  FormGroup = tw.div`w-3/12`,
  Text = tw.span`uppercase`,
  Document = tw.a`font-medium text-red-600`;

const DocumentsVerificationFields = (): Node => {
  const { errors } = useFormState();
  const { data: user } = useSelector<State, UserDetailState>(
    (state) => state.users.detail
  );
  const { sceneIsView, sceneIsAdd } = useUserDetail();
  const readOnly = sceneIsView;

  return (
    <Container>
      <FormGroup>
        <Text>National ID</Text>
      </FormGroup>
      <FormGroup>
        <Label htmlFor="documentExpiry">Document Expiry</Label>
        <Input
          name="documentExpiry"
          id="documentExpiry"
          type="date"
          rules={{ required: true }}
          readOnly={readOnly}
        />
        <Error>{errors.documentExpiry && 'Document Expiry is required'}</Error>
      </FormGroup>
      <FormGroup>
        {!sceneIsView ? (
          <Fragment>
            <Label htmlFor="documentExpiryFile">Select File</Label>
            <Input
              name="documentExpiryFile"
              id="documentExpiryFile"
              type="file"
              rules={{ required: sceneIsAdd ? true : false }}
              readOnly={readOnly}
            />
            <Error>
              {errors.documentExpiryFile && 'Document File is required'}
            </Error>
          </Fragment>
        ) : (
          user?.documents && (
            <Fragment>
              <Label htmlFor="documentExpiryFile">Document File</Label>
              <Document href={user.documents.filePath} target="_blank">
                document
              </Document>
            </Fragment>
          )
        )}
      </FormGroup>
    </Container>
  );
};

export default DocumentsVerificationFields;
