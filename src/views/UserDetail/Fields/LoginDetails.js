/**
 * @flow
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState } from 'react-hook-form';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Error from 'components/common/Error';
import Select from 'components/common/Select';
import { useRoles } from 'hooks/roles';

import { useUserDetail } from '../hook';

const FormGroup = tw.div`mb-4`;

const LoginDetailsFields = (): Node => {
  const { errors } = useFormState();
  const { roleOptions } = useRoles();
  const { sceneIsView, sceneIsEdit, sceneIsAdd, auth } = useUserDetail();
  const readOnly = sceneIsView;

  return (
    <Fragment>
      <FormGroup>
        <Label htmlFor="username">Username</Label>
        <Input
          name="username"
          id="username"
          type="text"
          rules={{ required: true }}
          readOnly={readOnly}
        />
        <Error>{errors.username && 'Username is required'}</Error>
      </FormGroup>
      <FormGroup>
        <Label htmlFor="password">
          {sceneIsEdit ? 'Old Password' : 'Password'}
        </Label>
        <Input
          name="password"
          id="password"
          type="password"
          rules={{ required: sceneIsAdd ? true : false }}
          readOnly={readOnly}
        />
        <Error>{errors.password && 'Password is required'}</Error>
      </FormGroup>
      {/* TODO: Add Password validation (EDIT MODE) */}
      {!sceneIsView && (
        <FormGroup>
          <Label htmlFor="confirmPassword">
            {sceneIsEdit ? 'New Password' : 'Confirm Password'}
          </Label>
          <Input
            name="confirmPassword"
            id="confirmPassword"
            type="password"
            readOnly={readOnly}
          />
          <Error>{errors.confirmPassword && 'Passwords do not match'}</Error>
        </FormGroup>
      )}
      {auth.userType === 'Admin' && (
        <FormGroup>
          <Label htmlFor="role">Role</Label>
          <Select
            name="role"
            id="role"
            options={roleOptions}
            readOnly={readOnly}
          />
        </FormGroup>
      )}
    </Fragment>
  );
};

export default LoginDetailsFields;
