/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

import { useUser } from 'views/Users/hook';
import type { State } from 'reducers/types';
import type { UserDetailState } from 'reducers/users/detail';

import Form from './Form';
import { mapUserToFormData } from './utils';

const Container = tw.div`p-8 bg-white`;

const UsersDetail = (): Node => {
  const { id } = useParams();
  const navigate = useNavigate();
  const {
    data: user,
    status: { fetching, fetched, added, edited },
  } = useSelector<State, UserDetailState>((state) => state.users.detail);
  const { dispatchFetchUser, dispatchResetUser } = useUser();

  useEffect(() => {
    return () => {
      dispatchResetUser();
    };
  }, [dispatchResetUser]);

  useEffect(() => {
    if (id) {
      dispatchFetchUser(id);
    }
  }, [id, dispatchFetchUser]);

  useEffect(() => {
    if (added) {
      toast.success('Account succssfully registered!');
      navigate('/users');
    }
  }, [added, navigate]);

  useEffect(() => {
    if (edited) {
      // TODO: Hide toast if nothing was changed
      toast.success('Account succssfully updated!');
      navigate('/users');
    }
  }, [edited, navigate]);

  return (
    <Container>
      {fetching && <div>Loading...</div>}
      {fetched && <Form defaultValues={mapUserToFormData(user)} />}
      {!fetching && !fetched && <Form />}
    </Container>
  );
};

export default UsersDetail;
