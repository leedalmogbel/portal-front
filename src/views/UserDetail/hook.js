/**
 * @flow
 */

import { useState, useContext, createContext } from 'react';
import type { Node } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import type { Scene } from 'types';
import type { AuthState } from 'reducers/auth';
import type { State } from 'reducers/types';

type ContextType = {
  sceneIsAdd: boolean,
  sceneIsEdit: boolean,
  sceneIsView: boolean,
  changeSceneToAdd: Function,
  changeSceneToEdit: Function,
  changeSceneToView: Function,
};

const initialValues = {
  scene: 'view',
  sceneIsAdd: false,
  sceneIsEdit: false,
  sceneIsView: true,
  changeSceneToAdd: () => {},
  changeSceneToEdit: () => {},
  changeSceneToView: () => {},
};

const UserContext = createContext<ContextType>(initialValues);

type UserDetailProviderProps = {
  children: ?Node,
};

const UserDetailProvider = ({ children }: UserDetailProviderProps): Node => {
  const [scene, setScene] = useState<Scene>(initialValues.scene);
  const sceneIsAdd = scene === 'add',
    sceneIsEdit = scene === 'edit',
    sceneIsView = scene === 'view';

  const { data: auth } = useSelector<State, AuthState>((state) => state.auth);

  const changeSceneToAdd = () => {
    setScene('add');
  };

  const changeSceneToEdit = () => {
    setScene('edit');
  };

  const changeSceneToView = () => {
    setScene('view');
  };

  return (
    <UserContext.Provider
      value={{
        auth,
        sceneIsAdd,
        sceneIsEdit,
        sceneIsView,
        changeSceneToAdd,
        changeSceneToEdit,
        changeSceneToView,
      }}>
      {children}
    </UserContext.Provider>
  );
};

const useUserDetail = (): ContextType => {
  const context = useContext(UserContext);
  if (context === undefined) {
    throw new Error('useUserDetail must be used within a UserContextProvider');
  }
  return context;
};

export { UserDetailProvider, useUserDetail };
