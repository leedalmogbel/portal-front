/**
 * @flow
 */

import { pick, first } from 'lodash';
import { DateTime } from 'luxon';

import type { User, FormData } from 'types/user';
import { locationOptions, disciplineOptions } from 'utils/constants';

export const mapUserToFormData = (user: User): FormData => {
  const userData = pick(user, [
    'username',
    'email',
    'emiratesId',
    'firstName',
    'lastName',
    'mobile',
  ]);

  const formData = {
    documentExpiry: user.documents
      ? new DateTime(user.documents.documentExpiry).toFormat('yyyy-LL-dd')
      : null,
    dob: new DateTime(user.dob).toFormat('yyyy-LL-dd'),
    location:
      locationOptions.find((option) => option.value === user.discipline) ||
      first(locationOptions),
    discipline:
      disciplineOptions.find((option) => option.value === user.discipline) ||
      first(disciplineOptions),
    ...userData,
  };

  return formData;
};
