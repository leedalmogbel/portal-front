/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { first, isEmpty } from 'lodash';
import { useNavigate } from 'react-router-dom';

import { useUser } from 'views/Users/hook';
import type { FormData } from 'types/user';
import type { Option } from 'types';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import { locationOptions, disciplineOptions } from 'utils/constants';
import { useRoles } from 'hooks/roles';
import type { State } from 'reducers/types';
import type { UserDetailState } from 'reducers/users/detail';

import PersonalDetailsFields from './Fields/PersonalDetails';
import LoginDetailsFields from './Fields/LoginDetails';
import DocumentsVerificationFields from './Fields/DocumentsVerification';
import { useUserDetail } from './hook';

const InnerContainer = tw.div`lg:(flex gap-10 )`,
  PersonalDetails = tw.div`lg:(w-2/3)`,
  LoginDetails = tw.div`lg:(w-1/3)`,
  Title = tw(BaseTitle)`mb-4 text-red-600`,
  Buttons = tw.div`flex gap-2`;

type FormProps = {
  defaultValues?: FormData,
};

const Form = ({ defaultValues }: FormProps): Node => {
  const { dispatchAddUser, dispatchEditUser } = useUser();
  const { data: user } = useSelector<State, UserDetailState>(
    (state) => state.users.detail
  );
  const { roleOptions } = useRoles();
  const navigate = useNavigate();
  const { sceneIsAdd, sceneIsEdit, sceneIsView, changeSceneToView } =
    useUserDetail();

  const methods = useForm({
    defaultValues,
  });

  const onSubmit = (data) => {
    if (sceneIsAdd) {
      dispatchAddUser(data);
    } else {
      dispatchEditUser(data, user.id);
    }
  };

  const onCancel = () => {
    if (sceneIsAdd) {
      navigate('/users');
    } else if (sceneIsEdit) {
      changeSceneToView();
    }
  };

  useEffect(() => {
    if (
      (sceneIsView || sceneIsEdit) &&
      !isEmpty(user) &&
      !isEmpty(roleOptions)
    ) {
      methods.setValue(
        'role',
        roleOptions.find(({ value }) => parseInt(value) === user.roleId)
      );
    }
  }, [sceneIsEdit, sceneIsView, user, methods, roleOptions]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(roleOptions)) {
      methods.setValue('role', first(roleOptions));
    }
  }, [methods, roleOptions, sceneIsAdd]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <InnerContainer>
          <PersonalDetails>
            <Title type="h4">Personal Details</Title>
            <PersonalDetailsFields />
          </PersonalDetails>
          <LoginDetails>
            <Title type="h4">Login Details</Title>
            <LoginDetailsFields />
          </LoginDetails>
        </InnerContainer>
        <Title type="h4">Documents Verification</Title>
        <DocumentsVerificationFields />
        <Buttons>
          {!sceneIsView && (
            <Button type="submit" size="xs">
              {sceneIsAdd ? 'Create' : 'Update'}
            </Button>
          )}
          {(sceneIsAdd || sceneIsEdit) && (
            <Button type="button" size="xs" onClick={onCancel}>
              Cancel
            </Button>
          )}
        </Buttons>
      </form>
    </FormProvider>
  );
};

Form.defaultProps = {
  defaultValues: {
    firstName: '',
    lastName: '',
    dob: null,
    username: '',
    mobile: '',
    emiratesId: '',
    location: (first(locationOptions): Option),
    discipline: (first(disciplineOptions): Option),
    email: '',
    password: '',
    confirmPassword: '',
    documentExpiry: null,
    documentExpiryFile: null,
    role: null,
  },
};

export default Form;
