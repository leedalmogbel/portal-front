/**
 * @flow
 */

import { useState, useContext, createContext } from 'react';
import type { Node } from 'react';

import type { Scene } from 'types';

type ContextType = {
  sceneIsAdd: boolean,
  sceneIsEdit: boolean,
  sceneIsView: boolean,
  changeSceneToAdd: Function,
  changeSceneToEdit: Function,
  changeSceneToView: Function,
};

const initialValues = {
  scene: 'view',
  sceneIsAdd: false,
  sceneIsEdit: false,
  sceneIsView: true,
  changeSceneToAdd: () => {},
  changeSceneToEdit: () => {},
  changeSceneToView: () => {},
};

const EntryContext = createContext<ContextType>(initialValues);

type EntryDetailProviderProps = {
  children: ?Node,
};

const EntryDetailProvider = ({ children }: EntryDetailProviderProps): Node => {
  const [scene, setScene] = useState<Scene>(initialValues.scene);
  const sceneIsAdd = scene === 'add',
    sceneIsEdit = scene === 'edit',
    sceneIsView = scene === 'view';

  const changeSceneToAdd = () => {
    setScene('add');
  };

  const changeSceneToEdit = () => {
    setScene('edit');
  };

  const changeSceneToView = () => {
    setScene('view');
  };

  return (
    <EntryContext.Provider
      value={{
        sceneIsAdd,
        sceneIsEdit,
        sceneIsView,
        changeSceneToAdd,
        changeSceneToEdit,
        changeSceneToView,
      }}>
      {children}
    </EntryContext.Provider>
  );
};

const useEntryDetail = (): ContextType => {
  const context = useContext(EntryContext);
  if (context === undefined) {
    throw new Error(
      'useEntryDetail must be used within a EntryContextProvider'
    );
  }
  return context;
};

export { EntryDetailProvider, useEntryDetail };
