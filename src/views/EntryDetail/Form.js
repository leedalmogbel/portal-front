/**
 * @flow
 */

import { useEffect, useState, Fragment } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useForm, FormProvider } from 'react-hook-form';
import { useSelector } from 'react-redux';
import { first, isEmpty } from 'lodash';
import { useNavigate, useSearchParams } from 'react-router-dom';

import Select from 'components/common/Select';
import { useAuth } from 'hooks/auth';
import { useUser } from 'views/Users/hook';
import { useEntry } from 'views/Entries/hook';
import type { FormData } from 'types/entry';
import type { Option } from 'types';
import BaseTitle from 'components/common/Title';
import Button from 'components/common/Button';
import type { State } from 'reducers/types';
import type { EntryDetailState } from 'reducers/entries/detail';
import type { RaceDetailState } from 'reducers/races/detail';
import type { AuthState } from 'reducers/auth';
import { useRace } from 'views/Races/hook';
import { useHorse } from 'views/Horses/hook';
import { useRider } from 'views/Riders/hook';

import RegistrationDetailsFields from './Fields/RegistrationDetails';
import EntryDetailFields from './Fields/EntryInformation';
import { useEntryDetail } from './hook';

const Title = tw(BaseTitle)`mb-4 text-red-600`,
  Buttons = tw.div`flex gap-2`;

type FormProps = {
  defaultValues?: FormData,
};

const Form = ({ defaultValues }: FormProps): Node => {
  const [searchParams] = useSearchParams();
  const { dispatchAddEntry, dispatchEditEntry } = useEntry();
  const { user } = useAuth();
  const { raceOptions, getPledgeStatus } = useRace();
  const { horseOptions, dispatchFetchHorses } = useHorse();
  const { riderOptions, dispatchFetchRiders } = useRider();
  const { userOptions } = useUser();

  const [acceptPledge] = useState(false);
  const { data: entry } = useSelector<State, EntryDetailState>(
    (state) => state.entries.detail
  );
  const { data: race } = useSelector<State, RaceDetailState>(
    (state) => state.races.detail
  );
  const { isAdmin } = useAuth();
  const navigate = useNavigate();
  const { sceneIsAdd, sceneIsEdit, sceneIsView, changeSceneToView } =
    useEntryDetail();

  const methods = useForm({
    defaultValues,
  });

  const onSubmit = (data) => {
    if (sceneIsAdd) {
      dispatchAddEntry(data);
    } else {
      dispatchEditEntry(data, entry.id);
    }
  };

  const onCancel = () => {
    if (sceneIsAdd) {
      navigate('/entries');
    } else if (sceneIsEdit) {
      changeSceneToView();
    }
  };

  const onSelectUser = ({ value }) => {
    dispatchFetchHorses(value);
    dispatchFetchRiders(value);
  };

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(user)) {
      methods.setValue('userId', user.id);
    }
  }, [user, methods, sceneIsAdd]);

  useEffect(() => {
    if ((sceneIsView || sceneIsEdit) && !isEmpty(user)) {
      methods.setValue('userId', user.id);
    }
  }, [user, methods, sceneIsView, sceneIsEdit]);

  useEffect(() => {
    if (searchParams.get('raceId') !== null) return;
    if (sceneIsAdd && !isEmpty(raceOptions)) {
      methods.setValue('race', first(raceOptions));
    }
  }, [raceOptions, methods, sceneIsAdd, searchParams]);

  useEffect(() => {
    if (searchParams.get('raceId') && race) {
      methods.setValue('race', {
        name: race.name,
        value: race.id,
        event: race.eventId,
        content: race,
      });
    }
  }, [searchParams, race, methods]);

  useEffect(() => {
    if (sceneIsEdit || sceneIsView) {
      methods.setValue('entryDetail', { content: entry });
    }
  }, [sceneIsEdit, sceneIsView, entry, methods]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(horseOptions)) {
      methods.setValue('horse', first(horseOptions));
    }
  }, [horseOptions, methods, sceneIsAdd]);

  useEffect(() => {
    if (sceneIsAdd && !isEmpty(riderOptions)) {
      methods.setValue('rider', first(riderOptions));
    }
  }, [riderOptions, methods, sceneIsAdd]);

  useEffect(() => {
    if (
      (sceneIsView || sceneIsEdit) &&
      !isEmpty(entry) &&
      !isEmpty(riderOptions)
    ) {
      methods.setValue(
        'rider',
        riderOptions.find(({ value }) => parseInt(value) === entry.riderId)
      );
    }
  }, [sceneIsEdit, sceneIsView, entry, methods, riderOptions]);

  useEffect(() => {
    if (
      (sceneIsView || sceneIsEdit) &&
      !isEmpty(entry) &&
      !isEmpty(raceOptions)
    ) {
      methods.setValue(
        'race',
        raceOptions.find(({ value }) => parseInt(value) === entry.raceId)
      );
    }
  }, [sceneIsEdit, sceneIsView, entry, methods, raceOptions]);

  useEffect(() => {
    if (
      (sceneIsView || sceneIsEdit) &&
      !isEmpty(entry) &&
      !isEmpty(horseOptions)
    ) {
      methods.setValue(
        'horse',
        horseOptions.find(({ value }) => parseInt(value) === entry.horseId)
      );
    }
  }, [sceneIsEdit, sceneIsView, entry, methods, horseOptions]);
  // useEffect(() => {
  // make sure na yung structure ng events.races[] === races.all[]
  // setRaceOptions(mapRacesToOptions(selectedEvents.value.races))
  // }, [selectedEvent]);

  return (
    <FormProvider {...methods}>
      <form onSubmit={methods.handleSubmit(onSubmit)}>
        <Title type="h4">Entry Details</Title>
        <RegistrationDetailsFields />
        {isAdmin && userOptions.length > 0 && (
          <Fragment>
            <Title type="h4">User Details</Title>
            <Select
              name="userId"
              id="userId"
              options={userOptions}
              readOnly={(methods.watch('entries') ?? []).length > 0}
              onChange={onSelectUser}
            />
          </Fragment>
        )}
        <Title type="h4">Athletes Details</Title>
        <EntryDetailFields />
        <Buttons>
          {!sceneIsView && (
            <Button type="submit" size="xs">
              {sceneIsAdd ? 'Create' : 'Update'}
            </Button>
          )}
          {(sceneIsAdd || sceneIsEdit) && (
            <Button type="button" size="xs" onClick={onCancel}>
              Cancel
            </Button>
          )}
        </Buttons>
      </form>
    </FormProvider>
  );
};

Form.defaultProps = {
  defaultValues: {
    name: '',
    race: null,
    rider: null,
    horse: null,
    pledge: false,
  },
};

export default Form;
