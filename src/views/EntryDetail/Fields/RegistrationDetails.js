/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment, useEffect, useState } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useFormState, useFormContext } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { parseISO, format } from 'date-fns';
import { FaFlag } from 'react-icons/fa';

import Label from 'components/common/Label';
import Input from 'components/common/Input';
import Checkbox from 'components/common/Checkbox';
import Select from 'components/common/Select';
import Error from 'components/common/Error';
import { useRace } from 'views/Races/hook';
import { useHorse } from 'views/Horses/hook';
import { useRider } from 'views/Riders/hook';
import {
  eventLocationOptions,
  locationOptions,
  visaCategoryOptions,
} from 'utils/constants';
import Modal from 'components/Modal';

import { useEntryDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`,
  OneThirdColumnContainer = tw.div`sm:(flex gap-10 w-[calc((100% - 80px) / 3)])`;

const RegistrationDetailsFields = (): Node => {
  const { errors } = useFormState();
  const { watch } = useFormContext();
  const navigate = useNavigate();
  const { sceneIsView, sceneIsEdit, sceneIsAdd } = useEntryDetail();
  const { raceOptions, setQuery, getPledgeStatus } = useRace();
  const { horseOptions } = useHorse();
  const { riderOptions } = useRider();
  const readOnly = sceneIsView;

  // TODO: clean up
  const [isOpen, setIsOpen] = useState(true);

  const race = watch('race');
  const detail = watch('entryDetail');

  console.log('detail', detail);
  let raceDate,
    openDate,
    closeDate = '';
  raceDate = race?.content?.eventDate;
  closeDate = race?.content?.closingDate;
  openDate = race?.content?.openingDate;

  if ((sceneIsView || sceneIsEdit) && detail) {
    raceDate = detail?.content?.races?.eventDate;
    closeDate = detail?.content?.races?.closingDate;
    openDate = detail?.content?.races?.openingDate;
  }

  raceDate = parseISO(raceDate);
  closeDate = parseISO(closeDate);
  openDate = parseISO(openDate);

  console.log(race?.content);
  console.log('sceneIsView', sceneIsView);
  console.log('sceneIsEdit', sceneIsEdit);
  console.log('getPledgeStatus', race?.content?.pledge);
  return (
    <Fragment>
      {sceneIsAdd && race?.content?.pledge && (
        <Modal
          title={
            race?.content?.notice?.title ||
            detail?.content?.races?.notice?.title
          }
          body={
            race?.content?.notice?.body || detail?.content?.races?.notice?.body
          }
          isOpen={isOpen}
          closeModal={() => setIsOpen(false)}
          reject={() => navigate(`/races`)}
        />
      )}

      {/* START: RACE INFORMATION */}
      <div tw="bg-gray-400 mb-3 p-3 rounded-md border border-transparent">
        <FlexContainer>
          <FormGroup tw="flex-1 inline-flex gap-4 text-5xl">
            <FaFlag />
            <p tw="font-bold">
              {race?.content?.name || detail?.content?.races?.name}
            </p>
          </FormGroup>
        </FlexContainer>
        <FlexContainer>
          <FormGroup tw="flex-1">
            <Label>Event Date</Label>
            <p>{race && format(raceDate, 'dd-MMMM-yyyy')}</p>
          </FormGroup>
          <FormGroup tw="flex-1">
            <Label>Opening Date</Label>
            <p>{race && format(openDate, 'dd-MMMM-yyyy')}</p>
          </FormGroup>
          <FormGroup tw="flex-1">
            <Label>Closing Date</Label>
            <p>{race && format(closeDate, 'dd-MMMM-yyyy')}</p>
          </FormGroup>
        </FlexContainer>
        <FlexContainer>
          <FormGroup tw="flex-1">
            <Label>Location</Label>
            <p>{race?.content?.location || detail?.content?.races?.location}</p>
          </FormGroup>
        </FlexContainer>
      </div>
      <TwoThirdColumnContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="race">Race</Label>
          <Select
            name="race"
            id="race"
            options={raceOptions}
            readOnly={readOnly}
          />
        </FormGroup>
      </TwoThirdColumnContainer>
      {/* <FlexContainer>
        <FormGroup tw="flex-1">
          <Label htmlFor="horse">Horse</Label>
          <Select
            name="horse"
            id="horse"
            options={horseOptions}
            readOnly={readOnly}
          />
        </FormGroup>
        <FormGroup tw="flex-1">
          <Label htmlFor="rider">Rider</Label>
          <Select
            name="rider"
            id="rider"
            options={riderOptions}
            readOnly={readOnly}
          />
        </FormGroup>
      </FlexContainer> */}
      {/* {race && !sceneIsView && !sceneIsEdit && getPledgeStatus(race.value) && (
        <FormGroup>
          <Label tw="flex gap-1" htmlFor="pledge">
            <Checkbox
              name="pledge"
              id="pledge"
              type="checkbox"
              rules={{ required: true }}
              readOnly={readOnly}
            />
            <button
              tw="hover:text-gold-700"
              type="button"
              onClick={() => setIsOpen(true)}>
              Accepting this means you agree with the pledge
            </button>
          </Label>
          <Error>{errors.pledge && 'Agree with the plege is required'}</Error>
        </FormGroup>
      )} */}
    </Fragment>
  );
};

export default RegistrationDetailsFields;

/*
  component (Races)
   - hooks (fetch/get records/dispatch actions), - stateless components, as much as possible all functions, effects, and states are located in hooks\
    - every api call are inside redux actions / required to dispatch actions
    - getter dispatch actions (redux) - dispatchFetchRaces
      - populate redux (state.races.all)
    - const { data, status: { fetched, fetching } } = useSelector(state.races.all) (expose)
    - state - [races, setRaces]
      - useEffect(() => if(fetched) setRaces(transformToOptions(data)) )
*/
