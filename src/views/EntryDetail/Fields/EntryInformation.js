/**
 * @flow
 * @jsxImportSource @emotion/react
 */
import { first, isEmpty } from 'lodash';
import { Fragment, useState, useCallback } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import {
  useFormState,
  useForm,
  useFieldArray,
  useFormContext,
} from 'react-hook-form';

import { useHorse } from 'views/Horses/hook';
import { useRider } from 'views/Riders/hook';
import Label from 'components/common/Label';
import Select from 'components/common/Select';
import Button from 'components/common/Button';

import { useEntryDetail } from '../hook';

const FormGroup = tw.div`mb-4`,
  FlexContainer = tw.div`lg:(flex gap-10)`,
  TwoThirdColumnContainer = tw.div`lg:(flex gap-10 w-[calc(((100% - 20px) / 3) * 2)])`,
  OneThirdColumnContainer = tw.div`sm:(flex gap-10 w-[calc((100% - 80px) / 3)])`;

const EntryDetailFields = (): Node => {
  const { sceneIsView, sceneIsEdit, sceneIsAdd } = useEntryDetail();
  const readOnly = sceneIsView;
  const { horseOptions } = useHorse();
  const { riderOptions } = useRider();

  const { fields, append, remove } = useFieldArray({
    name: 'entries',
  });

  const addEntryDefault = useCallback(() => {
    append({
      horse: first(horseOptions),
      rider: first(riderOptions),
    });
  }, [append, horseOptions, riderOptions]);

  return (
    <Fragment>
      {fields.map((entryItem, index) => (
        <Fragment key={index}>
          <FlexContainer>
            <FormGroup tw="flex-1">
              <Label htmlFor="horse">Horse</Label>
              <Select
                name={`entries.${index}.horse`}
                id={`entries.${index}.horse`}
                options={horseOptions}
                readOnly={readOnly}
              />
            </FormGroup>
            <FormGroup tw="flex-1">
              <Label htmlFor="rider">Rider</Label>
              <Select
                name={`entries.${index}.rider`}
                id={`entries.${index}.rider`}
                options={riderOptions}
                readOnly={readOnly}
              />
            </FormGroup>
          </FlexContainer>
        </Fragment>
      ))}

      {!sceneIsView && (
        <FlexContainer tw="mb-4">
          <FormGroup tw="flex items-center mb-0">
            <Button tw="" type="button" size="xs" onClick={addEntryDefault}>
              Add Entry
            </Button>
          </FormGroup>
        </FlexContainer>
      )}
    </Fragment>
  );
};

export default EntryDetailFields;
