/**
 * @flow
 */

import { useEffect } from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { isEmpty } from 'lodash';

import { useUser } from 'views/Users/hook';
import { useRace } from 'views/Races/hook';
import { useHorse } from 'views/Horses/hook';
import { useRider } from 'views/Riders/hook';
import { useEntry } from 'views/Entries/hook';
import { useAuth } from 'hooks/auth';
import type { State } from 'reducers/types';
import type { EntryDetailState } from 'reducers/entries/detail';

import Form from './Form';
import { mapEntryToFormData } from './utils';

const Container = tw.div`p-8 bg-white`;

const EntryDetail = (): Node => {
  const { id } = useParams();
  const navigate = useNavigate();
  const { raceOptions, dispatchFetchRaces } = useRace();
  const { dispatchFetchHorses } = useHorse();
  const { dispatchFetchRiders } = useRider();
  const { dispatchFetchUsers } = useUser();
  const {
    data: entryDetail,
    status: { fetching, fetched, added, edited },
  } = useSelector<State, EntryDetailState>((state) => state.entries.detail);
  const { dispatchFetchEntry, dispatchResetEntry } = useEntry();
  const { isAdmin } = useAuth();

  useEffect(() => {
    return () => {
      dispatchResetEntry();
    };
  }, [dispatchResetEntry]);

  useEffect(() => {
    if (id) {
      dispatchFetchEntry(id);
    }
  }, [id, dispatchFetchEntry]);

  useEffect(() => {
    if (added) {
      toast.success('Entry succssfully registered!');
      navigate('/entries');
    }
  }, [added, navigate, dispatchResetEntry]);

  useEffect(() => {
    if (edited) {
      // TODO: Hide toast if nothing was changed
      toast.success('Entry succssfully updated!');
      navigate('/entries');
    }
  }, [edited, navigate, dispatchResetEntry]);

  useEffect(() => {
    // setQuery('fetchraces');
    dispatchFetchRaces();
    console.log('isAdmin', isAdmin);
    if (!isAdmin) {
      dispatchFetchHorses();
      dispatchFetchRiders();
    } else {
      console.log('fetch users');
      dispatchFetchUsers();
    }
  }, [
    dispatchFetchRaces,
    dispatchFetchHorses,
    dispatchFetchRiders,
    dispatchFetchUsers,
    isAdmin,
  ]);

  return (
    <Container>
      {fetching && <div>Loading...</div>}
      {fetched && !isEmpty(raceOptions) && (
        // !isEmpty(horseOptions) &&
        // !isEmpty(riderOptions) &&
        <Form defaultValues={mapEntryToFormData(entryDetail)} />
      )}
      {!fetching && !fetched && !isEmpty(raceOptions) && (
        // !isEmpty(horseOptions) &&
        // !isEmpty(riderOptions) &&
        <Form />
      )}
    </Container>
  );
};

export default EntryDetail;

/**
 * Entry - List of Entries
 * Add Entry
 *  - Fetch Events
 *    - Select Event
 *  - Fetch Races (associated with selected Event)
 */
