/**
 * @flow
 */

import { pick, first } from 'lodash';
import { DateTime } from 'luxon';
import { parseISO, format } from 'date-fns';

import type { Entry, FormData } from 'types/entry';

export const mapEntryToFormData = (entry: Entry): FormData => {
  const entryData = pick(entry, [
    'name',
    'contactPerson',
    'contactNumber',
    'description',
  ]);

  console.log('entry', entry);
  const formData = {
    ...entryData,
  };

  return formData;
};
