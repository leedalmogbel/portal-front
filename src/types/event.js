/**
 * @flow
 */

import type { Option, Document, Image } from 'types';

export type Event = {|
  id: string,
  name: string,
  country: string,
  countryCode: string,
  location: string,
  description: string,
  startDate: Date,
  endDate: Date,
  active: boolean,
  seasonId: number,
  status: string,
  seasonId: string,
  createdAt: Date,
  updatedAt: Date,
|};

export type FormData = {|
  name: string,
  country: string,
  countryCode: string,
  location: string,
  description: string,
  startDate: ?Date,
  endDate: ?Date,
  // userId: string,
|};
