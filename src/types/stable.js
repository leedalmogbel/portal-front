/**
 * @flow
 */

import type { Option, Document, Image } from 'types';

export type Stable = {|
  id: string,
  name: string,
  entryCount: string,
  eievStableId: string,
  stableType: string,
  active: boolean,
  status: string,
  userId: string,
  createdAt: Date,
  updatedAt: Date,
|};

export type FormData = {|
  name: string,
  eievStableId: string,
  entryCount: string,
  stableType: string,
  status: string,
  userId: string,
|};
