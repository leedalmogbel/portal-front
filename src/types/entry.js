/**
 * @flow
 */

import type { Option, Document, Image } from 'types';
// TODO: FIX TYPES, IMPORT RACES, HORSES, EVENTS, RIDERS 7/7
export type Entry = {|
  id: string,
  eventId: string,
  horseId: string,
  riderId: string,
  userId: string,
  remarks: string,
  active: boolean,
  status: string,
  createdAt: Date,
  updatedAt: Date,
|};

export type FormData = {|
  name: string,
  eventId: string,
  horseId: string,
  riderId: string,
  remarks: string,
  userId: string,
  pledge: boolean,
|};
