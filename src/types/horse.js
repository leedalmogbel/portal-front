/**
 * @flow
 */

import type { Option, Document } from 'types';

type HorseDiscipline = 'endurance' | 'dressage' | 'flat' | 'jumping';

type HorseVisa = 'resident' | 'touristVisiting' | 'citizen';

export type Horse = {|
  id: string,
  name: string,
  originalName: string,
  countryBirth: string,
  breed: string,
  breeder: string,
  dob: Date,
  gender: string,
  color: string,
  microchipNum: string,
  uelnNo: string,
  countryResidence: string,
  sire: string,
  dam: string,
  sireOfDam: string,
  feiRegistration: {
    no: string,
    passportNo: string,
    date: Date,
  },
  trainerId: number,
  ownerId: number,
  documents: Document,
  active: boolean,
  status: string,
  remarks: string,
  userId: number,
  createdAt: Date,
  updatedAt: Date,
|};

export type FormData = {|
  name: string,
  originalName: string,
  countryBirth: Option,
  breed: Option,
  breeder: string,
  dob: ?Date,
  gender: string,
  color: Option,
  microchipNum: string,
  uelnNo: string,
  countryResidence: Option,
  sire: string,
  dam: string,
  sireOfDam: string,
  feiPassportNo: string,
  feiPassportExpiryDate: ?Date,
  feiRegistrationNo: string,
  owner: ?Option,
  documentExpiry: ?Date,
  documentExpiryFile: ?File,
  remarks: string,
|};
