/**
 * @flow
 */

export type Option = {|
  name: string,
  value: string,
|};

export type Document = {|
  file: {
    path: string,
    size: number,
    encoding: string,
    filename: string,
    mimetype: string,
    fieldname: string,
    destination: string,
    originalname: string,
  },
  filePath: string,
  documentExpiry: Date,
|};

export type Image = {|
  file: {
    path: string,
    size: number,
    encoding: string,
    filename: string,
    mimetype: string,
    fieldname: string,
    destination: string,
    originalname: string,
  },
  filePath: string,
  documentExpiry: Date,
|};

export type Scene = 'view' | 'add' | 'edit';
