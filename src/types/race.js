/**
 * @flow
 */

import type { Event } from 'types/event';

export type Race = {|
  id: string,
  name: string,
  allowedCount: string,
  contactPerson: string,
  contactNumber: string,
  startTime: Date,
  eventDate: Date,
  events: Event,
  eventTitle: string,
  openingDate: Date,
  closingDate: Date,
  active: boolean,
  status: string,
  eventId: string,
  location: string,
  country: string,
  createdAt: Date,
  updatedAt: Date,
|};

export type FormData = {|
  eventName: string,
  name: string,
  allowedCount: string,
  country: string,
  eventTitle: string,
  location: string,
  description: string,
  startTime: ?Date,
  eventDate: ?Date,
  openingDate: ?Date,
  closingDate: ?Date,
  eventId: string,
|};
