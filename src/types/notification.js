/**
 * @flow
 */

export type Notification = {|
  notifications: Object,
|};
