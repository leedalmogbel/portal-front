/**
 * @flow
 */

export type Dashboard = {|
  users: string,
  owners: string,
  trainers: string,
  riders: string,
  horses: string,
|};
