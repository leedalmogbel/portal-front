/**
 * @flow
 */

import type { Option, Document } from 'types';

type UserDiscipline = 'endurance' | 'dressage' | 'flat' | 'jumping';

type UserStatus = 'pending' | 'rejected' | 'approved';

type UserStableName = 'private_individual_stable';

export type User = {|
  id: string,
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  emiratesId?: string,
  dob: Date,
  mobile: string,
  discipline: UserDiscipline,
  status: UserStatus,
  documents: Document,
  active: boolean,
  eievId?: number,
  eefId?: number,
  feiId?: number,
  stableName?: UserStableName,
  location: string,
  nationality?: string,
  username?: string,
  userType: string,
  accessToken?: string,
  refreshToken?: string,
  createdAt?: Date,
  updatedAt?: Date,
  roleId?: number,
|};

type Permission = {|
  id: number,
  actions: Object,
  active: boolean,
  roleId: number,
  createdAt: Date,
  updatedAt: Date,
|};

export type Role = {|
  id: number,
  name: string,
  active: boolean,
  users: Array<User>,
  permissions: Permission,
  createdAt: Date,
  updatedAt: Date,
|};

export type UserWithPermission = User & {|
  role: Role,
|};

export type FormData = {|
  discipline: Option,
  email: string,
  emiratesId: string,
  firstName: string,
  lastName: string,
  location: Option,
  mobile: string,
  documentExpiry: ?Date,
  documentExpiryFile: ?File,
  dob: ?Date,
  username: string,
  password: string,
  confirmPassword: string,
  role: ?Option,
|};
