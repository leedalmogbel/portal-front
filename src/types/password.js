/**
 * @flow
 */

export type ForgotPassword = {|
  id: string,
  userId: string,
  expiresAt: Date,
  createdAt: Date,
  updatedAt: Date,
|};
