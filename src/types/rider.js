/**
 * @flow
 */

import type { Option, Document, Image } from 'types';

type RiderDiscipline = 'endurance' | 'dressage' | 'flat' | 'jumping';

type RiderVisa = 'resident' | 'touristVisiting' | 'citizen';

export type Rider = {|
  id: string,
  emiratesId: string,
  discipline: RiderDiscipline,
  feiRegistration: {
    no: string,
    date: Date,
  },
  visa: RiderVisa,
  gender: string,
  firstName: string,
  lastName: string,
  dob: Date,
  nationality: string,
  address: {
    uae: {
      address: string,
      city: string,
      country: string,
    },
    home: {
      address: string,
      city: string,
      country: string,
    },
  },
  pobox: string,
  email: string,
  contact: {
    personal: {
      telephone: string,
      mobile: string,
    },
    home: {
      telephone: string,
      mobile: string,
    },
  },
  documents: Document,
  registeredWeight: string,
  riderImage: Image,
  active: boolean,
  status: string,
  remarks: string,
  userId: number,
  createdAt: Date,
  updatedAt: Date,
|};

export type FormData = {|
  emiratesId: string,
  discipline: Option,
  feiRegistrationNo: string,
  feiRegistrationDate: ?Date,
  visa: Option,
  firstName: string,
  lastName: string,
  nationality: string,
  uaeAddress: string,
  uaeCity: Option,
  pobox: string,
  uaeCountry: Option,
  contactEmail: string,
  contactMobile: string,
  contactTelephone: string,
  homeAddress: string,
  homeCity: string,
  homeCountry: Option,
  contactTelHome: string,
  contactMobHome: string,
  documentExpiry: ?Date,
  documentExpiryFile: ?File,
  userId: string,
|};
