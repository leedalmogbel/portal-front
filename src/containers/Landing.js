/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment } from 'react';
import type { Node } from 'react';
import tw, { styled } from 'twin.macro';

import BaseBanner from 'components/Banner';
import Footer from 'components/Footer';

const Container = tw.div`lg:(flex) min-h-[calc(100vh - 48px)] font-cairo`,
  Banner = tw(BaseBanner)`lg:(border-r border-r-gray-300)`,
  Wrapper = styled.div(() => [
    tw`px-10 pt-8 lg:(pt-32)`,
    tw`lg:(flex-1 background-image[url('/assets/images/bg-pattern.png')])`,
  ]);

type LandingProps = {|
  children: ?Node,
|};

const Landing = ({ children }: LandingProps): Node => (
  <Fragment>
    <Container>
      <Banner />
      <Wrapper>{children}</Wrapper>
    </Container>
    <Footer type="full" />
  </Fragment>
);

export default Landing;
