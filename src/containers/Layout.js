/**
 * @flow
 * @jsxImportSource @emotion/react
 */
import useEffect from 'react';
import type { Node } from 'react';
import tw from 'twin.macro';
import { Outlet } from 'react-router-dom';

import Header from 'components/Header';
import Sidebar from 'components/Sidebar';
import Footer from 'components/Footer';

const Container = tw.div`flex min-h-screen font-cairo`,
  Main = tw.main`flex-1 flex flex-col overflow-hidden background-image[url('/assets/images/bg-pattern.png')]`,
  OutletWrapper = tw.div`p-4 lg:(p-8 flex-1)`;

const Layout = (): Node => (
  <Container>
    <Sidebar />
    <Main>
      <Header />
      <OutletWrapper>
        <Outlet />
      </OutletWrapper>
      <Footer type="inner" />
    </Main>
  </Container>
);

export default Layout;
