/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment, Suspense, useEffect } from 'react';
import type { Node } from 'react';
import { GlobalStyles } from 'twin.macro';
import { Navigate, Route, Routes } from 'react-router-dom';
import { isEmpty } from 'lodash';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import Authentication from 'views/Authentication';
import Registration from 'views/Registration';
import Login from 'views/Login';
import ThemedSuspense from 'components/ThemedSuspense';
import Landing from 'containers/Landing';
import { useAuth } from 'hooks/auth';
import { useRoles } from 'hooks/roles';
import { axiosInterceptors } from 'utils/api';
import ForgotPassword from 'views/ForgotPassword';
import ChangePassword from 'views/ChangePassword';

import { routes } from './routes';

const App = (): Node => {
  const { user } = useAuth();
  const { dispatchFetchRoles } = useRoles();

  useEffect(() => {
    axiosInterceptors();
  }, []);

  useEffect(() => {
    dispatchFetchRoles();
  }, [dispatchFetchRoles]);

  return (
    <Fragment>
      <GlobalStyles />
      <Routes>
        <Route path="/" element={<Navigate to="/dashboard" />} />
        <Route path="/authentication" element={<Authentication />} />
        <Route
          path="/registration"
          element={
            <Landing>
              <Registration />
            </Landing>
          }
        />
        <Route
          path="/login"
          element={
            <Landing>
              <Login />
            </Landing>
          }
        />
        <Route
          path="/forgot-password"
          element={
            <Landing>
              <ForgotPassword />
            </Landing>
          }
        />
        <Route
          path="/change-password"
          element={
            <Landing>
              <ChangePassword />
            </Landing>
          }
        />
        <Route element={<Authentication />}>
          {routes
            .filter((route) =>
              isEmpty(route.permittedRoles) ||
              route.permittedRoles?.includes(user?.userType)
                ? true
                : false
            )
            .map(({ path, element }) => (
              <Route
                key={path}
                path={path}
                element={
                  <Suspense fallback={<ThemedSuspense />}>{element}</Suspense>
                }
              />
            ))}
        </Route>
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
      <ToastContainer
        limit={3}
        tw="text-sm"
        draggable={false}
        pauseOnHover={false}
        pauseOnFocusLoss={false}
      />
    </Fragment>
  );
};

export default App;
