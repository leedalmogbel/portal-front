/**
 * @flow
 */

import type { AuthState } from './auth';
import type { RolesState } from './roles';
import type { DashboardState } from './dashboard';
import type { UsersState } from './users/all';
import type { UserDetailState } from './users/detail';
import type { OwnersState } from './owners/all';
import type { OwnerDetailState } from './owners/detail';
import type { TrainersState } from './trainers/all';
import type { TrainerDetailState } from './trainers/detail';
import type { RidersState } from './riders/all';
import type { RiderDetailState } from './riders/detail';

export type State = {
  auth: AuthState,
  users: {
    all: UsersState,
    detail: UserDetailState,
  },
  owners: {
    all: OwnersState,
    detail: OwnerDetailState,
  },
  trainers: {
    all: TrainersState,
    detail: TrainerDetailState,
  },
  riders: {
    all: RidersState,
    detail: RiderDetailState,
  },
  roles: RolesState,
  dashboard: DashboardState,
};
