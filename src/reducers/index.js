import { combineReducers } from 'redux';

import auth from './auth';
import owners from './owners';
import users from './users';
import roles from './roles';
import trainers from './trainers';
import dashboard from './dashboard';
import password from './password';
import riders from './riders';
import horses from './horses';
import events from './events';
import races from './races';
import entries from './entries';
import stables from './stables';
import notification from './notification';

const rootReducer = combineReducers({
  auth,
  users,
  owners,
  roles,
  trainers,
  dashboard,
  password,
  riders,
  horses,
  events,
  races,
  entries,
  stables,
  notification,
});

export default rootReducer;
