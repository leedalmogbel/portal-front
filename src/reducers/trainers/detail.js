/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { Trainer } from 'types/trainer';

export type TrainerDetailState = {|
  data: ?Trainer,
  status: {
    fetching: boolean,
    fetched: boolean,
    adding: boolean,
    added: boolean,
    editing: boolean,
    edited: boolean,
  },
  error: {
    fetch: ?Error,
    edit: ?Error,
    add: ?Error,
  },
|};

const initialState: TrainerDetailState = {
  data: null,
  status: {
    fetching: false,
    fetched: false,
    adding: false,
    added: false,
    editing: false,
    edited: false,
  },
  error: {
    fetch: null,
    edit: null,
    add: null,
  },
};

const TrainerReducer = (
  state: TrainerDetailState = initialState,
  action: Action
): TrainerDetailState => {
  switch (action.type) {
    case 'trainers/fetchTrainer':
      return {
        data: null,
        status: {
          fetching: true,
          fetched: false,
          adding: false,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'trainers/fetchTrainerFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          fetching: false,
          fetched: true,
        },
      };
    case 'trainers/fetchTrainerRejected':
      return {
        data: null,
        status: {
          ...state.status,
          fetching: false,
          fetched: false,
        },
        error: {
          ...state.error,
          fetch: action.error,
        },
      };
    case 'trainers/addTrainer':
      return {
        data: null,
        status: {
          fetching: false,
          fetched: false,
          adding: true,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'trainers/addTrainerFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          adding: false,
          added: true,
        },
      };
    case 'trainers/addTrainerRejected':
      return {
        ...state,
        status: {
          ...state.status,
          adding: false,
        },
        error: {
          ...state.error,
          add: action.error,
        },
      };
    case 'trainers/editTrainer':
    case 'trainers/editTrainerStatus':
      return {
        ...state,
        status: {
          fetching: false,
          fetched: false,
          adding: false,
          added: false,
          editing: true,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'trainers/editTrainerFulfilled':
    case 'trainers/editTrainerStatusFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          editing: false,
          edited: true,
        },
      };
    case 'trainers/editTrainerRejected':
    case 'trainers/editTrainerStatusRejected':
      return {
        ...state,
        status: {
          ...state.status,
          editing: false,
        },
        error: {
          ...state.error,
          edit: action.error,
        },
      };
    case 'trainers/resetTrainer':
      return initialState;
    default:
      return state;
  }
};

export default TrainerReducer;
