/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { Trainer } from 'types/trainer';

export type TrainersState = {|
  data: Array<Trainer>,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: TrainersState = {
  data: [],
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const TrainersReducer = (
  state: TrainersState = initialState,
  action: Action
): TrainersState => {
  switch (action.type) {
    case 'trainers/fetchTrainers':
      return {
        data: [],
        status: {
          fetching: true,
          fetched: false,
        },
        error: {
          fetch: null,
        },
      };
    case 'trainers/fetchTrainersFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    case 'trainers/fetchTrainersRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    case 'trainers/resetFetchTrainers':
      return initialState;
    default:
      return state;
  }
};

export default TrainersReducer;
