/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { Dashboard } from 'types/dashboard';

export type DashboardState = {|
  data: ?Dashboard,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: DashboardState = {
  data: null,
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const DashboardReducer = (
  state: DashboardState = initialState,
  action: Action
): any => {
  switch (action.type) {
    case 'dashboard/fetch':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: true,
        },
      };
    case 'dashboard/fetchFulfilled': {
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    }
    case 'dashboard/fetchRejected': {
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    }
    case 'dashboard/resetFetch':
      return initialState;
    default:
      return state;
  }
};

export default DashboardReducer;
