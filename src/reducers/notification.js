/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { Notification } from 'types/notification';

export type NotificationState = {|
  data: ?Notification,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: NotificationState = {
  data: null,
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const NotificationReducer = (
  state: NotificationState = initialState,
  action: Action
): any => {
  switch (action.type) {
    case 'notification/fetch':
      return {
        data: [],
        status: {
          fetching: true,
          fetched: false,
        },
        error: {
          fetch: null,
        },
      };
    case 'notification/fetchFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    case 'notification/fetchRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    case 'notification/resetFetch':
      return initialState;
    default:
      return state;
  }
};

export default NotificationReducer;
