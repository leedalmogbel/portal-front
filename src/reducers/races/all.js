/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { Race } from 'types/race';

export type RacesState = {|
  data: Array<Race>,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: RacesState = {
  data: [],
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const RacesReducer = (
  state: RacesState = initialState,
  action: Action
): RacesState => {
  switch (action.type) {
    case 'races/fetchRaces':
      return {
        data: [],
        status: {
          fetching: true,
          fetched: false,
        },
        error: {
          fetch: null,
        },
      };
    case 'races/fetchRacesFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    case 'races/fetchRacesRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    case 'races/resetFetchRaces':
      return initialState;
    default:
      return state;
  }
};

export default RacesReducer;
