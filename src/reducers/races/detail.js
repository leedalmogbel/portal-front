/**
 * @flow
 */

import type { Race } from 'types/race';
import type { Action } from 'actions/types';

export type RaceDetailState = {|
  data: ?Race,
  status: {
    fetching: boolean,
    fetched: boolean,
    adding: boolean,
    added: boolean,
    editing: boolean,
    edited: boolean,
  },
  error: {
    fetch: ?Error,
    edit: ?Error,
    add: ?Error,
  },
|};

const initialState: RaceDetailState = {
  data: null,
  status: {
    fetching: false,
    fetched: false,
    adding: false,
    added: false,
    editing: false,
    edited: false,
  },
  error: {
    fetch: null,
    edit: null,
    add: null,
  },
};

const RaceReducer = (
  state: RaceDetailState = initialState,
  action: Action
): RaceDetailState => {
  switch (action.type) {
    case 'races/fetchRace':
      return {
        data: null,
        status: {
          fetching: true,
          fetched: false,
          adding: false,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'races/fetchRaceFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          fetching: false,
          fetched: true,
        },
      };
    case 'races/fetchRaceRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
          fetched: false,
        },
        error: {
          ...state.error,
          fetch: action.error,
        },
      };
    case 'races/addRace':
      return {
        data: null,
        status: {
          fetching: false,
          fetched: false,
          adding: true,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'races/addRaceFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          adding: false,
          added: true,
        },
      };
    case 'races/addRaceRejected':
      return {
        ...state,
        status: {
          ...state.status,
          adding: false,
        },
        error: {
          ...state.error,
          add: action.error,
        },
      };
    case 'races/editRace':
    case 'races/editRaceStatus':
      return {
        ...state,
        status: {
          fetching: false,
          fetched: false,
          adding: false,
          added: false,
          editing: true,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'races/editRaceFulfilled':
    case 'races/editRaceStatusFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          editing: false,
          edited: true,
        },
      };
    case 'races/editRaceRejected':
    case 'races/editRaceStatusRejected':
      return {
        ...state,
        status: {
          ...state.status,
          editing: false,
        },
        error: {
          ...state.error,
          edit: action.error,
        },
      };
    case 'races/resetRace':
      return initialState;
    default:
      return state;
  }
};

export default RaceReducer;
