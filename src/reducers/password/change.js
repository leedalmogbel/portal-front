/**
 * @flow
 */

import type { Action } from 'actions/types';

export type ChangePasswordState = {|
  status: {
    changing: boolean,
    changed: boolean,
  },
  error: {
    change: ?Error,
  },
|};

const initialState: ChangePasswordState = {
  status: {
    changing: false,
    changed: false,
  },
  error: {
    change: null,
  },
};

const ChangePasswordReducer = (
  state: ChangePasswordState = initialState,
  action: Action
): any => {
  switch (action.type) {
    case 'password/changePassword':
      return {
        ...state,
        status: {
          ...state.status,
          changing: true,
        },
      };
    case 'password/changePasswordFulfilled': {
      return {
        ...state,
        status: {
          changing: false,
          changed: true,
        },
      };
    }
    case 'password/changePasswordRejected': {
      return {
        ...state,
        status: {
          ...state.status,
          changing: false,
        },
        error: {
          change: action.error,
        },
      };
    }
    case 'password/resetPassword':
      return initialState;
    default:
      return state;
  }
};

export default ChangePasswordReducer;
