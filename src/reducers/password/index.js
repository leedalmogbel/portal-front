import { combineReducers } from 'redux';

import change from './change';
import forgot from './forgot';

const rootReducer = combineReducers({
  forgot,
  change,
});

export default rootReducer;
