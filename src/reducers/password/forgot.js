/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { ForgotPassword } from 'types/password';

export type ForgotPasswordState = {|
  data: ?ForgotPassword,
  status: {
    resetting: boolean,
    reset: boolean,
  },
  error: {
    reset: ?Error,
  },
|};

const initialState: ForgotPasswordState = {
  data: null,
  status: {
    resetting: false,
    reset: false,
  },
  error: {
    reset: null,
  },
};

const ForgotPasswordReducer = (
  state: ForgotPasswordState = initialState,
  action: Action
): any => {
  switch (action.type) {
    case 'password/forgotPassword':
      return {
        ...state,
        status: {
          ...state.status,
          resetting: true,
        },
      };
    case 'password/forgotPasswordFulfilled': {
      return {
        ...state,
        data: action.payload.data,
        status: {
          resetting: false,
          reset: true,
        },
      };
    }
    case 'password/forgotPasswordRejected': {
      return {
        ...state,
        status: {
          ...state.status,
          resetting: false,
        },
        error: {
          reset: action.error,
        },
      };
    }
    case 'password/resetPassword':
      return initialState;
    default:
      return state;
  }
};

export default ForgotPasswordReducer;
