/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { Role } from 'types/user';

export type RolesState = {|
  data: Array<Role>,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: RolesState = {
  data: [],
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const RolesReducer = (
  state: RolesState = initialState,
  action: Action
): any => {
  switch (action.type) {
    case 'roles/fetchRoles':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: true,
        },
      };
    case 'roles/fetchRolesFulfilled': {
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    }
    case 'roles/fetchRolesRejected': {
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    }
    case 'roles/resetFetchRoles':
      return initialState;
    default:
      return state;
  }
};

export default RolesReducer;
