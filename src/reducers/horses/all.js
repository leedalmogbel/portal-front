/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { Horse } from 'types/horse';

export type HorsesState = {|
  data: Array<Horse>,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: HorsesState = {
  data: [],
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const HorsesReducer = (
  state: HorsesState = initialState,
  action: Action
): HorsesState => {
  switch (action.type) {
    case 'horses/fetchHorses':
      return {
        data: [],
        status: {
          fetching: true,
          fetched: false,
        },
        error: {
          fetch: null,
        },
      };
    case 'horses/fetchHorsesFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    case 'horses/fetchHorsesRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    case 'horses/resetFetchHorses':
      return initialState;
    default:
      return state;
  }
};

export default HorsesReducer;
