/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { Owner } from 'types/owner';

export type OwnersState = {|
  data: Array<Owner>,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: OwnersState = {
  data: [],
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const OwnersReducer = (
  state: OwnersState = initialState,
  action: Action
): OwnersState => {
  switch (action.type) {
    case 'owners/fetchOwners':
      return {
        data: [],
        status: {
          fetching: true,
          fetched: false,
        },
        error: {
          fetch: null,
        },
      };
    case 'owners/fetchOwnersFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    case 'owners/fetchOwnersRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    case 'owners/resetFetchOwners':
      return initialState;
    default:
      return state;
  }
};

export default OwnersReducer;
