/**
 * @flow
 */

import type { Stable } from 'types/stable';
import type { Action } from 'actions/types';

export type StablesState = {|
  data: Array<Stable>,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: StablesState = {
  data: [],
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const StablesReducer = (
  state: StablesState = initialState,
  action: Action
): StablesState => {
  switch (action.type) {
    case 'stables/fetchStables':
      return {
        data: [],
        status: {
          fetching: true,
          fetched: false,
        },
        error: {
          fetch: null,
        },
      };
    case 'stables/fetchStablesFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    case 'stables/fetchStablesRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    case 'stables/resetFetchStables':
      return initialState;
    default:
      return state;
  }
};

export default StablesReducer;
