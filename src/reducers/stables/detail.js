/**
 * @flow
 */

import type { Stable } from 'types/stable';
import type { Action } from 'actions/types';

export type StableDetailState = {|
  data: ?Stable,
  status: {
    fetching: boolean,
    fetched: boolean,
    adding: boolean,
    added: boolean,
    editing: boolean,
    edited: boolean,
  },
  error: {
    fetch: ?Error,
    edit: ?Error,
    add: ?Error,
  },
|};

const initialState: StableDetailState = {
  data: null,
  status: {
    fetching: false,
    fetched: false,
    adding: false,
    added: false,
    editing: false,
    edited: false,
  },
  error: {
    fetch: null,
    edit: null,
    add: null,
  },
};

const StableReducer = (
  state: StableDetailState = initialState,
  action: Action
): StableDetailState => {
  switch (action.type) {
    case 'stables/fetchStable':
      return {
        data: null,
        status: {
          fetching: true,
          fetched: false,
          adding: false,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'stables/fetchStableFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          fetching: false,
          fetched: true,
        },
      };
    case 'stables/fetchStableRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
          fetched: false,
        },
        error: {
          ...state.error,
          fetch: action.error,
        },
      };
    case 'stables/addStable':
      return {
        data: null,
        status: {
          fetching: false,
          fetched: false,
          adding: true,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'stables/addStableFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          adding: false,
          added: true,
        },
      };
    case 'stables/addStableRejected':
      return {
        ...state,
        status: {
          ...state.status,
          adding: false,
        },
        error: {
          ...state.error,
          add: action.error,
        },
      };
    case 'stables/editStable':
    case 'stables/editStableStatus':
      return {
        ...state,
        status: {
          fetching: false,
          fetched: false,
          adding: false,
          added: false,
          editing: true,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'stables/editStableFulfilled':
    case 'stables/editStableStatusFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          editing: false,
          edited: true,
        },
      };
    case 'stables/editStableRejected':
    case 'stables/editStableStatusRejected':
      return {
        ...state,
        status: {
          ...state.status,
          editing: false,
        },
        error: {
          ...state.error,
          edit: action.error,
        },
      };
    case 'stables/resetStable':
      return initialState;
    default:
      return state;
  }
};

export default StableReducer;
