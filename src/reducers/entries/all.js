/**
 * @flow
 */

import type { Entry } from 'types/entry';
import type { Action } from 'actions/types';

export type EntriesState = {|
  data: Array<Entry>,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: EntriesState = {
  data: [],
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const EntryReducer = (
  state: EntriesState = initialState,
  action: Action
): EntriesState => {
  switch (action.type) {
    case 'entries/fetchEntries':
      return {
        data: [],
        status: {
          fetching: true,
          fetched: false,
        },
        error: {
          fetch: null,
        },
      };
    case 'entries/fetchEntriesFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    case 'entries/fetchEntriesRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    case 'entries/resetFetchEntries':
      return initialState;
    default:
      return state;
  }
};

export default EntryReducer;
