/**
 * @flow
 */

import type { Entry } from 'types/entry';
import type { Action } from 'actions/types';

export type EntryDetailState = {|
  data: ?Entry,
  status: {
    fetching: boolean,
    fetched: boolean,
    adding: boolean,
    added: boolean,
    editing: boolean,
    edited: boolean,
  },
  error: {
    fetch: ?Error,
    edit: ?Error,
    add: ?Error,
  },
|};

const initialState: EntryDetailState = {
  data: null,
  status: {
    fetching: false,
    fetched: false,
    adding: false,
    added: false,
    editing: false,
    edited: false,
  },
  error: {
    fetch: null,
    edit: null,
    add: null,
  },
};

const EntryReducer = (
  state: EntryDetailState = initialState,
  action: Action
): EntryDetailState => {
  switch (action.type) {
    case 'entries/fetchEntry':
      return {
        data: null,
        status: {
          fetching: true,
          fetched: false,
          adding: false,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'entries/fetchEntryFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          fetching: false,
          fetched: true,
        },
      };
    case 'entries/fetchEntryRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
          fetched: false,
        },
        error: {
          ...state.error,
          fetch: action.error,
        },
      };
    case 'entries/addEntry':
      return {
        data: null,
        status: {
          fetching: false,
          fetched: false,
          adding: true,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'entries/addEntryFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          adding: false,
          added: true,
        },
      };
    case 'entries/addEntryRejected':
      return {
        ...state,
        status: {
          ...state.status,
          adding: false,
        },
        error: {
          ...state.error,
          add: action.error,
        },
      };
    case 'entries/editEntry':
    case 'entries/editEntryStatus':
      return {
        ...state,
        status: {
          fetching: false,
          fetched: false,
          adding: false,
          added: false,
          editing: true,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'entries/editEntryFulfilled':
    case 'entries/editEntryStatusFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          editing: false,
          edited: true,
        },
      };
    case 'entries/editEntryRejected':
    case 'entries/editEntryStatusRejected':
      return {
        ...state,
        status: {
          ...state.status,
          editing: false,
        },
        error: {
          ...state.error,
          edit: action.error,
        },
      };
    case 'entries/resetEntry':
      return initialState;
    default:
      return state;
  }
};

export default EntryReducer;
