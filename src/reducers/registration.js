/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { User } from 'types/user';

type RegistrationState = {|
  data: ?User,
  status: {
    adding: boolean,
    added: boolean,
  },
  error: {
    add: ?Error,
  },
|};

const initialState: RegistrationState = {
  data: null,
  status: {
    adding: false,
    added: false,
  },
  error: {
    add: null,
  },
};

const RegistrationReducer = (
  state: RegistrationState = initialState,
  action: Action
): RegistrationState => {
  switch (action.type) {
    case 'users/addUser':
      return {
        data: null,
        status: {
          adding: true,
          added: false,
        },
        error: {
          add: null,
        },
      };
    case 'users/addUserFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          adding: false,
          added: true,
        },
      };
    case 'users/addUserRejected':
      return {
        ...state,
        status: {
          ...state.status,
          adding: false,
        },
        error: {
          add: action.error,
        },
      };
    case 'users/resetUser':
      return initialState;
    default:
      return state;
  }
};

export default RegistrationReducer;
