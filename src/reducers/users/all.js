/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { User } from 'types/user';

export type UsersState = {|
  data: Array<User>,
  status: {
    fetching: boolean,
    fetched: boolean,
    adding: boolean,
    added: boolean,
    editing: boolean,
    edited: boolean,
  },
  error: {
    fetch: ?Error,
    edit: ?Error,
    add: ?Error,
  },
|};

const initialState: UsersState = {
  data: [],
  status: {
    fetching: false,
    fetched: false,
    adding: false,
    added: false,
    editing: false,
    edited: false,
  },
  error: {
    fetch: null,
    edit: null,
    add: null,
  },
};

const UsersReducer = (
  state: UsersState = initialState,
  action: Action
): UsersState => {
  switch (action.type) {
    case 'users/fetchUsers':
      return {
        data: [],
        status: {
          fetching: true,
          fetched: false,
          adding: false,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'users/fetchUsersFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          fetching: false,
          fetched: true,
        },
      };
    case 'users/fetchUsersRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          ...state.error,
          fetch: action.error,
        },
      };
    case 'users/resetFetchUsers':
      return initialState;
    default:
      return state;
  }
};

export default UsersReducer;
