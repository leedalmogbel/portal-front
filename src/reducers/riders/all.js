/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { Rider } from 'types/rider';

export type RidersState = {|
  data: Array<Rider>,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: RidersState = {
  data: [],
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const RidersReducer = (
  state: RidersState = initialState,
  action: Action
): RidersState => {
  switch (action.type) {
    case 'riders/fetchRiders':
      return {
        data: [],
        status: {
          fetching: true,
          fetched: false,
        },
        error: {
          fetch: null,
        },
      };
    case 'riders/fetchRidersFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    case 'riders/fetchRidersRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    case 'riders/resetFetchRiders':
      return initialState;
    default:
      return state;
  }
};

export default RidersReducer;
