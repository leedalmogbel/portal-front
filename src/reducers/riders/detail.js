/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { Rider } from 'types/rider';

export type RiderDetailState = {|
  data: ?Rider,
  status: {
    fetching: boolean,
    fetched: boolean,
    adding: boolean,
    added: boolean,
    editing: boolean,
    edited: boolean,
  },
  error: {
    fetch: ?Error,
    edit: ?Error,
    add: ?Error,
  },
|};

const initialState: RiderDetailState = {
  data: null,
  status: {
    fetching: false,
    fetched: false,
    adding: false,
    added: false,
    editing: false,
    edited: false,
  },
  error: {
    fetch: null,
    edit: null,
    add: null,
  },
};

const RiderReducer = (
  state: RiderDetailState = initialState,
  action: Action
): RiderDetailState => {
  switch (action.type) {
    case 'riders/fetchRider':
      return {
        data: null,
        status: {
          fetching: true,
          fetched: false,
          adding: false,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'riders/fetchRiderFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          fetching: false,
          fetched: true,
        },
      };
    case 'riders/fetchRiderRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
          fetched: false,
        },
        error: {
          ...state.error,
          fetch: action.error,
        },
      };
    case 'riders/addRider':
      return {
        data: null,
        status: {
          fetching: false,
          fetched: false,
          adding: true,
          added: false,
          editing: false,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'riders/addRiderFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          adding: false,
          added: true,
        },
      };
    case 'riders/addRiderRejected':
      return {
        ...state,
        status: {
          ...state.status,
          adding: false,
        },
        error: {
          ...state.error,
          add: action.error,
        },
      };
    case 'riders/editRider':
    case 'riders/editRiderStatus':
      return {
        ...state,
        status: {
          fetching: false,
          fetched: false,
          adding: false,
          added: false,
          editing: true,
          edited: false,
        },
        error: {
          fetch: null,
          edit: null,
          add: null,
        },
      };
    case 'riders/editRiderFulfilled':
    case 'riders/editRiderStatusFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          editing: false,
          edited: true,
        },
      };
    case 'riders/editRiderRejected':
    case 'riders/editRiderStatusRejected':
      return {
        ...state,
        status: {
          ...state.status,
          editing: false,
        },
        error: {
          ...state.error,
          edit: action.error,
        },
      };
    case 'riders/resetRider':
      return initialState;
    default:
      return state;
  }
};

export default RiderReducer;
