import { combineReducers } from 'redux';

import all from './all';
import detail from './detail';

const rootReducer = combineReducers({
  all,
  detail,
});

export default rootReducer;
