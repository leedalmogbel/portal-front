/**
 * @flow
 */

import type { Event } from 'types/event';
import type { Action } from 'actions/types';

export type EventsState = {|
  data: Array<Event>,
  status: {
    fetching: boolean,
    fetched: boolean,
  },
  error: {
    fetch: ?Error,
  },
|};

const initialState: EventsState = {
  data: [],
  status: {
    fetching: false,
    fetched: false,
  },
  error: {
    fetch: null,
  },
};

const EventsReducer = (
  state: EventsState = initialState,
  action: Action
): EventsState => {
  switch (action.type) {
    case 'events/fetchEvents':
      return {
        data: [],
        status: {
          fetching: true,
          fetched: false,
        },
        error: {
          fetch: null,
        },
      };
    case 'events/fetchEventsFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          fetching: false,
          fetched: true,
        },
      };
    case 'events/fetchEventsRejected':
      return {
        ...state,
        status: {
          ...state.status,
          fetching: false,
        },
        error: {
          fetch: action.error,
        },
      };
    case 'events/resetFetchEvents':
      return initialState;
    default:
      return state;
  }
};

export default EventsReducer;
