/**
 * @flow
 */

import type { Action } from 'actions/types';
import type { UserWithPermission } from 'types/user';

export type AuthState = {|
  data: ?UserWithPermission,
  status: {
    loggingIn: boolean,
    loggedIn: boolean,
    loggingOut: boolean,
    loggedOut: boolean,
  },
  error: {
    login: ?Error,
    logout: ?Error,
  },
|};

const initialState: AuthState = {
  data: null,
  status: {
    loggingIn: false,
    loggedIn: false,
    loggingOut: false,
    loggedOut: false,
  },
  error: {
    login: null,
    logout: null,
  },
};

const AuthReducer = (state: AuthState = initialState, action: Action): any => {
  switch (action.type) {
    case 'auth/login':
      return {
        data: null,
        status: {
          loggingIn: true,
          loggedIn: false,
          loggingOut: false,
          loggedOut: false,
        },
        error: {
          login: null,
          logout: null,
        },
      };
    case 'auth/loginFulfilled':
      return {
        ...state,
        data: action.payload.data,
        status: {
          ...state.status,
          loggingIn: false,
          loggedIn: true,
        },
      };
    case 'auth/loginRejected':
      return {
        ...state,
        status: {
          ...state.status,
          loggingIn: false,
        },
        error: {
          ...state.error,
          login: action.error,
        },
      };
    case 'auth/logout':
      return {
        data: null,
        status: {
          loggingIn: false,
          loggedIn: false,
          loggingOut: true,
          loggedOut: false,
        },
        error: {
          login: null,
          logout: null,
        },
      };
    case 'auth/logoutFulfilled':
      return {
        ...state,
        status: {
          ...state.status,
          loggingOut: false,
          loggedOut: true,
        },
      };
    case 'auth/logoutRejected':
      return {
        ...state,
        status: {
          ...state.status,
          loggingOut: false,
        },
        error: {
          ...state.error,
          logout: action.error,
        },
      };
    case 'auth/resetLogout':
    case 'auth/resetLogin':
      return initialState;
    default:
      return state;
  }
};

export default AuthReducer;
