/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetPassword = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'password/resetPassword' });
};
