/**
 * @flow
 */

import type { ForgotPassword } from 'types/password';

type PasswordForgotPasswordPayload = {|
  data: ForgotPassword,
  messsage: string,
  status: boolean,
|};

type PasswordChangePasswordPayload = {|
  messsage: string,
  status: boolean,
|};

type PasswordResetAction = {|
  type: 'password/resetPassword',
|};

/**
 * Forgot Password
 */
type PasswordForgotPasswordction = {|
  type: 'password/forgotPassword',
|};

type PasswordForgotPasswordFulfilledAction = {|
  type: 'password/forgotPasswordFulfilled',
  payload: PasswordForgotPasswordPayload,
|};

type PasswordForgotPasswordRejectedAction = {|
  type: 'password/forgotPasswordRejected',
  error: Error,
|};

/**
 * Change Password
 */
type PasswordChangePasswordction = {|
  type: 'password/changePassword',
|};

type PasswordChangePasswordFulfilledAction = {|
  type: 'password/changePasswordFulfilled',
  payload: PasswordChangePasswordPayload,
|};

type PasswordChangePasswordRejectedAction = {|
  type: 'password/changePasswordRejected',
  error: Error,
|};

export type PasswordAction =
  | PasswordForgotPasswordction
  | PasswordForgotPasswordFulfilledAction
  | PasswordForgotPasswordRejectedAction
  | PasswordChangePasswordction
  | PasswordChangePasswordFulfilledAction
  | PasswordChangePasswordRejectedAction
  | PasswordResetAction;
