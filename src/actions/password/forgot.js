/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const forgotPassword =
  (mobile: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'password/forgotPassword' });

    try {
      const res = await eievApi.post('users/forgot-password', mobile);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'password/forgotPasswordFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'password/forgotPasswordRejected',
        error,
      });
    }
  };
