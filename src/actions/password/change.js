/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

type Params = {
  userId: string,
  otp: string,
  password: string,
};

export const changePassword =
  (data: Params): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'password/changePassword' });

    try {
      const res = await eievApi.post('users/change-password', data);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'password/changePasswordFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'password/changePasswordRejected',
        error,
      });
    }
  };
