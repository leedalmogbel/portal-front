/**
 * @flow
 */

import type { Horse } from 'types/horse';

type HorsesPayload = {|
  data: Array<Horse>,
  messsage: string,
  status: boolean,
|};

type HorsePayload = {|
  data: Horse,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH HORSES
 */
type HorsesFetchAction = {|
  type: 'horses/fetchHorses',
|};

type HorsesFetchFulfilledAction = {|
  type: 'horses/fetchHorsesFulfilled',
  payload: HorsesPayload,
|};

type HorsesFetchRejectedAction = {|
  type: 'horses/fetchHorsesRejected',
  error: Error,
|};

type HorsesFetchResetAction = {|
  type: 'horses/resetFetchHorses',
|};

/**
 * FETCH HORSE
 */
type HorsesFetchHorseAction = {|
  type: 'horses/fetchHorse',
|};

type HorsesFetchHorseFulfilledAction = {|
  type: 'horses/fetchHorseFulfilled',
  payload: HorsePayload,
|};

type HorsesFetchHorseRejectedAction = {|
  type: 'horses/fetchHorseRejected',
  error: Error,
|};

type HorsesResetHorseAction = {|
  type: 'horses/resetHorse',
|};

// EDIT HORSE
type HorsesEditHorseAction = {|
  type: 'horses/editHorse',
|};

type HorsesEditHorseFulfilledAction = {|
  type: 'horses/editHorseFulfilled',
  payload: HorsePayload,
|};

type HorsesEditHorseRejectedAction = {|
  type: 'horses/editHorseRejected',
  error: Error,
|};

/**
 * ADD HORSE
 */
type HorsesAddHorseAction = {|
  type: 'horses/addHorse',
|};

type HorsesAddHorseFulfilledAction = {|
  type: 'horses/addHorseFulfilled',
  payload: HorsePayload,
|};

type HorsesAddHorseRejectedAction = {|
  type: 'horses/addHorseRejected',
  error: Error,
|};

// EDIT STATUS
type HorsesEditStatusAction = {|
  type: 'horses/editHorseStatus',
|};

type HorsesEditStatusFulfilledAction = {|
  type: 'horses/editHorseStatusFulfilled',
  payload: HorsePayload,
|};

type HorsesEditStatusRejectedAction = {|
  type: 'horses/editHorseStatusRejected',
  error: Error,
|};

export type HorsesAction =
  | HorsesFetchAction
  | HorsesFetchFulfilledAction
  | HorsesFetchRejectedAction
  | HorsesFetchResetAction
  | HorsesFetchHorseAction
  | HorsesFetchHorseFulfilledAction
  | HorsesFetchHorseRejectedAction
  | HorsesAddHorseAction
  | HorsesAddHorseFulfilledAction
  | HorsesAddHorseRejectedAction
  | HorsesEditHorseAction
  | HorsesEditHorseFulfilledAction
  | HorsesEditHorseRejectedAction
  | HorsesEditStatusAction
  | HorsesEditStatusFulfilledAction
  | HorsesEditStatusRejectedAction
  | HorsesResetHorseAction;
