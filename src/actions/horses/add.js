/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const addHorse =
  (data: any): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'horses/addHorse' });

    const {
      documentExpiryFile,
      owner,
      trainer,
      color,
      countryBirth,
      countryResidence,
      breed,
      ...rest
    } = data;
    console.log('feieef', data);
    const params = {
      ...rest,
      active: true,
      trainerId: trainer.value,
      ownerId: owner.value,
      color: color.value,
      countryBirth: countryBirth.value,
      countryResidence: countryResidence.value,
      breed: breed.value,
      status: 'pending',
    };

    const formData = new FormData();
    formData.append('file', documentExpiryFile[0]);
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.post('horses', formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'horses/addHorseFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'horses/addHorseRejected',
        error,
      });
    }
  };
