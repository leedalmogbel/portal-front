/**
 * @flow
 */
import { pickBy } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

// TODO: Remove id params; Fetch horses by accesstoken
/**
 * @param id: User Id
 */
export const fetchHorses =
  ({ id, query }: { id: string, query?: string }): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'horses/fetchHorses' });
    try {
      const params = pickBy({
        id,
        query,
      });
      const res = await eievApi.get('horses', { params });

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'horses/fetchHorsesFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'horses/fetchHorsesRejected',
        error,
      });
    }
  };

export const fetchHorse =
  (id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'horses/fetchHorse' });

    try {
      const res = await eievApi.get(`horses/${id}`);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      console.log(res.data);
      dispatch({
        type: 'horses/fetchHorseFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'horses/fetchHorseRejected',
        error,
      });
    }
  };
