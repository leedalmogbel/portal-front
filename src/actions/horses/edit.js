/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const editHorse =
  (data: any, id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'horses/editHorse' });

    const { documentExpiryFile, ...rest } = data;

    // TODO: Remove unchanged data
    const params = {
      ...rest,
      discipline: data.discipline.value,
      visa: data.visa.value,
      homeCountry: data.homeCountry.value,
      uaeCountry: data.uaeCountry.value,
      uaeCity: data.uaeCity.value,
    };

    const formData = new FormData();
    formData.append('file', documentExpiryFile[0]);
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.patch(`horses/${id}`, formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'horses/editHorseFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'horses/editHorseRejected',
        error,
      });
    }
  };

export const editHorseStatus =
  (id: string, status: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'horses/editHorseStatus' });

    const params = {
      status,
    };

    try {
      const res = await eievApi.patch(`horses/${id}/status`, params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'horses/editHorseStatusFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'horses/editHorseStatusRejected',
        error,
      });
    }
  };
