/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetHorse = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'horses/resetHorse' });
};
