/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetEvent = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'events/resetEvent' });
};
