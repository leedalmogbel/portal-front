/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const editEvent =
  (data: any, id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'events/editEvent' });

    const { ...rest } = data;

    const params = {
      ...rest,
      location: data.location.value,
      country: data.country.name,
      countryCode: data.country.value,
      organizerId: 1,
      active: true,
      status: 'pending',
    };

    const formData = new FormData();
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.patch(`events/${id}`, formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'events/editEventFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'events/editEventRejected',
        error,
      });
    }
  };

export const editEventStatus =
  (id: string, status: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'events/editEventStatus' });

    const params = {
      status,
    };

    try {
      const res = await eievApi.patch(`events/${id}/status`, params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'events/editEventStatusFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'events/editEventStatusRejected',
        error,
      });
    }
  };
