/**
 * @flow
 */
import { pickBy } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

// TODO: Remove id params; Fetch events by accesstoken
/**
 * @param id: User Id
 */
export const fetchEvents =
  ({ id, query }: { id: string, query?: string }): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'events/fetchEvents' });

    try {
      const params = pickBy({
        id,
        query,
      });
      const res = await eievApi.get('events', { params });

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      console.log(res.data);

      dispatch({
        type: 'events/fetchEventsFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'events/fetchEventsRejected',
        error,
      });
    }
  };

export const fetchEvent =
  (id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'events/fetchEvent' });

    try {
      const res = await eievApi.get(`events/${id}`);
      console.log('res', res);
      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      console.log('lols', res.data);
      dispatch({
        type: 'events/fetchEventFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'events/fetchEventRejected',
        error,
      });
    }
  };
