/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const addEvent =
  (data: any): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'events/addEvent' });
    const { ...rest } = data;

    const params = {
      ...rest,
      location: data.location.value,
      country: data.country.name,
      countryCode: data.country.value,
      organizerId: 1,
      active: true,
      status: 'approved',
    };

    const formData = new FormData();
    // formData.append('file', file[0]);
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.post('events', formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'events/addEventFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'events/addEventRejected',
        error,
      });
    }
  };
