/**
 * @flow
 */

import type { Event } from 'types/event';

type EventsPayload = {|
  data: Array<Event>,
  messsage: string,
  status: boolean,
|};

type EventPayload = {|
  data: Event,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH EVENTS
 */
type EventsFetchAction = {|
  type: 'events/fetchEvents',
|};

type EventsFetchFulfilledAction = {|
  type: 'events/fetchEventsFulfilled',
  payload: EventsPayload,
|};

type EventsFetchRejectedAction = {|
  type: 'events/fetchEventsRejected',
  error: Error,
|};

type EventsFetchResetAction = {|
  type: 'events/resetFetchEvents',
|};

/**
 * FETCH EVENT
 */
type EventsFetchEventAction = {|
  type: 'events/fetchEvent',
|};

type EventsFetchEventFulfilledAction = {|
  type: 'events/fetchEventFulfilled',
  payload: EventPayload,
|};

type EventsFetchEventRejectedAction = {|
  type: 'events/fetchEventRejected',
  error: Error,
|};

type EventsResetEventAction = {|
  type: 'events/resetEvent',
|};

// EDIT EVENT
type EventsEditEventAction = {|
  type: 'events/editEvent',
|};

type EventsEditEventFulfilledAction = {|
  type: 'events/editEventFulfilled',
  payload: EventPayload,
|};

type EventsEditEventRejectedAction = {|
  type: 'events/editEventRejected',
  error: Error,
|};

/**
 * ADD EVENT
 */
type EventsAddEventAction = {|
  type: 'events/addEvent',
|};

type EventsAddEventFulfilledAction = {|
  type: 'events/addEventFulfilled',
  payload: EventPayload,
|};

type EventsAddEventRejectedAction = {|
  type: 'events/addEventRejected',
  error: Error,
|};

// EDIT STATUS
type EventsEditStatusAction = {|
  type: 'events/editEventStatus',
|};

type EventsEditStatusFulfilledAction = {|
  type: 'events/editEventStatusFulfilled',
  payload: EventPayload,
|};

type EventsEditStatusRejectedAction = {|
  type: 'events/editEventStatusRejected',
  error: Error,
|};

export type EventsAction =
  | EventsFetchAction
  | EventsFetchFulfilledAction
  | EventsFetchRejectedAction
  | EventsFetchResetAction
  | EventsFetchEventAction
  | EventsFetchEventFulfilledAction
  | EventsFetchEventRejectedAction
  | EventsAddEventAction
  | EventsAddEventFulfilledAction
  | EventsAddEventRejectedAction
  | EventsEditEventAction
  | EventsEditEventFulfilledAction
  | EventsEditEventRejectedAction
  | EventsEditStatusAction
  | EventsEditStatusFulfilledAction
  | EventsEditStatusRejectedAction
  | EventsResetEventAction;
