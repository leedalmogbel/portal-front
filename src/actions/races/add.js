/**
 * @flow
 */

import { isEmpty } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const addRace =
  (data: any): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'races/addRace' });
    const {
      eventCreate,
      location,
      country,
      startDate,
      endDate,
      photo,
      file,
      eventName,
      seasonId,
      noticeTitle,
      noticeBody,
      stableName,
      specific,
      count,
      stables,
      pvtStable,
      sheikhStable,
      raceCode,
      raceCategory,
      ...rest
    } = data;

    let startTime = data.eventDate.split('T')[1];
    console.log('zxczxc', data);
    let params = {
      ...rest,
      active: true,
      notice: { title: noticeTitle, body: noticeBody },
      startTime: startTime,
      eventId: !isEmpty(eventName) ? parseInt(eventName.value) : null,
      metadata: stables,
      pvtStable: pvtStable.value + '',
      sheikhStable: sheikhStable.value + '',
      raceCode: raceCode.value,
      raceCategory: raceCategory.value,
      status: 'approved',
    };

    if (eventCreate) {
      params = {
        ...params,
        eventCreate: eventCreate,
        startDate: startDate,
        endDate: endDate,
        eventName: data.eventName,
        location: location.value,
        country: country.value,
      };
    }

    console.log('params', params);

    const formData = new FormData();
    formData.append('file', file[0]);
    formData.append('photo', photo[0]);
    formData.append('data', JSON.stringify(params));

    // try {
    //   const res = await eievApi.post('races', formData);
    //   if (res.status !== 200) {
    //     throw new Error(`Status code: ${res.status}`);
    //   }
    //   dispatch({
    //     type: 'races/addRaceFulfilled',
    //     payload: res.data,
    //   });
    // } catch (error) {
    //   dispatch({
    //     type: 'races/addRaceRejected',
    //     error,
    //   });
    // }
  };
