/**
 * @flow
 */
import { pickBy } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

// TODO: Remove id params; Fetch races by accesstoken
/**
 * @param id: User Id
 */
export const fetchRaces =
  ({ id, query }: { id: string, query?: string }): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'races/fetchRaces' });
    console.log(query);
    try {
      const params = pickBy({
        id,
        query,
      });
      const res = await eievApi.get('races', { params });

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'races/fetchRacesFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'races/fetchRacesRejected',
        error,
      });
    }
  };

export const fetchRace =
  (id: string): ThunkAction =>
  async (dispatch) => {
    console.log('DISPATCH ACTION');
    dispatch({ type: 'races/fetchRace' });

    try {
      const res = await eievApi.get(`races/${id}`);
      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      console.log('lols', res.data);
      dispatch({
        type: 'races/fetchRaceFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'races/fetchRaceRejected',
        error,
      });
    }
  };
