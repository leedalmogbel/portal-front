/**
 * @flow
 */

import type { Race } from 'types/race';

type RacesPayload = {|
  data: Array<Race>,
  messsage: string,
  status: boolean,
|};

type RacePayload = {|
  data: Race,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH RACES
 */
type RacesFetchAction = {|
  type: 'races/fetchRaces',
|};

type RacesFetchFulfilledAction = {|
  type: 'races/fetchRacesFulfilled',
  payload: RacesPayload,
|};

type RacesFetchRejectedAction = {|
  type: 'races/fetchRacesRejected',
  error: Error,
|};

type RacesFetchResetAction = {|
  type: 'races/resetFetchRaces',
|};

/**
 * FETCH RACE
 */
type RacesFetchRaceAction = {|
  type: 'races/fetchRace',
|};

type RacesFetchRaceFulfilledAction = {|
  type: 'races/fetchRaceFulfilled',
  payload: RacePayload,
|};

type RacesFetchRaceRejectedAction = {|
  type: 'races/fetchRaceRejected',
  error: Error,
|};

type RacesResetRaceAction = {|
  type: 'races/resetRace',
|};

// EDIT RACE
type RacesEditRaceAction = {|
  type: 'races/editRace',
|};

type RacesEditRaceFulfilledAction = {|
  type: 'races/editRaceFulfilled',
  payload: RacePayload,
|};

type RacesEditRaceRejectedAction = {|
  type: 'races/editRaceRejected',
  error: Error,
|};

/**
 * ADD RACE
 */
type RacesAddRaceAction = {|
  type: 'races/addRace',
|};

type RacesAddRaceFulfilledAction = {|
  type: 'races/addRaceFulfilled',
  payload: RacePayload,
|};

type RacesAddRaceRejectedAction = {|
  type: 'races/addRaceRejected',
  error: Error,
|};

// EDIT STATUS
type RacesEditStatusAction = {|
  type: 'races/editRaceStatus',
|};

type RacesEditStatusFulfilledAction = {|
  type: 'races/editRaceStatusFulfilled',
  payload: RacePayload,
|};

type RacesEditStatusRejectedAction = {|
  type: 'races/editRaceStatusRejected',
  error: Error,
|};

export type RacesAction =
  | RacesFetchAction
  | RacesFetchFulfilledAction
  | RacesFetchRejectedAction
  | RacesFetchResetAction
  | RacesFetchRaceAction
  | RacesFetchRaceFulfilledAction
  | RacesFetchRaceRejectedAction
  | RacesAddRaceAction
  | RacesAddRaceFulfilledAction
  | RacesAddRaceRejectedAction
  | RacesEditRaceAction
  | RacesEditRaceFulfilledAction
  | RacesEditRaceRejectedAction
  | RacesEditStatusAction
  | RacesEditStatusFulfilledAction
  | RacesEditStatusRejectedAction
  | RacesResetRaceAction;
