/**
 * @flow
 */
import { isEmpty } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const editRace =
  (data: any, id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'races/editRace' });
    const {
      eventCreate,
      location,
      country,
      startDate,
      endDate,
      photo,
      file,
      eventName,
      seasonId,
      noticeTitle,
      noticeBody,
      specific,
      stables,
      ...rest
    } = data;

    let startTime = data.eventDate.split('T')[1];
    const params = {
      ...rest,
      active: true,
      notice: { title: noticeTitle, body: noticeBody },
      startTime: startTime,
      eventId: !isEmpty(eventName) ? parseInt(eventName.value) : null,
      metadata: stables,
      // status: 'pending',
    };
    console.log('eventName', !isEmpty(eventName));
    console.log('value', !isEmpty(eventName.value));
    console.log('zfile', params);

    const formData = new FormData();
    formData.append('file', file.length !== 0 ? file[0] : []);
    formData.append('photo', photo.length !== 0 ? photo[0] : []);
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.patch(`races/${id}`, formData);
      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      dispatch({
        type: 'races/editRaceFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'races/editRaceRejected',
        error,
      });
    }
  };

export const editRaceStatus =
  (id: string, status: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'races/editRaceStatus' });

    const params = {
      status,
    };

    try {
      const res = await eievApi.patch(`races/${id}/status`, params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'races/editRaceStatusFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'races/editRaceStatusRejected',
        error,
      });
    }
  };
