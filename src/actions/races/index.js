/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetRace = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'races/resetRace' });
};
