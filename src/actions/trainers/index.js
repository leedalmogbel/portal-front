/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetTrainer = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'trainers/resetTrainer' });
};
