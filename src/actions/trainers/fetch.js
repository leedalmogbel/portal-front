/**
 * @flow
 */

import { pickBy } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

// TODO: Remove id params; Fetch trainers by accesstoken
/**
 * @param id: User Id
 */
export const fetchTrainers =
  ({ id, query }: { id: string, query?: string }): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'trainers/fetchTrainers' });
    try {
      const params = pickBy({
        id,
        query,
      });
      console.log(params);
      const res = await eievApi.get('trainers', { params });

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'trainers/fetchTrainersFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'trainers/fetchTrainersRejected',
        error,
      });
    }
  };

export const fetchTrainer =
  (id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'trainers/fetchTrainer' });

    try {
      const res = await eievApi.get(`trainers/${id}`);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'trainers/fetchTrainerFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'trainers/fetchTrainerRejected',
        error,
      });
    }
  };
