/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const editTrainer =
  (data: any, id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'trainers/editTrainer' });

    const { documentExpiryFile, ...rest } = data;

    // TODO: Remove unchanged data
    const params = {
      ...rest,
      discipline: data.discipline.value,
      visa: data.visa.value,
      homeCountry: data.homeCountry.value,
      uaeCountry: data.uaeCountry.value,
      uaeCity: data.uaeCity.value,
    };

    const formData = new FormData();
    formData.append('file', documentExpiryFile[0]);
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.patch(`trainers/${id}`, formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'trainers/editTrainerFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'trainers/editTrainerRejected',
        error,
      });
    }
  };

export const editTrainerStatus =
  (id: string, status: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'trainers/editTrainerStatus' });

    const params = {
      status,
    };

    try {
      const res = await eievApi.patch(`trainers/${id}/status`, params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'trainers/editTrainerStatusFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'trainers/editTrainerStatusRejected',
        error,
      });
    }
  };
