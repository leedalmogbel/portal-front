/**
 * @flow
 */

import type { Trainer } from 'types/trainer';

type TrainersPayload = {|
  data: Array<Trainer>,
  messsage: string,
  status: boolean,
|};

type TrainerPayload = {|
  data: Trainer,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH TRAINERS
 */
type TrainersFetchAction = {|
  type: 'trainers/fetchTrainers',
|};

type TrainersFetchFulfilledAction = {|
  type: 'trainers/fetchTrainersFulfilled',
  payload: TrainersPayload,
|};

type TrainersFetchRejectedAction = {|
  type: 'trainers/fetchTrainersRejected',
  error: Error,
|};

type TrainersFetchResetAction = {|
  type: 'trainers/resetFetchTrainers',
|};

/**
 * FETCH TRAINER
 */
type TrainersFetchTrainerAction = {|
  type: 'trainers/fetchTrainer',
|};

type TrainersFetchTrainerFulfilledAction = {|
  type: 'trainers/fetchTrainerFulfilled',
  payload: TrainerPayload,
|};

type TrainersFetchTrainerRejectedAction = {|
  type: 'trainers/fetchTrainerRejected',
  error: Error,
|};

type TrainersResetTrainerAction = {|
  type: 'trainers/resetTrainer',
|};

// EDIT USER
type TrainersEditTrainerAction = {|
  type: 'trainers/editTrainer',
|};

type TrainersEditTrainerFulfilledAction = {|
  type: 'trainers/editTrainerFulfilled',
  payload: TrainerPayload,
|};

type TrainersEditTrainerRejectedAction = {|
  type: 'trainers/editTrainerRejected',
  error: Error,
|};

/**
 * ADD TRAINER
 */
type TrainersAddTrainerAction = {|
  type: 'trainers/addTrainer',
|};

type TrainersAddTrainerFulfilledAction = {|
  type: 'trainers/addTrainerFulfilled',
  payload: TrainerPayload,
|};

type TrainersAddTrainerRejectedAction = {|
  type: 'trainers/addTrainerRejected',
  error: Error,
|};

// EDIT STATUS
type TrainersEditStatusAction = {|
  type: 'trainers/editTrainerStatus',
|};

type TrainersEditStatusFulfilledAction = {|
  type: 'trainers/editTrainerStatusFulfilled',
  payload: TrainerPayload,
|};

type TrainersEditStatusRejectedAction = {|
  type: 'trainers/editTrainerStatusRejected',
  error: Error,
|};

export type TrainersAction =
  | TrainersFetchAction
  | TrainersFetchFulfilledAction
  | TrainersFetchRejectedAction
  | TrainersFetchResetAction
  | TrainersFetchTrainerAction
  | TrainersFetchTrainerFulfilledAction
  | TrainersFetchTrainerRejectedAction
  | TrainersAddTrainerAction
  | TrainersAddTrainerFulfilledAction
  | TrainersAddTrainerRejectedAction
  | TrainersEditTrainerAction
  | TrainersEditTrainerFulfilledAction
  | TrainersEditTrainerRejectedAction
  | TrainersEditStatusAction
  | TrainersEditStatusFulfilledAction
  | TrainersEditStatusRejectedAction
  | TrainersResetTrainerAction;
