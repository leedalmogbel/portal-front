/**
 * @flow
 */

import type { UserWithPermission } from 'types/user';

type AuthPayload = {|
  data: UserWithPermission,
  messsage: string,
  status: boolean,
|};

type AuthLoginAction = {|
  type: 'auth/login',
|};

type AuthLoginFulfilledAction = {|
  type: 'auth/loginFulfilled',
  payload: AuthPayload,
|};

type AuthLoginRejectedAction = {|
  type: 'auth/loginRejected',
  error: Error,
|};

type AuthLoginResetAction = {|
  type: 'auth/resetLogin',
|};

type AuthLogoutAction = {|
  type: 'auth/logout',
|};

type AuthLogoutFulfilledAction = {|
  type: 'auth/logoutFulfilled',
|};

type AuthLogoutRejectedAction = {|
  type: 'auth/logoutRejected',
  error: Error,
|};

type AuthLogoutResetAction = {|
  type: 'auth/resetLogout',
|};

export type AuthAction =
  | AuthLoginAction
  | AuthLoginFulfilledAction
  | AuthLoginRejectedAction
  | AuthLoginResetAction
  | AuthLogoutAction
  | AuthLogoutFulfilledAction
  | AuthLogoutRejectedAction
  | AuthLogoutResetAction;
