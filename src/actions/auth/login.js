/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

type Options = {
  username: string,
  password: string,
};

export const authLogin =
  ({ username, password }: Options): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'auth/login' });

    const params = { username, password };

    try {
      const res = await eievApi.post('users/login', params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'auth/loginFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'auth/loginRejected',
        error,
      });
    }
  };
