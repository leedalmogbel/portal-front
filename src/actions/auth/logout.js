/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const authLogout = (): ThunkAction => async (dispatch) => {
  dispatch({ type: 'auth/logout' });

  try {
    // TODO: Logout API
    // const res = await eievApi.post('users/logout');

    // if (res.status !== 200) {
    //   throw new Error(`Status code: ${res.status}`);
    // }

    dispatch({
      type: 'auth/logoutFulfilled',
    });
  } catch (error) {
    dispatch({
      type: 'auth/logoutRejected',
      error,
    });
  }
};
