/**
 * @flow
 */

import type { Stable } from 'types/stable';

type StablesPayload = {|
  data: Array<Stable>,
  messsage: string,
  status: boolean,
|};

type StablePayload = {|
  data: Stable,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH EVENTS
 */
type StablesFetchAction = {|
  type: 'stables/fetchStables',
|};

type StablesFetchFulfilledAction = {|
  type: 'stables/fetchStablesFulfilled',
  payload: StablesPayload,
|};

type StablesFetchRejectedAction = {|
  type: 'stables/fetchStablesRejected',
  error: Error,
|};

type StablesFetchResetAction = {|
  type: 'stables/resetFetchStables',
|};

/**
 * FETCH EVENT
 */
type StablesFetchStableAction = {|
  type: 'stables/fetchStable',
|};

type StablesFetchStableFulfilledAction = {|
  type: 'stables/fetchStableFulfilled',
  payload: StablePayload,
|};

type StablesFetchStableRejectedAction = {|
  type: 'stables/fetchStableRejected',
  error: Error,
|};

type StablesResetStableAction = {|
  type: 'stables/resetStable',
|};

// EDIT EVENT
type StablesEditStableAction = {|
  type: 'stables/editStable',
|};

type StablesEditStableFulfilledAction = {|
  type: 'stables/editStableFulfilled',
  payload: StablePayload,
|};

type StablesEditStableRejectedAction = {|
  type: 'stables/editStableRejected',
  error: Error,
|};

/**
 * ADD EVENT
 */
type StablesAddStableAction = {|
  type: 'stables/addStable',
|};

type StablesAddStableFulfilledAction = {|
  type: 'stables/addStableFulfilled',
  payload: StablePayload,
|};

type StablesAddStableRejectedAction = {|
  type: 'stables/addStableRejected',
  error: Error,
|};

// EDIT STATUS
type StablesEditStatusAction = {|
  type: 'stables/editStableStatus',
|};

type StablesEditStatusFulfilledAction = {|
  type: 'stables/editStableStatusFulfilled',
  payload: StablePayload,
|};

type StablesEditStatusRejectedAction = {|
  type: 'stables/editStableStatusRejected',
  error: Error,
|};

export type StablesAction =
  | StablesFetchAction
  | StablesFetchFulfilledAction
  | StablesFetchRejectedAction
  | StablesFetchResetAction
  | StablesFetchStableAction
  | StablesFetchStableFulfilledAction
  | StablesFetchStableRejectedAction
  | StablesAddStableAction
  | StablesAddStableFulfilledAction
  | StablesAddStableRejectedAction
  | StablesEditStableAction
  | StablesEditStableFulfilledAction
  | StablesEditStableRejectedAction
  | StablesEditStatusAction
  | StablesEditStatusFulfilledAction
  | StablesEditStatusRejectedAction
  | StablesResetStableAction;
