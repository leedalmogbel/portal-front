/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const addStable =
  (data: any): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'stables/addStable' });
    const { user, userId, stableType, ...rest } = data;

    console.log('stable', data);
    console.log('user', user);
    console.log('rest', rest);

    const params = {
      ...rest,
      metadata: data.metadata || {},
      entryCount: parseInt(data.entryCount),
      name: data.name,
      userId: parseInt(user.value),
      stableType: data.stableType.value,
      active: true,
      status: 'approved',
    };

    const formData = new FormData();
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.post('stables', formData);
      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      dispatch({
        type: 'stables/addStableFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'stables/addStableRejected',
        error,
      });
    }
  };
