/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetStable = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'stables/resetStable' });
};
