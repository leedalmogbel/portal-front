/**
 * @flow
 */
import { pickBy } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

// TODO: Remove id params; Fetch stables by accesstoken
/**
 * @param id: User Id
 */
export const fetchStables =
  ({ id, query }: { id: string, query?: string }): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'stables/fetchStables' });

    try {
      const params = pickBy({
        id,
        query,
      });
      const res = await eievApi.get('stables', { params });
      console.log(res);
      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      console.log(res.data);

      dispatch({
        type: 'stables/fetchStablesFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'stables/fetchStablesRejected',
        error,
      });
    }
  };

export const fetchStable =
  (id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'stables/fetchStable' });

    try {
      const res = await eievApi.get(`stables/${id}`);
      console.log('res', res);
      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      console.log('lols', res.data);
      dispatch({
        type: 'stables/fetchStableFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'stables/fetchStableRejected',
        error,
      });
    }
  };
