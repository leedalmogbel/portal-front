/**
 * @flow
 */
import { isEmpty } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const editStable =
  (data: any, id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'stables/editStable' });
    const { user, userId, stableTypeOptions, ...rest } = data;
    console.log('stable', data);
    console.log('user', user);
    console.log('rest', rest);
    const params = {
      ...rest,
      metadata: data.metadata || {},
      entryCount: parseInt(data.entryCount),
      name: data.name,
      userId: parseInt(user.value),
      stableType: !isEmpty(data?.stableType?.value)
        ? data?.stableType?.value
        : stableTypeOptions?.value,
      active: true,
      status: 'pending',
    };

    const formData = new FormData();
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.patch(`stables/${id}`, formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'stables/editStableFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'stables/editStableRejected',
        error,
      });
    }
  };

export const editStableStatus =
  (id: string, status: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'stables/editStableStatus' });

    const params = {
      status,
    };

    try {
      const res = await eievApi.patch(`stables/${id}/status`, params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'stables/editStableStatusFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'stables/editStableStatusRejected',
        error,
      });
    }
  };
