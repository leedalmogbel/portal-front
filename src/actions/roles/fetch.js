/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const fetchRoles = (): ThunkAction => async (dispatch) => {
  dispatch({ type: 'roles/fetchRoles' });

  try {
    const res = await eievApi.get('roles');

    if (res.status !== 200) {
      throw new Error(`Status code: ${res.status}`);
    }

    dispatch({
      type: 'roles/fetchRolesFulfilled',
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: 'roles/fetchRolesRejected',
      error,
    });
  }
};
