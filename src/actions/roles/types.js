/**
 * @flow
 */

import type { Role } from 'types/user';

type RolesPayload = {|
  data: Array<Role>,
  status: string,
  message: string,
|};

type RolesFetch = {|
  type: 'roles/fetchRoles',
|};

type RolesFetchFullfilled = {|
  type: 'roles/fetchRolesFulfilled',
  payload: RolesPayload,
|};

type RolesFetchRejected = {|
  type: 'roles/fetchRolesRejected',
  error: Error,
|};

type RolesFetchReset = {|
  type: 'roles/resetFetchRoles',
|};

export type RolesAction =
  | RolesFetch
  | RolesFetchFullfilled
  | RolesFetchRejected
  | RolesFetchReset;
