/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetEntry = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'entries/resetEntry' });
};
