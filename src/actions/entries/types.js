/**
 * @flow
 */

import type { Entry } from 'types/entry';

type EntriesPayload = {|
  data: Array<Entry>,
  messsage: string,
  status: boolean,
|};

type EntryPayload = {|
  data: Entry,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH EVENTS
 */
type EntriesFetchAction = {|
  type: 'entries/fetchEntries',
|};

type EntriesFetchFulfilledAction = {|
  type: 'entries/fetchEntriesFulfilled',
  payload: EntriesPayload,
|};

type EntriesFetchRejectedAction = {|
  type: 'entries/fetchEntriesRejected',
  error: Error,
|};

type EntriesFetchResetAction = {|
  type: 'entries/resetFetchEntries',
|};

/**
 * FETCH EVENT
 */
type EntriesFetchEntryAction = {|
  type: 'entries/fetchEntry',
|};

type EntriesFetchEntryFulfilledAction = {|
  type: 'entries/fetchEntryFulfilled',
  payload: EntryPayload,
|};

type EntriesFetchEntryRejectedAction = {|
  type: 'entries/fetchEntryRejected',
  error: Error,
|};

type EntriesResetEntryAction = {|
  type: 'entries/resetEntry',
|};

// EDIT EVENT
type EntriesEditEntryAction = {|
  type: 'entries/editEntry',
|};

type EntriesEditEntryFulfilledAction = {|
  type: 'entries/editEntryFulfilled',
  payload: EntryPayload,
|};

type EntriesEditEntryRejectedAction = {|
  type: 'entries/editEntryRejected',
  error: Error,
|};

/**
 * ADD EVENT
 */
type EntriesAddEntryAction = {|
  type: 'entries/addEntry',
|};

type EntriesAddEntryFulfilledAction = {|
  type: 'entries/addEntryFulfilled',
  payload: EntryPayload,
|};

type EntriesAddEntryRejectedAction = {|
  type: 'entries/addEntryRejected',
  error: Error,
|};

// EDIT STATUS
type EntriesEditStatusAction = {|
  type: 'entries/editEntryStatus',
|};

type EntriesEditStatusFulfilledAction = {|
  type: 'entries/editEntryStatusFulfilled',
  payload: EntryPayload,
|};

type EntriesEditStatusRejectedAction = {|
  type: 'entries/editEntryStatusRejected',
  error: Error,
|};

export type EntriesAction =
  | EntriesFetchAction
  | EntriesFetchFulfilledAction
  | EntriesFetchRejectedAction
  | EntriesFetchResetAction
  | EntriesFetchEntryAction
  | EntriesFetchEntryFulfilledAction
  | EntriesFetchEntryRejectedAction
  | EntriesResetEntryAction
  | EntriesEditEntryAction
  | EntriesEditEntryFulfilledAction
  | EntriesEditEntryRejectedAction
  | EntriesAddEntryAction
  | EntriesAddEntryFulfilledAction
  | EntriesAddEntryRejectedAction
  | EntriesEditStatusAction
  | EntriesEditStatusFulfilledAction
  | EntriesEditStatusRejectedAction;
