/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const editEntry =
  (data: any, id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'entries/editEntry' });
    const { race, rider, horse, name, pledge, ...rest } = data;

    const params = {
      ...rest,
      raceId: parseInt(race.value),
      riderId: parseInt(rider.value),
      horseId: parseInt(horse.value),
      eventId: parseInt(race.event),
      active: true,
      status: 'pending',
    };

    const formData = new FormData();
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.patch(`entries/${id}`, formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'entries/editEntryFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'entries/editEntryRejected',
        error,
      });
    }
  };

export const editEntryStatus =
  (id: string, status: string, query: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'entries/editEntryStatus' });
    console.log(query, 'query');
    const params = {
      status,
      raceId: query,
    };

    try {
      const res = await eievApi.patch(`entries/${id}/status`, params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'entries/editEntryStatusFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'entries/editEntryStatusRejected',
        error,
      });
    }
  };

export const editManageEntry =
  (id: string, entries: Object): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'entries/editEntryStatus' });
    console.log('entries', entries);
    entries.forEach((item, i) => {
      console.log('iiiii', i);
      item.sequence = i + 1;
    });
    const params = {
      id,
      entries,
    };

    try {
      const res = await eievApi.patch(`entries/${id}/save`, params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'entries/editEntryStatusFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'entries/editEntryStatusRejected',
        error,
      });
    }
  };
