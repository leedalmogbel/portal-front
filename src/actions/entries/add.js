/**
 * @flow
 */

import { isEmpty } from 'lodash';
import { toast } from 'react-toastify';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const addEntry =
  (data: any): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'entries/addEntry' });
    const { entries, race, rider, horse, name, pledge, userId, ...rest } = data;
    console.log('york new city', data);

    const params = {
      ...rest,
      raceId: parseInt(race.value),
      // riderId: parseInt(rider.value),
      // horseId: parseInt(horse.value),
      eventId: parseInt(race.event),
      entries: entries,
      userId: parseInt(userId.value) || data.userId,
      active: true,
      status: 'pending',
    };

    console.log('controls', params);

    const formData = new FormData();
    formData.append('data', JSON.stringify(params));

    try {
      if (isEmpty(entries)) {
        const error = {
          statusCode: 500,
          message: 'Please add at least one entry',
          stack: 'Error: Please add at least one entry',
        };
        console.log('www');
        toast.error('Please add at least one entry');
        throw new Error(error);
      }
      console.log('GPSGPSGPSGPSGPS');

      const res = await eievApi.post('entries', formData);
      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      dispatch({
        type: 'entries/addEntryFulfilled',
        payload: res.data,
      });
    } catch (error) {
      console.log('zzzzzzzzssssss', error);
      dispatch({
        type: 'entries/addEntryRejected',
        error,
      });
    }
  };
