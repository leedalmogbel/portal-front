/**
 * @flow
 */
import { pickBy } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

// TODO: Remove id params; Fetch entries by accesstoken
/**
 * @param id: User Id
 */
export const fetchEntries =
  ({ id, query }: { id: string, query?: string }): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'entries/fetchEntries' });
    try {
      // const [searchParams] = useSearchParams();
      const params = pickBy({
        id,
        query,
      });

      // console.log('manage', searchParams.get('manage'));
      console.log('query', query);
      console.log('params', params);
      const res = await eievApi.get('entries', { params });

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      console.log('entriesview', res);
      dispatch({
        type: 'entries/fetchEntriesFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'entries/fetchEntriesRejected',
        error,
      });
    }
  };

export const fetchEntry =
  (id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'entries/fetchEntry' });

    try {
      const res = await eievApi.get(`entries/${id}`);
      console.log('res', res);
      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }
      console.log('lols', res.data);
      dispatch({
        type: 'entries/fetchEntryFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'entries/fetchEntryRejected',
        error,
      });
    }
  };

export const fetchManageEntries =
  ({ id, query }: { id: string, query?: string }): ThunkAction =>
  async (dispatch) => {
    // dispatch({ type: 'entries/fetchEntries' });
    try {
      let params = pickBy({
        id,
        query,
      });

      // console.log('manage usok');
      // console.log('query', query);
      console.log('params', params);
      params = { ...params, manage: true };
      const res = await eievApi.get('entries/manage', { params });
      console.log('rererere', res);
      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      console.log('[data]', res.data);

      dispatch({
        type: 'entries/fetchEntriesFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'entries/fetchEntriesRejected',
        error,
      });
    }
  };
