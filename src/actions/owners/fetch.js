/**
 * @flow
 */

import { pickBy } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

// TODO: Remove id params; Fetch owners by accesstoken
/**
 * @param id: User Id
 */
export const fetchOwners =
  ({ id, query }: { id: string, query?: string }): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'owners/fetchOwners' });
    try {
      const params = pickBy({
        id,
        query,
      });
      const res = await eievApi.get('owners', { params });

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'owners/fetchOwnersFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'owners/fetchOwnersRejected',
        error,
      });
    }
  };

export const fetchOwner =
  (id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'owners/fetchOwner' });

    try {
      const res = await eievApi.get(`owners/${id}`);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'owners/fetchOwnerFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'owners/fetchOwnerRejected',
        error,
      });
    }
  };
