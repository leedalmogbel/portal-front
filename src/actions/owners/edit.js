/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const editOwner =
  (data: any, id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'owners/editOwner' });

    const { documentExpiryFile, ...rest } = data;

    // TODO: Remove unchanged data
    const params = {
      ...rest,
      discipline: data.discipline.value,
      visa: data.visa.value,
      homeCountry: data.homeCountry.value,
      uaeCountry: data.uaeCountry.value,
      uaeCity: data.uaeCity.value,
    };

    const formData = new FormData();
    formData.append('file', documentExpiryFile[0]);
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.patch(`owners/${id}`, formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'owners/editOwnerFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'owners/editOwnerRejected',
        error,
      });
    }
  };

export const editOwnerStatus =
  (id: string, status: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'owners/editOwnerStatus' });

    const params = {
      status,
    };

    try {
      const res = await eievApi.patch(`owners/${id}/status`, params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'owners/editOwnerStatusFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'owners/editOwnerStatusRejected',
        error,
      });
    }
  };
