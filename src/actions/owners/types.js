/**
 * @flow
 */

import type { Owner } from 'types/owner';

type OwnersPayload = {|
  data: Array<Owner>,
  messsage: string,
  status: boolean,
|};

type OwnerPayload = {|
  data: Owner,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH OWNERS
 */
type OwnersFetchAction = {|
  type: 'owners/fetchOwners',
|};

type OwnersFetchFulfilledAction = {|
  type: 'owners/fetchOwnersFulfilled',
  payload: OwnersPayload,
|};

type OwnersFetchRejectedAction = {|
  type: 'owners/fetchOwnersRejected',
  error: Error,
|};

type OwnersFetchResetAction = {|
  type: 'owners/resetFetchOwners',
|};

/**
 * FETCH OWNER
 */
type OwnersFetchOwnerAction = {|
  type: 'owners/fetchOwner',
|};

type OwnersFetchOwnerFulfilledAction = {|
  type: 'owners/fetchOwnerFulfilled',
  payload: OwnerPayload,
|};

type OwnersFetchOwnerRejectedAction = {|
  type: 'owners/fetchOwnerRejected',
  error: Error,
|};

type OwnersResetOwnerAction = {|
  type: 'owners/resetOwner',
|};

// EDIT USER
type OwnersEditOwnerAction = {|
  type: 'owners/editOwner',
|};

type OwnersEditOwnerFulfilledAction = {|
  type: 'owners/editOwnerFulfilled',
  payload: OwnerPayload,
|};

type OwnersEditOwnerRejectedAction = {|
  type: 'owners/editOwnerRejected',
  error: Error,
|};

/**
 * ADD OWNER
 */
type OwnersAddOwnerAction = {|
  type: 'owners/addOwner',
|};

type OwnersAddOwnerFulfilledAction = {|
  type: 'owners/addOwnerFulfilled',
  payload: OwnerPayload,
|};

type OwnersAddOwnerRejectedAction = {|
  type: 'owners/addOwnerRejected',
  error: Error,
|};

// EDIT STATUS
type OwnersEditStatusAction = {|
  type: 'owners/editOwnerStatus',
|};

type OwnersEditStatusFulfilledAction = {|
  type: 'owners/editOwnerStatusFulfilled',
  payload: OwnerPayload,
|};

type OwnersEditStatusRejectedAction = {|
  type: 'owners/editOwnerStatusRejected',
  error: Error,
|};

export type OwnersAction =
  | OwnersFetchAction
  | OwnersFetchFulfilledAction
  | OwnersFetchRejectedAction
  | OwnersFetchResetAction
  | OwnersFetchOwnerAction
  | OwnersFetchOwnerFulfilledAction
  | OwnersFetchOwnerRejectedAction
  | OwnersAddOwnerAction
  | OwnersAddOwnerFulfilledAction
  | OwnersAddOwnerRejectedAction
  | OwnersEditOwnerAction
  | OwnersEditOwnerFulfilledAction
  | OwnersEditOwnerRejectedAction
  | OwnersEditStatusAction
  | OwnersEditStatusFulfilledAction
  | OwnersEditStatusRejectedAction
  | OwnersResetOwnerAction;
