/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const addOwner =
  (data: any): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'owners/addOwner' });

    const { documentExpiryFile, ...rest } = data;

    const params = {
      ...rest,
      active: true,
      discipline: data.discipline.value,
      visa: data.visa.value,
      homeCountry: data.homeCountry.value,
      uaeCountry: data.uaeCountry.value,
      uaeCity: data.uaeCity.value,
      status: 'pending',
    };

    const formData = new FormData();
    formData.append('file', documentExpiryFile[0]);
    formData.append('data', JSON.stringify(params));
    console.log(';', params);
    try {
      const res = await eievApi.post('owners', formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'owners/addOwnerFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'owners/addOwnerRejected',
        error,
      });
    }
  };
