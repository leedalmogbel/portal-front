/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetOwner = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'owners/resetOwner' });
};
