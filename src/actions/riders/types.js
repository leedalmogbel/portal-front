/**
 * @flow
 */

import type { Rider } from 'types/rider';

type RidersPayload = {|
  data: Array<Rider>,
  messsage: string,
  status: boolean,
|};

type RiderPayload = {|
  data: Rider,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH RIDERS
 */
type RidersFetchAction = {|
  type: 'riders/fetchRiders',
|};

type RidersFetchFulfilledAction = {|
  type: 'riders/fetchRidersFulfilled',
  payload: RidersPayload,
|};

type RidersFetchRejectedAction = {|
  type: 'riders/fetchRidersRejected',
  error: Error,
|};

type RidersFetchResetAction = {|
  type: 'riders/resetFetchRiders',
|};

/**
 * FETCH RIDER
 */
type RidersFetchRiderAction = {|
  type: 'riders/fetchRider',
|};

type RidersFetchRiderFulfilledAction = {|
  type: 'riders/fetchRiderFulfilled',
  payload: RiderPayload,
|};

type RidersFetchRiderRejectedAction = {|
  type: 'riders/fetchRiderRejected',
  error: Error,
|};

type RidersResetRiderAction = {|
  type: 'riders/resetRider',
|};

// EDIT RIDER
type RidersEditRiderAction = {|
  type: 'riders/editRider',
|};

type RidersEditRiderFulfilledAction = {|
  type: 'riders/editRiderFulfilled',
  payload: RiderPayload,
|};

type RidersEditRiderRejectedAction = {|
  type: 'riders/editRiderRejected',
  error: Error,
|};

/**
 * ADD RIDER
 */
type RidersAddRiderAction = {|
  type: 'riders/addRider',
|};

type RidersAddRiderFulfilledAction = {|
  type: 'riders/addRiderFulfilled',
  payload: RiderPayload,
|};

type RidersAddRiderRejectedAction = {|
  type: 'riders/addRiderRejected',
  error: Error,
|};

// EDIT STATUS
type RidersEditStatusAction = {|
  type: 'riders/editRiderStatus',
|};

type RidersEditStatusFulfilledAction = {|
  type: 'riders/editRiderStatusFulfilled',
  payload: RiderPayload,
|};

type RidersEditStatusRejectedAction = {|
  type: 'riders/editRiderStatusRejected',
  error: Error,
|};

export type RidersAction =
  | RidersFetchAction
  | RidersFetchFulfilledAction
  | RidersFetchRejectedAction
  | RidersFetchResetAction
  | RidersFetchRiderAction
  | RidersFetchRiderFulfilledAction
  | RidersFetchRiderRejectedAction
  | RidersAddRiderAction
  | RidersAddRiderFulfilledAction
  | RidersAddRiderRejectedAction
  | RidersEditRiderAction
  | RidersEditRiderFulfilledAction
  | RidersEditRiderRejectedAction
  | RidersEditStatusAction
  | RidersEditStatusFulfilledAction
  | RidersEditStatusRejectedAction
  | RidersResetRiderAction;
