/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const editRider =
  (data: any, id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'riders/editRider' });

    const { documentExpiryFile, ...rest } = data;

    // TODO: Remove unchanged data
    const params = {
      ...rest,
      discipline: data.discipline.value,
      visa: data.visa.value,
      homeCountry: data.homeCountry.value,
      uaeCountry: data.uaeCountry.value,
      uaeCity: data.uaeCity.value,
    };

    const formData = new FormData();
    formData.append('file', documentExpiryFile[0]);
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.patch(`riders/${id}`, formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'riders/editRiderFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'riders/editRiderRejected',
        error,
      });
    }
  };

export const editRiderStatus =
  (id: string, status: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'riders/editRiderStatus' });

    const params = {
      status,
    };

    try {
      const res = await eievApi.patch(`riders/${id}/status`, params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'riders/editRiderStatusFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'riders/editRiderStatusRejected',
        error,
      });
    }
  };
