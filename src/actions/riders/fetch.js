/**
 * @flow
 */
import { pickBy } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

// TODO: Remove id params; Fetch riders by accesstoken
/**
 * @param id: User Id
 */
export const fetchRiders =
  ({ id, query }: { id: string, query?: string }): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'riders/fetchRiders' });
    try {
      const params = pickBy({
        id,
        query,
      });
      const res = await eievApi.get('riders', { params });

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'riders/fetchRidersFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'riders/fetchRidersRejected',
        error,
      });
    }
  };

export const fetchRider =
  (id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'riders/fetchRider' });

    try {
      const res = await eievApi.get(`riders/${id}`);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'riders/fetchRiderFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'riders/fetchRiderRejected',
        error,
      });
    }
  };
