/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetRider = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'riders/resetRider' });
};
