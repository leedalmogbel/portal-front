/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const fetchDashboard =
  (id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'dashboard/fetch' });

    try {
      const params = {
        id,
      };
      const res = await eievApi.get('dashboard', { params });

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'dashboard/fetchFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'dashboard/fetchRejected',
        error,
      });
    }
  };
