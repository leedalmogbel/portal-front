/**
 * @flow
 */

import type { Dashboard } from 'types/dashboard';

type DashboardPayload = {|
  data: Dashboard,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH DASHBOARD
 */
type DashboardFetchAction = {|
  type: 'dashboard/fetch',
|};

type DashboardFetchFulfilledAction = {|
  type: 'dashboard/fetchFulfilled',
  payload: DashboardPayload,
|};

type DashboardFetchRejectedAction = {|
  type: 'dashboard/fetchRejected',
  error: Error,
|};

type DashboardFetchResetAction = {|
  type: 'dashboard/resetFetch',
|};

export type DashboardAction =
  | DashboardFetchAction
  | DashboardFetchFulfilledAction
  | DashboardFetchRejectedAction
  | DashboardFetchResetAction;
