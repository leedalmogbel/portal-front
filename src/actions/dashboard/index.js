/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetDashboard = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'dashboard/resetFetch' });
};
