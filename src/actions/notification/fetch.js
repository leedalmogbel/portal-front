/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const fetchNotification =
  (id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'notification/fetch' });

    try {
      const params = {
        id,
      };
      console.log('zzzz', id);
      const res = await eievApi.get('notify', { params });
      console.log('res', res);
      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'notification/fetchFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'notification/fetchRejected',
        error,
      });
    }
  };
