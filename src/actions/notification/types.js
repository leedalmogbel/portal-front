/**
 * @flow
 */

import type { Notification } from 'types/notification';

type NotificationPayload = {|
  data: Notification,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH DASHBOARD
 */
type NotificationFetchAction = {|
  type: 'notification/fetch',
|};

type NotificationFetchFulfilledAction = {|
  type: 'notification/fetchFulfilled',
  payload: NotificationPayload,
|};

type NotificationFetchRejectedAction = {|
  type: 'notification/fetchRejected',
  error: Error,
|};

type NotificationFetchResetAction = {|
  type: 'notification/resetFetch',
|};

export type NotificationAction =
  | NotificationFetchAction
  | NotificationFetchFulfilledAction
  | NotificationFetchRejectedAction
  | NotificationFetchResetAction;
