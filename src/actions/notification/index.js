/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetNotification = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'notification/resetFetch' });
};
