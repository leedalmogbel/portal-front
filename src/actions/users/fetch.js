/**
 * @flow
 */
import { pickBy } from 'lodash';

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const fetchUsers =
  ({ id, query }: { id: string, query?: string }): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'users/fetchUsers' });

    try {
      const params = pickBy({ id, query });
      const res = await eievApi.get('users', { params });

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'users/fetchUsersFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'users/fetchUsersRejected',
        error,
      });
    }
  };

export const fetchUserDetail =
  (id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'users/fetchUserDetail' });

    try {
      const res = await eievApi.get(`users/${id}`);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'users/fetchUserDetailFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'users/fetchUserDetailRejected',
        error,
      });
    }
  };
