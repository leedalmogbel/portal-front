/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const addUser =
  (data: any): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'users/addUser' });

    const { documentExpiryFile, ...rest } = data;
    delete rest.confirmPassword;
    delete rest.role;

    console.log(data);
    const params = {
      ...rest,
      active: true,
      location: data.location.value,
      discipline: data.discipline.value,
      roleId: data.role.value,
      status: 'pending',
    };

    const formData = new FormData();
    formData.append('file', documentExpiryFile[0]);
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.post('users', formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'users/addUserFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'users/addUserRejected',
        error,
      });
    }
  };
