/**
 * @flow
 */

import type { ThunkAction } from 'actions/types';

export const resetUser = (): ThunkAction => (dispatch) => {
  dispatch({ type: 'users/resetUser' });
};
