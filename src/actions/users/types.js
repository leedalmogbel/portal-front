/**
 * @flow
 */

import type { User } from 'types/user';

/**
 * PAYLOADS
 */
type UsersFetchPayload = {|
  data: Array<User>,
  messsage: string,
  status: boolean,
|};

type UserPayload = {|
  data: User,
  messsage: string,
  status: boolean,
|};

/**
 * FETCH USERS
 */
type UsersFetchAction = {|
  type: 'users/fetchUsers',
|};

type UsersFetchFulfilledAction = {|
  type: 'users/fetchUsersFulfilled',
  payload: UsersFetchPayload,
|};

type UsersFetchRejectedAction = {|
  type: 'users/fetchUsersRejected',
  error: Error,
|};

type UsersFetchResetAction = {|
  type: 'users/resetFetchUsers',
|};

/**
 * ADD USER
 */
type UsersAddUserAction = {|
  type: 'users/addUser',
|};

type UsersAddUserFulfilledAction = {|
  type: 'users/addUserFulfilled',
  payload: UserPayload,
|};

type UsersAddUserRejectedAction = {|
  type: 'users/addUserRejected',
  error: Error,
|};

// EDIT USER
type UsersEditUserAction = {|
  type: 'users/editUser',
|};

type UsersEditUserFulfilledAction = {|
  type: 'users/editUserFulfilled',
  payload: UserPayload,
|};

type UsersEditUserRejectedAction = {|
  type: 'users/editUserRejected',
  error: Error,
|};

// EDIT STATUS
type UsersEditStatusAction = {|
  type: 'users/editUserStatus',
|};

type UsersEditStatusFulfilledAction = {|
  type: 'users/editUserStatusFulfilled',
  payload: UserPayload,
|};

type UsersEditStatusRejectedAction = {|
  type: 'users/editUserStatusRejected',
  error: Error,
|};

// FETCH USER

type UsersFetchUserDetailAction = {|
  type: 'users/fetchUserDetail',
|};

type UsersFetchUserDetailFulfilledAction = {|
  type: 'users/fetchUserDetailFulfilled',
  payload: UserPayload,
|};

type UsersFetchUserDetailRejectedAction = {|
  type: 'users/fetchUserDetailRejected',
  error: Error,
|};

type UsersResetUserAction = {|
  type: 'users/resetUser',
|};

export type UsersAction =
  | UsersFetchAction
  | UsersFetchFulfilledAction
  | UsersFetchRejectedAction
  | UsersFetchResetAction
  | UsersAddUserAction
  | UsersAddUserFulfilledAction
  | UsersAddUserRejectedAction
  | UsersEditUserAction
  | UsersEditUserFulfilledAction
  | UsersEditUserRejectedAction
  | UsersEditStatusAction
  | UsersEditStatusFulfilledAction
  | UsersEditStatusRejectedAction
  | UsersFetchUserDetailAction
  | UsersFetchUserDetailFulfilledAction
  | UsersFetchUserDetailRejectedAction
  | UsersResetUserAction;
