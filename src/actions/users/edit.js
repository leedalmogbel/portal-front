/**
 * @flow
 */

import { eievApi } from 'utils/api';
import type { ThunkAction } from 'actions/types';

export const editUser =
  (data: any, id: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'users/editUser' });

    const { documentExpiryFile, ...rest } = data;
    delete rest.confirmPassword;
    delete rest.password;
    delete rest.role;

    // TODO: Remove unchanged data
    const params = {
      ...rest,
      location: data.location.value,
      discipline: data.discipline.value,
      roleId: data.role.value,
      oldPassword: data.password,
      newPassword: data.confirmPassword,
    };

    const formData = new FormData();
    formData.append('file', documentExpiryFile[0]);
    formData.append('data', JSON.stringify(params));

    try {
      const res = await eievApi.patch(`users/${id}`, formData);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'users/editUserFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'users/editUserRejected',
        error,
      });
    }
  };

export const editUserStatus =
  (id: string, status: string): ThunkAction =>
  async (dispatch) => {
    dispatch({ type: 'users/editUserStatus' });

    const params = {
      status,
    };

    try {
      const res = await eievApi.patch(`users/${id}/status`, params);

      if (res.status !== 200) {
        throw new Error(`Status code: ${res.status}`);
      }

      dispatch({
        type: 'users/editUserStatusFulfilled',
        payload: res.data,
      });
    } catch (error) {
      dispatch({
        type: 'users/editUserStatusRejected',
        error,
      });
    }
  };
