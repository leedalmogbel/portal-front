/**
 * @flow
 */

import type { AuthAction } from './auth/types';
import type { UsersAction } from './users/types';
import type { OwnersAction } from './owners/types';
import type { TrainersAction } from './trainers/types';
import type { RidersAction } from './riders/types';
import type { RolesAction } from './roles/types';
import type { DashboardAction } from './dashboard/types';
import type { PasswordAction } from './password/types';
import type { HorsesAction } from './horses/types';
import type { EventsAction } from './events/types';
import type { RacesAction } from './races/types';
import type { EntriesAction } from './entries/types';
import type { StablesAction } from './stables/types';
import type { NotificationAction } from './notification/types';

export type Action =
  | AuthAction
  | UsersAction
  | OwnersAction
  | RolesAction
  | TrainersAction
  | RidersAction
  | DashboardAction
  | HorsesAction
  | EventsAction
  | RacesAction
  | EntriesAction
  | StablesAction
  | NotificationAction
  | PasswordAction;

export type PromiseAction = Promise<Action>;
export type ThunkAction = (dispatch: Dispatch) => any;
export type Dispatch = (action: Action | ThunkAction | PromiseAction) => void;
