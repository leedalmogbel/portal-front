/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { useCallback, Fragment } from 'react';
import { useNavigate } from 'react-router-dom';
import type { Node } from 'react';
import tw from 'twin.macro';
import { DateTime } from 'luxon';
import { FaUserCog } from 'react-icons/fa';
import { IoMdNotificationsOutline } from 'react-icons/io';
import { Menu, Transition } from '@headlessui/react';

import DropdownHeader from 'components/common/DropdownHeader';
import logo from 'assets/images/bg-auth.jpg';
import { useUserDetail } from 'views/UserDetail/hook';
import { useAuth } from 'hooks/auth';
import Button from 'components/common/Button';

const HeaderContainer = tw.header`flex justify-between py-3 px-6 bg-gray-50 shadow text-gray-900 italic`,
  Role = tw.p`text-sm font-bold`,
  Date = tw.p`text-xs`,
  UserType = tw.p`text-sm`;

const Header = (): Node => {
  const today = DateTime.now().toLocaleString(
    DateTime.DATETIME_MED_WITH_WEEKDAY
  );
  const { user } = useAuth();

  const navigate = useNavigate();
  const { changeSceneToEdit } = useUserDetail();

  const onClick = useCallback(() => {
    // console.log('auth.id', user);
    changeSceneToEdit();
    navigate(`/users/${user?.id}`);
  }, [changeSceneToEdit, navigate, user]);

  return (
    <HeaderContainer>
      <div>
        <Role>Private Individual Stable</Role>
        <Date>{today}</Date>
        <UserType>logged as : {user?.userType}</UserType>
      </div>
      <div tw="flex">
        {/* <Button> */}
        {/* <div tw="">
          <button tw="block w-8 h-8 rounded-full overflow-hidden border-2 border-gray-600 focus:outline-none focus:border-black">
            <img tw="w-full h-full object-cover" src={logo} alt={`profile`} />
          </button>
        </div> */}
        <div>
          <button tw="relative inline-block" onClick={onClick}>
            <IoMdNotificationsOutline tw="w-8 h-8 hover:(bg-white text-gold-400)" />
            {/* <span tw="absolute top-0 left-0 w-2 h-2 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full"></span> */}
          </button>
        </div>
        {/* START: Top-right Menu */}
        <Menu as="div" tw="relative inline-block">
          <div>
            <Menu.Button tw="block w-8 h-8 rounded-full overflow-hidden border-2 border-gray-600 focus:outline-none focus:border-black">
              <img tw="w-full h-full object-cover" src={logo} alt={`profile`} />
            </Menu.Button>
          </div>
          <Transition
            as={Fragment}
            enter="transition transform duration-100 ease-out"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="transition transform duration-75 ease-in"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95">
            <Menu.Items tw="z-10 absolute right-0 w-32 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
              <div tw="px-1 py-1 " key={1}>
                <Menu.Item key={1}>
                  <button
                    tw="hover:(bg-gold-400 text-white) text-gray-900 flex w-full items-center rounded-md px-2 py-2 text-sm"
                    onClick={onClick}>
                    <FaUserCog tw="w-4 h-4 mr-2" />
                    {`Edit Profile`}
                  </button>
                </Menu.Item>
              </div>
            </Menu.Items>
          </Transition>
        </Menu>
        {/* END: Top-right Menu */}
      </div>
    </HeaderContainer>
  );
};

export default Header;
