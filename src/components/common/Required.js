/**
 * @flow
 * @jsxImportSource @emotion/react
 */
import * as React from 'react';
import 'twin.macro';

const Required = (): React.Node => (
  <span className="text-red-500 font-black text-6xl"> *</span>
);

export default Required;
