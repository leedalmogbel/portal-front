/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import type { Node } from 'react';
import tw, { styled } from 'twin.macro';
import { Listbox } from '@headlessui/react';
import { Controller } from 'react-hook-form';
import { first } from 'lodash';
import { SelectorIcon } from '@heroicons/react/solid';

import type { Option } from 'types';

const Button = styled(Listbox.Button)(({ open, disabled }) => [
  tw`relative w-full py-2 pl-3 pr-10 text-sm text-left bg-white border border-gray-300 rounded shadow-sm cursor-pointer`,
  open && tw`ring-1 ring-gold-400 border-gold-400`,
  disabled && tw`bg-gray-100 cursor-not-allowed`,
]);

const ListOptions = tw(
    Listbox.Options
  )`absolute mt-1 max-h-60 z-10 w-full overflow-auto rounded-md bg-white py-1 shadow-lg focus:outline-none text-sm`,
  ListOption = tw(
    Listbox.Option
  )`relative select-none py-2 pl-4 pr-4 hover:(bg-gold-400 text-gray-50) cursor-pointer`;

type SelectProps = {|
  id: string,
  name: string,
  options: Array<Option>,
  readOnly?: boolean,
  onChange?: Function,
|};

const Select = ({
  id,
  name,
  options,
  onChange,
  readOnly,
}: SelectProps): Node => (
  <Controller
    name={name}
    render={({ field: { value, onChange: _onChange } }) => (
      <Listbox
        value={value}
        onChange={(e) => {
          onChange(e);
          _onChange(e);
        }}
        disabled={readOnly}>
        {({ open }) => (
          <div tw="relative">
            <Button open={open} disabled={readOnly}>
              <span tw="block truncate">
                {value?.name || first(options).name}
              </span>
              <span tw="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                <SelectorIcon tw="w-5 h-5 text-gray-400" aria-hidden="true" />
              </span>
            </Button>

            <ListOptions name={name} id={id}>
              {options.map((option) => (
                <ListOption key={option.name} value={option}>
                  {option.name}
                </ListOption>
              ))}
            </ListOptions>
          </div>
        )}
      </Listbox>
    )}
  />
);

Select.defaultProps = {
  readOnly: false,
  onChange: () => {},
};

export default Select;
