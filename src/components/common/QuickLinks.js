/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import type { ComponentType, Node } from 'react';
import tw from 'twin.macro';
import { Link as BaseLink } from 'react-router-dom';

const Container = tw.div`w-[33%] mb-2 border border-t-2 border-t-gold-400 bg-white p-8 rounded`,
  InnerContainer = tw.div`flex`,
  Title = tw.p`uppercase font-bold mb-4 text-xl`,
  LinkContainer = tw.div`flex-1`,
  Link = tw(BaseLink)`hover:text-gold-400 flex max-w-max items-center`,
  Text = tw.span`text-sm`,
  IconWrapper = tw.span`text-7xl`;

export type QuickLinksItem = {|
  label: string,
  Icon: ComponentType<{}>,
  path: string,
|};

export type QuickLinksProps = {|
  title: string,
  Icon: ComponentType<{}>,
  items: Array<QuickLinksItem>,
|};

const QuickLinks = ({ title, items, Icon }: QuickLinksProps): Node => (
  <Container>
    <Title>{title}</Title>
    <InnerContainer>
      <LinkContainer>
        {items.map(({ label, path, Icon: LinkIcon }) => (
          <Link key={label} to={path}>
            <LinkIcon tw="mr-2" />
            <Text>{label}</Text>
          </Link>
        ))}
      </LinkContainer>
      <IconWrapper>
        <Icon />
      </IconWrapper>
    </InnerContainer>
  </Container>
);

export default QuickLinks;
