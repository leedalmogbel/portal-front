/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import * as React from 'react';
import 'twin.macro';
import { Link } from 'react-router-dom';

type MainProps = {
  children: React.Node,
};

const Main = ({ children }: MainProps): React.Node => {
  return (
    <main className="flex flex-col flex-grow -ml-64 transition-all duration-150 ease-in bg-arabic-pattern main md:ml-0 ">
      <header className="px-4 py-4 bg-white shadow header">
        <div className="flex flex-row items-center header-content">
          <form action="#">
            <div className="flex md:hidden">
              <Link
                to=""
                className="flex items-center justify-center w-10 h-10 border-transparent">
                <svg
                  className="w-6 h-6 text-gray-500"
                  fill="none"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  viewBox="0 0 24 24"
                  stroke="currentColor">
                  <path d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
                </svg>
              </Link>
            </div>
          </form>
          <div className="flex ml-auto">
            <Link to="" className="flex flex-row items-center">
              <img
                src="https://placeholder.pics/svg/300"
                alt=""
                className="w-10 h-10 bg-gray-200 border rounded-full"
              />
              <span className="flex flex-col ml-2">
                <span className="w-20 font-semibold leading-none tracking-wide truncate">
                  John Doe
                </span>
                <span className="w-20 mt-1 text-xs leading-none text-gray-500 truncate">
                  Manager
                </span>
              </span>
            </Link>
          </div>
        </div>
      </header>
      <div className="lg:px-8">{children}</div>
      <footer className="px-4 py-6 footer">
        <div className="footer-content">
          <p className="text-sm text-center text-gray-600">
            © Copyright 2022. All rights reserved. <a href="#"></a>
          </p>
        </div>
      </footer>
    </main>
  );
};

export default Main;
