/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Fragment, useState } from 'react';
import type { Node } from 'react';
import tw, { styled, css } from 'twin.macro';
import { Controller } from 'react-hook-form';
import { debounce } from 'lodash';
import { Combobox, Transition } from '@headlessui/react';
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid';

import type { Option } from 'types';

type AutoCompleteProps = {|
  id: string,
  name: string,
  options: Array<Option>,
  readOnly?: boolean,
  onInputChange: Function,
|};

const AutoComplete = ({
  id,
  name,
  options,
  readOnly,
  onInputChange,
}: AutoCompleteProps): Node => {
  const onInputChangeDebounced = debounce(onInputChange, 1000);

  return (
    <Controller
      name={name}
      render={({ field: { value, onChange } }) => (
        <Combobox value={value} onChange={onChange} disabled={readOnly}>
          <div tw="relative mt-1">
            <div tw="relative w-full border border-gray-300 overflow-hidden text-left bg-white rounded shadow-sm cursor-pointer focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75 focus-visible:ring-offset-2 focus-visible:ring-offset-gold-400 text-sm">
              <Combobox.Input
                css={readOnly && tw`cursor-not-allowed bg-gray-100`}
                tw="w-full py-2 pl-3 pr-10 text-sm leading-5 text-gray-900 border-none focus:ring-0"
                displayValue={(option) => option?.name}
                onChange={(event) => onInputChangeDebounced(event.target.value)}
              />
              <Combobox.Button
                tw="absolute inset-y-0 right-0 flex items-center pr-2"
                disabled={readOnly}
                css={readOnly && tw`cursor-not-allowed`}>
                <SelectorIcon tw="w-5 h-5 text-gray-400" aria-hidden="true" />
              </Combobox.Button>
            </div>
            <Transition
              as={Fragment}
              leave="transition ease-in duration-100"
              leaveFrom="opacity-100"
              leaveTo="opacity-0"
              afterLeave={() => onInputChange('')}>
              <Combobox.Options tw="absolute mt-1 max-h-60 z-10 w-full overflow-auto rounded-md bg-white py-1 shadow-lg focus:outline-none text-sm">
                {options.length === 0 ? (
                  <div tw="relative px-4 py-2 text-gray-700 cursor-default select-none">
                    Nothing found.
                  </div>
                ) : (
                  options.map((option) => (
                    <Combobox.Option
                      key={option.name}
                      value={option}
                      tw="relative select-none py-2 pl-4 pr-4 hover:(bg-gold-400 text-gray-50) cursor-pointer">
                      {({ selected, active }) => (
                        <>
                          <span tw="block truncate font-normal">
                            {option.name}
                          </span>
                          {selected ? (
                            <span tw="absolute inset-y-0 left-0 flex items-center pl-3 text-gold-400">
                              <CheckIcon tw="w-5 h-5" aria-hidden="true" />
                            </span>
                          ) : null}
                        </>
                      )}
                    </Combobox.Option>
                  ))
                )}
              </Combobox.Options>
            </Transition>
          </div>
        </Combobox>
      )}
    />
  );
};

AutoComplete.defaultProps = {
  readOnly: false,
};

export default AutoComplete;
