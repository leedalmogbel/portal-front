/**
 * @flow
 */

import type { Node } from 'react';
import tw, { styled } from 'twin.macro';

const StyledButton = styled.button(({ size, disabled }) => [
  tw`flex items-center px-4 py-2 text-xs text-white uppercase transition border-2 rounded outline-none bg-gold-400 ring-gold-400 border-gold-400`,
  tw`hover:(bg-white text-gold-400)`,
  size === 'sm' && tw`text-sm`,
  size === 'lg' && tw`text-lg`,
  size === 'xl' && tw`text-xl`,
  disabled &&
    tw`bg-gray-400 cursor-not-allowed ring-gray-400 border-gray-400 hover:(bg-gray-400 text-white)`,
]);

type ButtonProps = {|
  children: Node,
  className?: string,
  onClick?: Function,
  size?: 'xs' | 'sm' | 'md' | 'lg' | 'xl',
  type: 'button' | 'submit',
  disabled?: boolean,
|};

const Button = ({
  children,
  className,
  onClick,
  size,
  type,
  disabled,
}: ButtonProps): Node => (
  <StyledButton
    className={className}
    type={type}
    size={size}
    onClick={onClick}
    disabled={disabled}>
    {children}
  </StyledButton>
);

Button.defaultProps = {
  className: '',
  size: 'sm',
  onClick: () => {},
  disabled: false,
};

export default Button;
