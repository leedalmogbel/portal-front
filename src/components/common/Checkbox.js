/**
 * @flow
 */

import type { Node } from 'react';
import tw, { styled, css, theme } from 'twin.macro';
import { useFormContext } from 'react-hook-form';

const StyledCheckbox = styled.input(({ type, readOnly }) => [
  tw`w-4 h-4 mr-2 border-gray-300 border rounded shadow-sm focus:(ring-gold-400 border-gold-400) sm:text-sm`,
  type === 'file' && [
    css`
      &:focus {
        outline: 2px solid ${theme`colors.gold.400`};
      }
    `,
    tw`p-1 bg-white`,
  ],
  readOnly && tw`bg-gray-100 cursor-not-allowed ring-0`,
]);

type CheckboxProps = {|
  id: string,
  name: string,
  placeholder?: string,
  type: string,
  rules?: Object,
  readOnly?: boolean,
  isPledge?: boolean,
  format?: string,
  onChange?: Function,
|};

const Checkbox = ({
  type,
  placeholder,
  name,
  id,
  rules,
  readOnly,
  onChange,
}: CheckboxProps): Node => {
  const { register } = useFormContext();

  return (
    <StyledCheckbox
      type={type}
      placeholder={
        readOnly && type === 'password' ? '************' : placeholder
      }
      name={name}
      id={id}
      {...register(id, rules)}
      readOnly={readOnly}
      disabled={readOnly}
      onChange={onChange}
    />
  );
};

Checkbox.defaultProps = {
  placeholder: '',
  rules: { required: false },
  readOnly: false,
  format: '',
  isPledge: false,
  onChange: () => {},
};

export default Checkbox;
