/**
 * @flow
 */

import type { ComponentType, Node } from 'react';
import tw from 'twin.macro';

const Container = tw.div`flex justify-between items-center border rounded p-4`,
  IconWrapper = tw.div`text-7xl`,
  LabelContainer = tw.div`flex flex-col`,
  Label = tw.span`text-lg`,
  Counter = tw.span`text-5xl font-bold`;

export type StatProps = {|
  className?: string,
  label: string,
  count: number,
  Icon: ComponentType<{}>,
|};

const Stat = ({ className, label, count, Icon }: StatProps): Node => (
  <Container className={className}>
    <LabelContainer>
      <Label>{label}</Label>
      <Counter>{count}</Counter>
    </LabelContainer>
    <IconWrapper>
      <Icon />
    </IconWrapper>
  </Container>
);

Stat.defaultProps = {
  className: null,
};

export default Stat;
