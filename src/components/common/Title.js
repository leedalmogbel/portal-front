/**
 * @flow
 */

import type { Node } from 'react';
import tw, { styled } from 'twin.macro';

const Heading = styled.p(({ type }) => [
  tw`font-bold`,
  type === 'h1' && tw`text-3xl`,
  type === 'h2' && tw`text-2xl`,
  type === 'h3' && tw`text-xl`,
  type === 'h4' && tw`text-lg`,
  type === 'h5' && tw`text-sm`,
  type === 'h6' && tw`text-xs`,
]);

type HeadingTypes = 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';

type TitleProps = {|
  children: Node,
  className?: string,
  type: HeadingTypes,
|};

const Title = ({ children, className, type }: TitleProps): Node => (
  <Heading className={className} as={type} type={type}>
    {children}
  </Heading>
);

Title.defaultProps = {
  className: '',
};

export default Title;
