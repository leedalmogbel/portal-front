/**
 * @flow
 */

import type { Node } from 'react';
import tw, { styled } from 'twin.macro';

import { capitalizeFirstLetter } from 'utils/StringHelper';

const StyledStatus = tw.div`flex gap-2 items-center `;

const Circle = styled.div(({ status }) => [
  tw`w-2 h-2 rounded-full`,
  status === 'approved' && tw`bg-lime-600`,
  status === 'pending' && tw`bg-orange-400`,
  status === 'rejected' && tw`bg-red-600`,
]);

type StatusProps = {|
  status: string,
|};

const Status = ({ status }: StatusProps): Node => (
  <StyledStatus>
    <Circle status={status} /> {capitalizeFirstLetter(status)}
  </StyledStatus>
);

export default Status;
