/**
 * @flow
 */

import type { Node } from 'react';
import tw from 'twin.macro';

const StyledLabel = tw.label`block mb-0.5 text-sm uppercase font-bold text-gray-700`;

type LabelProps = {|
  children: Array<Node>,
  className?: string,
  htmlFor: string,
|};

const Label = ({ children, className, htmlFor }: LabelProps): Node => (
  <StyledLabel className={className} htmlFor={htmlFor}>
    {children}
  </StyledLabel>
);

Label.defaultProps = {
  className: '',
};

export default Label;
