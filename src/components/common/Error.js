/**
 * @flow
 */

import type { Node } from 'react';
import tw from 'twin.macro';

const Span = tw.span`text-xs text-red-600`;

type ErrorProps = {|
  children: string,
|};

const Error = ({ children }: ErrorProps): Node => <Span>{children}</Span>;

export default Error;
