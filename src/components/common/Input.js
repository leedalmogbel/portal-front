/**
 * @flow
 */

import type { Node } from 'react';
import tw, { styled, css, theme } from 'twin.macro';
import { useFormContext } from 'react-hook-form';

const StyledInput = styled.input(({ type, readOnly }) => [
  tw`w-full border-gray-300 border rounded shadow-sm focus:(ring-gold-400 border-gold-400) sm:text-sm`,
  type === 'file' && [
    css`
      &:focus {
        outline: 2px solid ${theme`colors.gold.400`};
      }
    `,
    tw`p-1 bg-white`,
  ],
  readOnly && tw`bg-gray-100 cursor-not-allowed ring-0`,
]);

type InputProps = {|
  id: string,
  name: string,
  placeholder?: string,
  type: string,
  rules?: Object,
  readOnly?: boolean,
  format?: string,
  onChange?: Function,
|};

const Input = ({
  type,
  placeholder,
  name,
  id,
  rules,
  readOnly,
  onChange,
}: InputProps): Node => {
  const { register, getValues } = useFormContext();

  if (id === 'confirmPassword') {
    rules = {
      validate: {
        match: (value) => value === getValues('password'),
      },
    };
  }

  if (id === 'emiratesId') {
    console.log(getValues('emiratesId').length);
    rules = {
      minLength: {
        value: 16,
        message: 'Length must be 3 or more',
      },
      maxLength: {
        value: 16,
        message: 'max 5',
      },
    };
  }

  // if (id === 'mobile') {
  //   console.log('pattern');
  //   // rules = {
  //   //   pattern: {
  //   //     value: /"^(?:00971|\+971|0)?(?:50|51|52|55|56|58|2|3|4|6|7|9)\d{9}$"/,
  //   //     message: 'Invalid mobile number',
  //   //   },
  //   // };
  //   rules = {
  //     validate: (value) => {
  //       const validateNum = new RegExp(
  //         /^(?:971|\+971|0)?(?:50|51|52|55|56|58|2|3|4|6|7|9)\d{7}$/
  //       );

  //       console.log(`bbbbbb${value}`, validateNum.test(value));
  //       console.log(
  //         'tae',
  //         /^(?:00971|\+971|0)?(?:50|51|52|55|56|58|2|3|4|6|7|9)\d{9}$/.test(
  //           value
  //         )
  //       );
  //     },
  //     // value &&
  //     // /"^(?:00971|\+971|0)?(?:50|51|52|55|56|58|2|3|4|6|7|9)\d{9}$"/.test(
  //     //   value
  //     // ),
  //   };

  // console.log('rules', rules);
  // pattern = {
  //   value: /"^(?:00971|\+971|0)?(?:50|51|52|55|56|58|2|3|4|6|7|9)\d{7}$"/,
  // };
  // }

  return (
    <StyledInput
      type={type}
      placeholder={
        readOnly && type === 'password' ? '************' : placeholder
      }
      name={name}
      id={id}
      {...register(id, rules)}
      readOnly={readOnly}
      disabled={readOnly}
      onChange={onChange}
    />
  );
};

Input.defaultProps = {
  placeholder: '',
  rules: { required: false },
  readOnly: false,
  format: '',
  onChange: () => {},
};

export default Input;
