/**
 * @flow
 */

import type { Node } from 'react';
import tw, { styled } from 'twin.macro';
import { useFormContext } from 'react-hook-form';

type TextareaProps = {|
  id: string,
  name: string,
  placeholder?: string,
  rules?: Object,
  readOnly?: boolean,
|};

const StyledTextarea = styled.textarea(({ readOnly }) => [
  tw`w-full h-40 border-gray-300 border rounded shadow-sm focus:(ring-gold-400 border-gold-400) sm:text-sm`,
  readOnly && tw`bg-gray-100 cursor-not-allowed ring-0`,
]);

const Textarea = ({
  placeholder,
  name,
  id,
  rules,
  readOnly,
}: TextareaProps): Node => {
  const { register } = useFormContext();

  return (
    <StyledTextarea
      placeholder={placeholder}
      name={name}
      id={id}
      {...register(id, rules)}
      readOnly={readOnly}
      disabled={readOnly}
    />
  );
};

Textarea.defaultProps = {
  placeholder: '',
  rules: { required: false },
  readOnly: false,
};

export default Textarea;
