/**
 * @flow
 */

import type { Node } from 'react';
import tw, { styled } from 'twin.macro';

const Line = styled.div(({ text }) => [
  tw`w-full overflow-hidden text-center`,
  tw`before:(h-px bg-gold-400 w-1/2 relative ml-[-50%] inline-block align-middle)`,
  tw`after:(h-px bg-gold-400 w-1/2 relative mr-[-50%] inline-block align-middle)`,
  text && tw`before:right-2 after:left-2`,
]);

type DividerProps = {|
  className?: string,
  text?: string,
|};

const Divider = ({ className, text }: DividerProps): Node => (
  <Line className={className} text={text}>
    {text}
  </Line>
);

Divider.defaultProps = {
  className: '',
  text: '',
};

export default Divider;
