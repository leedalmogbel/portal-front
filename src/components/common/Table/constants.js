/**
 * @flow
 */

export type TableHeader = {
  Header: string,
  accessor: string,
};
