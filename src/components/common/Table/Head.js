/**
 * @flow
 */

import type { Node } from 'react';
import tw from 'twin.macro';

const TableRow = tw.tr`border-2 border-gray-200 bg-gray-100`,
  TableHeader = tw.th`text-left font-bold text-sm uppercase py-2 px-4`;

type TableHeadProps = {|
  headerGroups: Array<any>,
|};

const TableHead = ({ headerGroups }: TableHeadProps): Node => (
  <thead>
    {
      // Loop over the header rows
      headerGroups.map((headerGroup, index) => (
        // Apply the header row props
        <TableRow key={index} {...headerGroup.getHeaderGroupProps()}>
          {
            // Loop over the headers in each row
            headerGroup.headers.map((column, index) => (
              // Apply the header cell props
              <TableHeader key={index} {...column.getHeaderProps()}>
                {
                  // Render the header
                  column.render('Header')
                }
              </TableHeader>
            ))
          }
        </TableRow>
      ))
    }
  </thead>
);

export default TableHead;
