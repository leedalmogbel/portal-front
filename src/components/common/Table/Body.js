/**
 * @flow
 */

import type { Node } from 'react';
import tw from 'twin.macro';

const TableRow = tw.tr`bg-white border-b-2 border-b-gray-200 border-l-2 border-l-gray-200 border-r-2 border-r-gray-200 hover:(bg-gray-50)`,
  TableData = tw.td`px-4 py-2`;

type TableBodyProps = {|
  getTableBodyProps: Function,
  rows: Array<any>,
  prepareRow: Function,
|};

const TableBody = ({
  getTableBodyProps,
  rows,
  prepareRow,
}: TableBodyProps): Node => (
  <tbody {...getTableBodyProps()}>
    {
      // Loop over the table rows
      rows.map((row, index) => {
        // Prepare the row for display
        prepareRow(row);
        return (
          // Apply the row props
          <TableRow key={index} {...row.getRowProps()}>
            {
              // Loop over the rows cells
              row.cells.map((cell, index) => {
                // Apply the cell props
                return (
                  <TableData key={index} {...cell.getCellProps()}>
                    {
                      // Render the cell contents
                      cell.render('Cell')
                    }
                  </TableData>
                );
              })
            }
          </TableRow>
        );
      })
    }
  </tbody>
);

export default TableBody;
