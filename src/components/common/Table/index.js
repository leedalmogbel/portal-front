/**
 * @flow
 */

import type { Node } from 'react';
import tw from 'twin.macro';

import TableHead from './Head';
import TableBody from './Body';

const StyledTable = tw.table`w-full`;

export type TableHeader = {|
  Header: string,
  accessor: string,
|};

type TableProps = {|
  tableInstance: {
    getTableProps: Function,
    getTableBodyProps: Function,
    headerGroups: Array<any>,
    rows: Array<any>,
    prepareRow: Function,
  },
|};

const Table = ({
  tableInstance: {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  },
}: TableProps): Node => (
  <StyledTable {...getTableProps()}>
    <TableHead headerGroups={headerGroups} />
    <TableBody
      getTableBodyProps={getTableBodyProps}
      rows={rows}
      prepareRow={prepareRow}
    />
  </StyledTable>
);

export default Table;
