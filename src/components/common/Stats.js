/**
 * @flow
 */
import type { Node } from 'react';
import tw, { styled } from 'twin.macro';
import { Link as BaseLink } from 'react-router-dom';

const Link = styled(BaseLink)(() => [
  tw`inline-block px-4 py-3 mr-6 font-bold text-blue-500 transition duration-300 ease-in-out border-2 border-blue-500 rounded-lg`,
  tw`hover:(bg-blue-500 text-white)`,
]);

type StatProps = {|
  to: string,
  children: Node,
  className?: string,
|};

const Stats = ({ children, className, to }: StatProps): Node => (
  <Link className={className} to={to}>
    {children}
  </Link>
);

Stats.defaultProps = {
  className: '',
};

export default Stats;
