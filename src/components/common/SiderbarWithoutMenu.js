/**
 * @flow
 */

import * as React from 'react';
import tw, { styled } from 'twin.macro';

import Logo from 'assets/images/EIEV_Web.png';

const Wrapper = styled.div(() => [
  tw`md:(w-1/4 bg-repeat-y items-baseline bg-cover justify-center flex bg-center)`,
]);

const SidebarWithoutMenu = (): React.Node => {
  return (
    <Wrapper className="img-shadow md:bg-landing-page">
      <img className="mx-auto w-80" src={Logo} alt=""></img>
    </Wrapper>
  );
};

export default SidebarWithoutMenu;
