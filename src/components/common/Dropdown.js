/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import type { Node, ComponentType } from 'react';
import 'twin.macro';
import { Menu, Transition } from '@headlessui/react';
import { Fragment } from 'react';
import { BsThreeDots } from 'react-icons/bs';

export type DropdownItem = {
  label: string,
  onClick: Function,
  Icon: ComponentType<any>,
};

type DropdownProps = {
  items: Array<Array<DropdownItem>>,
  id: string,
};

const Dropdown = ({ items, id }: DropdownProps): Node => (
  <Menu as="div" tw="relative inline-block">
    <div>
      <Menu.Button>
        <BsThreeDots tw="w-5 h-5" aria-hidden="true" />
      </Menu.Button>
    </div>
    <Transition
      as={Fragment}
      enter="transition ease-out duration-100"
      enterFrom="transform opacity-0 scale-95"
      enterTo="transform opacity-100 scale-100"
      leave="transition ease-in duration-75"
      leaveFrom="transform opacity-100 scale-100"
      leaveTo="transform opacity-0 scale-95">
      <Menu.Items tw="z-10 absolute right-0 w-40 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
        {items.map((item, index) => (
          <div tw="px-1 py-1 " key={index}>
            {item.map(({ label, onClick, Icon }) => (
              <Menu.Item key={label}>
                <button
                  tw="hover:(bg-gold-400 text-white) text-gray-900 flex w-full items-center rounded-md px-2 py-2 text-sm"
                  onClick={() => onClick(id)}>
                  <Icon tw="w-4 h-4 mr-2" />
                  {label}
                </button>
              </Menu.Item>
            ))}
          </div>
        ))}
      </Menu.Items>
    </Transition>
  </Menu>
);

export default Dropdown;
