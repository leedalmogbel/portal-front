/**
 * @flow
 * @jsxImportSource @emotion/react
 */
import * as React from 'react';
import 'twin.macro';

const Footer = (): React.Node => (
  <footer className="flex items-center justify-center h-16 bg-gray-700 footer">
    <div className="footer-content">
      <p className="text-sm font-bold text-white">
        Copyright © 2022 EIEV.AE - All rights reserved.
      </p>
    </div>
  </footer>
);

export default Footer;
