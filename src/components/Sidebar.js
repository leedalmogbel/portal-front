/**
 * @flow
 */
import { useEffect } from 'react';
import type { Node } from 'react';
import tw, { styled } from 'twin.macro';
import { useSelector } from 'react-redux';
import { Link as BaseLink, useLocation } from 'react-router-dom';
import { isEmpty } from 'lodash';

import logo from 'assets/images/EIEV_Web_v4.png';
import { routes } from 'routes';
import type { Route } from 'routes';
import { useAuth } from 'hooks/auth';
import { useNotification } from 'hooks/notification';

const SidebarContainer = styled.div(({ userType }) => [
    tw`fixed inset-y-0 left-0 z-30 w-64 overflow-y-auto transition duration-300 ease-in transform -translate-x-full bg-gold-400`,
    tw`md:(w-3/12 translate-x-0 ease-out static inset-0 shadow-lg)`,
    tw`lg:(w-2/12)`,
    userType === 'Admin' && tw`bg-eiev-100`,
  ]),
  SidebarMenuContainer = tw.div`border-b border-b-gray-300 border-t border-t-gray-500`,
  LogoWrapper = tw.div`mb-4 p-3`,
  IconWrapper = tw.div`text-xs md:(text-lg) lg:(text-2xl mr-2)`,
  MenuBarWrapper = tw.div`flex justify-between`,
  Badge = tw.span`inline-flex items-center justify-center px-2 py-1 mr-2 text-xs font-bold leading-none text-red-100 bg-red-600 rounded-full`,
  Link = styled(BaseLink)(({ isActive }) => [
    tw`flex items-center px-4 py-3 text-xs text-white justify-between`,
    tw`md:(text-base) border-t border-t-gray-300 border-b border-b-gray-500`,
    tw`hover:(bg-gray-50 text-gray-900)`,
    isActive && tw`text-gray-700 bg-gray-50`,
  ]);

type SidebarMenuItemProps = {|
  route: Route,
  current: string,
  notif: Object,
|};

const SidebarMenuItem = ({
  route: { path, sidebar },
  current,
  notif,
}: SidebarMenuItemProps) => {
  if (!sidebar) return null;
  const { name, Icon } = sidebar;
  const isActive = current.includes(path);
  console.log(notif, 'zxczxc');
  let notifKeys = {};
  let count = 0;
  if (notif) {
    notifKeys = Object.keys(notif);
    console.log('notifKeys', notif);
    notifKeys.forEach((key, index) => {
      console.log('key', key);
      if (key.toUpperCase() === name.toUpperCase()) {
        count = notif[key].length;
      }
      console.log(console.log(`${key.toUpperCase()}: ${notif[key]}`));
    });
  }
  // console.log(notifKeys.toUpperCase(), 'lalalal');

  // const handleSidebarClick = () => {
  //   console.log(count);
  // };

  return (
    <Link
      to={path}
      isActive={isActive}
      // onClick={handleSidebarClick}
    >
      <MenuBarWrapper>
        <IconWrapper>
          <Icon />
        </IconWrapper>
        {name}
      </MenuBarWrapper>
      {count > 0 && <Badge>{count}</Badge>}
    </Link>
  );
};

const Sidebar = (): Node => {
  const { user } = useAuth();
  const { pathname } = useLocation();
  const { userNotification, dispatchFetchNotification } = useNotification();
  const {
    data: notification,
    status: { fetched },
  } = useSelector<State, NotificationState>((state) => state.notification);

  let _notif = {};
  useEffect(() => {
    dispatchFetchNotification();
  }, [dispatchFetchNotification]);
  useEffect(() => {
    if (fetched) {
      _notif = notification;
      console.log(userNotification, 'ligalig');
    }
    // console.log('userNotification', notification);
  }, [dispatchFetchNotification, notification, userNotification, fetched]);

  return (
    <SidebarContainer userType={user?.userType}>
      <LogoWrapper>
        <img src={logo} alt="EIEV" />
      </LogoWrapper>
      {/* TODO: Set active link */}
      <SidebarMenuContainer>
        {routes
          .filter((route) =>
            route.sidebar &&
            (isEmpty(route.permittedRoles) ||
              route.permittedRoles?.includes(user?.userType))
              ? true
              : false
          )
          .map((route) => (
            <SidebarMenuItem
              route={route}
              current={pathname}
              key={route.key}
              notif={userNotification}
            />
          ))}
      </SidebarMenuContainer>
    </SidebarContainer>
  );
};

export default Sidebar;
