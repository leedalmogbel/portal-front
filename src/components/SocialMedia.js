/**
 * @flow
 */

import type { Node } from 'react';
import tw from 'twin.macro';
import {
  BsTwitter,
  BsFacebook,
  BsInstagram,
  BsYoutube,
  BsWhatsapp,
  BsFillTelephoneFill,
} from 'react-icons/bs';
import { MdEmail } from 'react-icons/md';

import BaseTitle from 'components/common/Title';

const Container = tw.div``,
  Title = tw(BaseTitle)`mb-8`,
  MediaLinks = tw.div`flex flex-row gap-2`,
  MediaLink = tw.a`p-2 inline-block text-white border-2 cursor-pointer border-gold-400 bg-gold-400`;

type SocialMediaProps = {
  className?: string,
};

const SocialMedia = ({ className }: SocialMediaProps): Node => (
  <Container className={className}>
    <Title type="h2">Stay in touch with us</Title>
    <MediaLinks>
      <MediaLink href="https://twitter.com" target="_blank">
        <BsTwitter />
      </MediaLink>
      <MediaLink href="https://facebook.com" target="_blank">
        <BsFacebook />
      </MediaLink>
      <MediaLink href="https://instagram.com" target="_blank">
        <BsInstagram />
      </MediaLink>
      <MediaLink href="https://youtube.com" target="_blank">
        <BsYoutube />
      </MediaLink>
      <MediaLink href="https://wa.me/000000000000" target="_blank">
        <BsWhatsapp />
      </MediaLink>
      <MediaLink href="tel:00000000000" target="_blank">
        <BsFillTelephoneFill />
      </MediaLink>
      <MediaLink href="mailto:email@domain.com" target="_blank">
        <MdEmail />
      </MediaLink>
    </MediaLinks>
  </Container>
);

SocialMedia.defaultProps = {
  className: '',
};

export default SocialMedia;
