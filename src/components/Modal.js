/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { Dialog, Transition } from '@headlessui/react';
import { Fragment } from 'react';
import type { Node } from 'react';
import 'twin.macro';

type ModalProps = {
  isOpen: boolean,
  title: string,
  body: string,
  closeModal: Function,
  reject: Function,
};

const Modal = ({
  isOpen,
  closeModal,
  title,
  body,
  reject,
}: ModalProps): Node => {
  if (!isOpen) return null;

  return (
    <Transition appear show={isOpen} as={Fragment}>
      <Dialog as="div" tw="relative z-10" onClose={closeModal}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-900"
          enterFrom="opacity-100"
          enterTo="opacity-600"
          leave="ease-in duration-900"
          leaveFrom="opacity-600"
          leaveTo="opacity-100">
          <div tw="fixed inset-0 bg-black/30 bg-opacity-25" />
        </Transition.Child>

        <div tw="fixed inset-0 overflow-y-auto">
          <div tw="flex min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-900"
              enterFrom="opacity-100 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-500"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95">
              <Dialog.Panel tw="w-full max-w-xl transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
                <Dialog.Title
                  as="h3"
                  tw="text-lg font-semibold leading-6 text-gray-900">
                  {title ? title : 'Modal'}
                </Dialog.Title>
                <div tw="mt-2 text-gray-500">
                  {body ? (
                    <p tw="text-center">{body}</p>
                  ) : (
                    <Fragment>
                      <p>PLEDGE WORDINGS (ENGLISH)</p>
                      <p>
                        A pledge to sell a horse participated in the Endurance
                        Ride
                      </p>
                      <p>
                        I hereby declare that I will sell the horse to Al
                        Wathbah Stables if it wish to purchase it, in case if
                        that horse has successfully passed the necessary medical
                        tests and doping tests. , According to the following
                        Sections:
                      </p>
                      <p tw="font-semibold text-gray-700">
                        Section 1: Selling prices for Al Wathbah Stables as the
                        following:
                      </p>
                      <p>First place: AED 1.5 million.</p>
                      <p>Second place: AED 1 million.</p>
                      <p>
                        Third place till the last position: 500 thousand
                        dirhams.
                      </p>
                      <p tw="font-semibold text-gray-700">
                        Section 2: In the event of purchasing the Al Wathbah
                        Stables horse commit and undertake the following:
                      </p>
                      <p>
                        - To keep the horse sold and remain in my custody and
                        under my personal responsibility until the completion of
                        delivery to the Al Wathbah Stables.
                      </p>
                      <p>
                        - I do not participate in any race until the completion
                        results of medical tests and doping tests.
                      </p>
                      <p>
                        - To deliver it to the Al Wathbah Stables for good
                        health and good condition free from any defects, and in
                        the time, date and place to be determined.
                      </p>
                      <p>
                        - I undertake directly or through an official power of
                        attorney of another person to complete all the sales
                        procedures.
                      </p>
                      <p tw="font-semibold text-gray-700">
                        Section 3: canceling the sale in case of any defect in
                        the horse.
                      </p>
                      <p>
                        I undertake to accept the cancellation of the contract
                        of sale in case of any defect in the horse after the
                        completion of the sale.
                      </p>
                      <p tw="text-gray-700">Section 4: Penal Condition</p>
                      <p>
                        - In case of any breach of any of the provisions of this
                        declaration or undertaking, I shall be obliged to pay
                        To/ Al Wathbah Stables a contractual compensation of AED
                        1 million.
                      </p>
                      <p>
                        - In case of any breach of any of the provisions of this
                        declaration or undertaking, I shall waive the prize I
                        have obtained in the Endurance Ride of my own free will.
                      </p>
                      <p>
                        This is a pledge and acknowledgment of this, and
                        irrevocable
                      </p>
                      <p>
                        The OC will confirm the pledge 1 day before the start of
                        the ride
                      </p>
                    </Fragment>
                  )}
                </div>
                <div tw="text-center mt-2">
                  <div
                    tw="p-2 bg-gold-700 items-center text-indigo-100 leading-none lg:rounded-full flex lg:inline-flex"
                    role="alert">
                    <span tw="flex rounded-full bg-gold-300 uppercase px-2 py-1 text-xs font-bold mr-3">
                      Info
                    </span>
                    <span tw="font-semibold mr-2 text-left flex-auto">
                      If you agree on this, All of the rules here will be
                      accepted by you.
                    </span>
                  </div>
                </div>

                <div tw="mt-4 flex justify-evenly">
                  <button
                    type="button"
                    tw="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                    onClick={closeModal}>
                    I Agree
                  </button>
                  <button
                    type="button"
                    tw="inline-flex justify-center rounded-md border border-transparent bg-red-100 px-4 py-2 text-sm font-medium text-red-900 hover:bg-red-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-red-500 focus-visible:ring-offset-2"
                    onClick={reject}>
                    I Disagree
                  </button>
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  );
};

export default Modal;
