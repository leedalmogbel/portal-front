/**
 * @flow
 */

import type { Node } from 'react';
import tw from 'twin.macro';

const Wrapper = tw.div`
  w-full h-screen
  text-sm font-medium text-gray-600
  p-6
`;

const ThemedSuspense = (): Node => <Wrapper>Loading...</Wrapper>;

export default ThemedSuspense;
