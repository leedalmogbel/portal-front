/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import type { Node } from 'react';
import tw, { styled } from 'twin.macro';

import logo from 'assets/images/EIEV_Web.png';

const StyledBanner = styled.div(() => [
  tw`background-image[url('/assets/images/bg-sidebar.png')]`,
  tw`pt-8 ease-out translate-x-0 bg-cover`,
  tw`lg:(w-3/12 pt-20)`,
]);

const Logo = tw.img`w-3/6 mx-auto`;

type BannerProps = {|
  className?: string,
|};

const Banner = ({ className }: BannerProps): Node => (
  <StyledBanner className={className}>
    <Logo src={logo} alt="EIEV" />
  </StyledBanner>
);

Banner.defaultProps = {
  className: '',
};

export default Banner;
