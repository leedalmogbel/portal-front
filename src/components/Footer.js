/**
 * @flow
 */

import type { Node } from 'react';
import tw, { styled } from 'twin.macro';

const FooterWrapper = styled.footer(({ type }) => [
  tw`flex items-center justify-center text-xs text-gray-900`,
  type === 'inner' && tw`my-6`,
  type === 'full' && tw`h-12 mx-0 bg-gray-900 text-gray-50`,
]);

type FooterProps = {|
  type?: 'full' | 'inner',
|};

const Footer = ({ type }: FooterProps): Node => (
  <FooterWrapper type={type}>
    <p>&copy; Copyright 2022. All rights reserved.</p>
  </FooterWrapper>
);

Footer.defaultProps = {
  type: 'inner',
};

export default Footer;
