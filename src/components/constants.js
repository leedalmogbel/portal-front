/**
 * @flow
 * @jsxImportSource @emotion/react
 */

import { GrFormAdd } from 'react-icons/gr';
import { GiHorseHead, GiCowboyBoot } from 'react-icons/gi';
import { FaHatCowboy, FaUserTie } from 'react-icons/fa';
import { MdAutorenew, MdGroups } from 'react-icons/md';
import { BsCardChecklist } from 'react-icons/bs';
import { RiListOrdered } from 'react-icons/ri';
import { IoCardSharp } from 'react-icons/io5';

import type { QuickLinksItem, QuickLinksProps } from './common/QuickLinks';

const athletesQuickLinks: Array<QuickLinksItem> = [
  {
    Icon: GrFormAdd,
    label: 'Register New Athlete',
    path: '/riders/add',
  },
  // {
  //   Icon: MdAutorenew,
  //   label: 'Renew Athlete',
  //   path: '/athletes/renew',
  // },
  // {
  //   Icon: BsCardChecklist,
  //   label: 'Applications',
  //   path: '/athletes/applications',
  // },
  {
    Icon: MdGroups,
    label: 'Manage Athletes',
    path: '/riders',
  },
];

const trainersQuickLinks: Array<QuickLinksItem> = [
  {
    Icon: GrFormAdd,
    label: 'Register New Trainer',
    path: '/trainers/add',
  },
  // {
  //   Icon: MdAutorenew,
  //   label: 'Renew Athlete',
  //   path: '/athletes/renew',
  // },
  // {
  //   Icon: BsCardChecklist,
  //   label: 'Applications',
  //   path: '/athletes/applications',
  // },
  {
    Icon: MdGroups,
    label: 'Manage Trainers',
    path: '/trainers',
  },
];

const horsesQuickLinks: Array<QuickLinksItem> = [
  {
    Icon: GrFormAdd,
    label: 'Register New Horse',
    path: '/horses/add',
  },
  // {
  //   Icon: MdAutorenew,
  //   label: 'Renew Athlete',
  //   path: '/athletes/renew',
  // },
  // {
  //   Icon: BsCardChecklist,
  //   label: 'Applications',
  //   path: '/athletes/applications',
  // },
  {
    Icon: MdGroups,
    label: 'Manage Horses',
    path: '/horses',
  },
];

const ownersQuickLinks: Array<QuickLinksItem> = [
  {
    Icon: GrFormAdd,
    label: 'Register New Owner',
    path: '/owners/add',
  },
  // {
  //   Icon: MdAutorenew,
  //   label: 'Renew Athlete',
  //   path: '/athletes/renew',
  // },
  // {
  //   Icon: BsCardChecklist,
  //   label: 'Applications',
  //   path: '/athletes/applications',
  // },
  {
    Icon: MdGroups,
    label: 'Manage Owners',
    path: '/owners',
  },
];

const entriesQuickLinks: Array<QuickLinksItem> = [
  {
    Icon: GrFormAdd,
    label: 'Register New Entry',
    path: '/entries/add',
  },
  {
    Icon: MdAutorenew,
    label: 'Find Entry',
    path: '/entries/renew',
  },
  {
    Icon: BsCardChecklist,
    label: 'Season',
    path: '/entries/applications',
  },
  {
    Icon: MdGroups,
    label: 'Manage Enties',
    path: '/entries',
  },
];

export const quickLinks: Array<QuickLinksProps> = [
  {
    title: 'athletes',
    Icon: GiCowboyBoot,
    items: athletesQuickLinks,
  },
  {
    title: 'trainers',
    Icon: FaHatCowboy,
    items: trainersQuickLinks,
  },
  {
    title: 'horses',
    Icon: GiHorseHead,
    items: horsesQuickLinks,
  },
  {
    title: 'owners',
    Icon: FaUserTie,
    items: ownersQuickLinks,
  },
  {
    title: 'entries',
    Icon: RiListOrdered,
    items: entriesQuickLinks,
  },
  {
    title: 'finance and billing',
    Icon: IoCardSharp,
    items: athletesQuickLinks,
  },
];
